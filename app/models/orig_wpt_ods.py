from app.constants import TRANSACTION_TYPES, CHANGE_CAUSE_TYPES, TICKETS_OP_TYPE
from app.extensions import db
from app.libs.datetime_type import OGReadableAwareDateTime


class WPTODSGoldActivity(db.Model):
    __bind_key__ = 'orig_wpt_ods'
    __tablename__ = 'powergamecoin_detail'

    id = db.Column('id', db.Integer, primary_key=True)
    og_account = db.Column('username', db.String)
    game_id = db.Column('gameid', db.Integer)
    transaction_type = db.Column('producttype', db.Integer)
    transaction_amount = db.Column('gamecoin', db.Integer)
    balance = db.Column('coin', db.Integer)
    ip = db.Column('userip', db.Integer)
    created_at = db.Column('recdate', OGReadableAwareDateTime)

    def transaction_type_display(self):
        value = TRANSACTION_TYPES[self.transaction_type]
        return '%s %s' % (self.transaction_type, value) if value is not None else self.transaction_type

    def balance_display(self):
        return self.transaction_amount + self.balance

    def game_id_display(self):
        return self.game_id


class WPTODSSilverActivity(db.Model):
    __bind_key__ = 'orig_wpt_ods'
    __tablename__ = 'gamecoin_detail'

    id = db.Column('id', db.Integer, primary_key=True)
    og_account = db.Column('username', db.String)
    game_id = db.Column('gameid', db.Integer)
    transaction_type = db.Column('producttype', db.Integer)
    transaction_amount = db.Column('gamecoin', db.Integer)
    balance = db.Column('coin', db.Integer)
    ip = db.Column('userip', db.Integer)
    created_at = db.Column('recdate', OGReadableAwareDateTime)

    def transaction_type_display(self):
        value = TRANSACTION_TYPES[self.transaction_type]
        return '%s %s' % (self.transaction_type, value) if value is not None else self.transaction_type

    def balance_display(self):
        return self.transaction_amount + self.balance

    def game_id_display(self):
        return self.game_id


class WPTODSTICKETDETAIL(db.Model):
    __bind_key__ = 'orig_wpt_ods'
    __tablename__ = 'tj_flow_ticket_detail'

    rolename = db.Column('ROLENAME', db.String, primary_key=True)
    changecause = db.Column('CHANGECAUSE', db.Integer)
    changeamount = db.Column('CHANGEAMOUNT', db.Integer)
    updatetime = db.Column('UPDATETIME', db.DateTime)
    op_type = db.Column('OP_TYPE', db.Integer)
    ticket_itemcode = db.Column('TICKET_ITEMCODE', db.String)

    __table_args__ = {'extend_existing': True, 'schema': 'OGDZWEB'}

    def change_cause_display(self):
        return '{}'.format(CHANGE_CAUSE_TYPES.get(self.changecause, ' '))

    def op_type_display(self):
        return '{}'.format(TICKETS_OP_TYPE.get(self.op_type, ' '))

