#### Celery Task ############
import datetime
import json
from datetime import timedelta
from urllib import parse

import arrow
import base64
import re
import requests
import sqlparse
from flask import current_app as app
from functools import reduce
from operator import iand, ior
from random import choice
from sqlalchemy import UniqueConstraint
from sqlalchemy import text, bindparam
from sqlalchemy.schema import Index

from app.constants import PROMOTION_PUSH_HISTORY_STATUSES, PROMOTION_PUSH_STATUSES, PROMOTION_PUSH_TYPES
from app.exceptions import SQLException
from app.extensions import db, sendgrid, aws_sns_client
from app.extensions import redis_conn
from app.libs.datetime_type import AwareDateTime
from app.models.bi import BIUser
from app.models.main import AdminUser, AdminUserQuery
from app.models.orig_wpt import DisabledARN, WPTSNSTOKEN
from app.tasks import with_db_context, celery
from app.tasks.sendgrid import get_campaigns, get_senders
from app.utils import current_time, error_msg_from_exception, generate_email_validate_url


@celery.task
def send_message_to_user(user_id, increment, gold_balance):
    increment = '{0:,}'.format(int(increment))
    gold_balance = '{0:,}'.format(int(gold_balance))

    def send_facebook():

        facebook_id = db.session.query(BIUser.facebook_id).filter_by(user_id=user_id).scalar()
        if facebook_id is None:
            return

        facebook_message = """PlayWPT has deposited {} Gold into your account.""".format(increment)

        data = {
            'access_token': '122212108221118|iY2aFJsHhyWUvTV_4oeoGrXX-TA',
            'include_headers': 'false',
            'batch': json.dumps([{'method': 'POST', 'relative_url': 'v2.7/' + facebook_id + '/notifications',
                                  'body': parse.urlencode(
                                      {'template': facebook_message})}])
        }

        try:
            req = requests.post('https://graph.facebook.com', data=data)
        except Exception as e:
            print('Facebook Gold notification for user_id  {} :  exception '.format(
                user_id) + error_msg_from_exception(e))
        else:
            if req:
                responses = req.json()
                for response in responses:
                    resp_body = json.loads(response['body'])
                    if 'error' in resp_body:

                        print('Facebook Gold notification for user_id  {} : error: '.format(user_id))
                    else:
                        print('Facebook Gold notification for user_id  {} : succeed '.format(user_id))

            else:
                print('Facebook Gold notification for user_id {} : no response  '.format(user_id))

    def send_mobile():

        endpoint_arns = db.session.query(WPTSNSTOKEN.endpoint_arn).filter(
            WPTSNSTOKEN.endpoint_arn.isnot(None),
            WPTSNSTOKEN.platform.isnot(None),
            WPTSNSTOKEN.user_id == user_id).all()

        if endpoint_arns is None:
            return

        def send_to_aws_sns(target):

            body = """PlayWPT has deposited {} Gold into your account.""".format(increment)

            ios_payload = {'aps': {'content-available': 1, 'sound': 'default', 'alert': {'body': body}}}
            android_payload = {"notification": {"body": body}}

            payload = {'default': body,
                       'APNS': json.dumps(ios_payload),
                       'gcm': json.dumps(android_payload)}

            message = json.dumps(payload)

            aws_sns_client.publish(TargetArn=target, Message=message, MessageStructure='json')

        for endpoint_arn in endpoint_arns:

            try:
                send_to_aws_sns(endpoint_arn[0])

            except Exception as e:
                print(
                    'Mobile Gold notification for user_id  {} :  exception '.format(user_id) + error_msg_from_exception(
                        e))

            else:
                print('Mobile Gold notification for user_id  {} : succeed '.format(user_id))

    def send_email():

        result_proxy = \
            db.session.query(BIUser.user_id, BIUser.display_name, BIUser.email).filter(BIUser.user_id == user_id).all()[
                0]
        email = result_proxy[2]
        display_name = result_proxy[1]

        if email is None:
            return

        response = sendgrid.client.templates._('e9b5095f-42d9-49e1-8ab0-94ef9f958f52').get()
        email_content = json.loads(response._body.decode())['versions'][4]['html_content']
        email_subject = 'Your Account has been Credited with {} Gold'.format(increment)

        email_content = email_content. \
            replace("[Unsubscribe]", '<%asm_group_unsubscribe_raw_url%>'). \
            replace("{amount}", increment). \
            replace("[%email%]", "-email-"). \
            replace("[%Gold_Balance | %]", "-gold_balance-"). \
            replace("[%Play_Username&nbsp;| valued member%]", "-Play_Username-")

        suppression = {"group_id": 2161, "groups_to_display": [2161]}

        data = {
            "personalizations": [
                {
                    "to": [
                        {
                            "email": email,
                        }
                    ],
                    "subject": email_subject,

                    "substitutions": {
                        "-email-": email,
                        "-gold_balance-": gold_balance,
                        "-Play_Username-": display_name}
                }
            ],
            "from": {
                'email': 'no-reply@playwpt.com', 'name': 'PlayWPT'
            },

            "content": [
                {
                    "type": "text/html",
                    "value": email_content
                }
            ],

            'asm': suppression
        }

        try:
            sendgrid.client.mail.send.post(request_body=data)

        except Exception as e:

            print(
                'Email Gold notification for user_id  {} :  exception '.format(user_id) + error_msg_from_exception(e))

        else:
            print('Email Gold notification for user_id  {} :  succeed '.format(user_id))

    send_facebook()
    send_mobile()
    send_email()


@celery.task
def auto_promotion():
    AutoPromotionPush.check_auto_promotion()


@celery.task
def facebook_promotion_push(worker, push_id=None):
    PromotionPushHistory.send_facebook(worker=worker, push_id=push_id)


@celery.task
def email_promotion_push(worker, push_id):
    PromotionPushHistory.send_email(worker=worker, push_id=push_id)


@celery.task
def mobile_promotion_push(push_id, worker):
    PromotionPushHistory.send_mobile(push_id=push_id, worker=worker)


@celery.task
def push_async(push_id, cohort_id=None):
    push = db.session.query(PromotionPush).filter_by(id=push_id).one()

    if cohort_id is not None:
        push.publish_to_push_history(cohort_id)
        AutoPromotionPush.get_cohort(cohort_id).update_cohort_status()
    else:
        push.publish_to_push_history()


@celery.task
def retry(push_id):
    push = db.session.query(PromotionPush).filter_by(id=push_id).one()
    push.retry()


################################

class AutoPromotionPush(db.Model):
    __bind_key__ = 'bi_promotion'
    __tablename__ = 'promotion_auto_push'
    id = db.Column(db.Integer, primary_key=True)
    # 对应promotion_push中的id
    push_id = db.Column(db.Integer, nullable=False, index=True)
    push_type = db.Column(db.String(50), nullable=False, index=True)
    cohort_name = db.Column(db.String(50), nullable=False, index=True)
    category = db.Column(db.String(50), nullable=False, default='Default')
    # 发送的邮件的活动的id
    campaign_id = db.Column(db.Integer)
    # 活动配置,发件人，邮件主题等
    campaign_options = db.Column(db.Text)

    # 此cohort的描述
    description = db.Column(db.Text, nullable=False)
    # 可以重载几次
    maximum_count = db.Column(db.Integer, nullable=False, default=0)
    # 剩余几次
    remaining_count = db.Column(db.Integer, nullable=False, default=maximum_count)
    # 重载间隔
    rearm = db.Column(db.Integer, nullable=False, index=True)
    ##TODO
    # 是否自动排除近已经发送过的人
    is_important = db.Column(db.Boolean, nullable=False, default=False)
    # 是否是暂停状态
    is_paused = db.Column(db.Boolean, nullable=False, default=False)
    # 是否激活
    is_activated = db.Column(db.Boolean, nullable=False, default=False)

    __table_args__ = (UniqueConstraint('push_type', 'cohort_name', name='ix_uniq_push_type_and_cohort_name'),)

    @classmethod
    def get_mobile_cohort(cls, cohort_name):
        cohort = db.session.query(AutoPromotionPush).filter_by(cohort_name=cohort_name,
                                                               push_type=PROMOTION_PUSH_TYPES.MOBILE.value)

        return cohort

    @classmethod
    def get_email_cohort(csl, cohort_name):
        cohort = db.session.query(AutoPromotionPush).filter_by(cohort_name=cohort_name,
                                                               push_type=PROMOTION_PUSH_TYPES.EMAIL.value)

        return cohort

    @classmethod
    def get_facebook_cohort(cls, cohort_name):
        cohort = db.session.query(AutoPromotionPush).filter_by(cohort_name=cohort_name,
                                                               push_type=PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value)

        return cohort

    def update_cohort_status(self):

        scheduled_at = self.push.scheduled_at.to('UTC')

        if self.remaining_count - 1 != 0:
            self.push.scheduled_at = (scheduled_at + timedelta(hours=self.rearm)).to('UTC')

        self.remaining_count -= 1
        self.push.rearm_count += 1

        db.session.flush()
        db.session.commit()

    @property
    def push(self):

        promotion = db.session.query(PromotionPush).filter_by(id=self.push_id).one()
        return promotion

    def should_notify(self):

        scheduled_at = self.push.scheduled_at.to('UTC')

        # celery 阻塞,或者其他原因停止,延误不超过30分钟的任务可以被正常处理
        start = current_time().to('-00:30').format('YYYY-MM-DD HH:mm:ss')
        now = current_time().format('YYYY-MM-DD HH:mm:ss')

        triggered_times = []

        def generate_triggered_time(scheduled_at, remaining_count):
            for i in range(remaining_count):
                trigger_time = (scheduled_at + timedelta(hours=self.rearm)).to('UTC')
                scheduled_at = trigger_time
                triggered_times.append(scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

            return triggered_times

        triggered_times = generate_triggered_time(scheduled_at, self.remaining_count)
        triggered_times.insert(0, scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

        for triggered_time in triggered_times:
            if start <= triggered_time <= now:

                if not self.is_paused and self.is_activated:
                    return True
                else:
                    self.update_cohort_status()
                    return False

    @classmethod
    def alive_cohort_ids(cls):
        cohort_ids = db.session.query(AutoPromotionPush.id).filter(AutoPromotionPush.remaining_count > 0).all()
        cohort_ids = [row[0] for row in cohort_ids]
        return cohort_ids

    @classmethod
    def get_cohort(cls, cohort_id):
        cohort = db.session.query(AutoPromotionPush).filter(AutoPromotionPush.id == cohort_id).one()
        return cohort

    @classmethod
    def check_auto_promotion(cls):
        cohort_ids = cls.alive_cohort_ids()

        for cohort_id in cohort_ids:

            cohort = cls.get_cohort(cohort_id)

            if cohort.should_notify():

                # 开始处理当前cohort对应的push

                push_type = cohort.push_type
                cohort_name = cohort.cohort_name
                print('Start To Process {} Automated Promotion For {} '.format(push_type, cohort_name))

                if app.config['ENV'] == 'prod':

                    push_async.apply_async((cohort.push.id, cohort_id), queue='default')

                else:

                    push_async(cohort.push.id, cohort_id)

                print('Update {} Automated Promotion For {} '.format(push_type, cohort_name))

    @staticmethod
    def get_sendgrid_campaigns():
        sendgrid_campaigns = redis_conn.get('campaigns')
        sendgrid_senders = redis_conn.get('senders')

        if sendgrid_campaigns is None or sendgrid_senders is None:
            campaigns_content = get_campaigns()
            senders_content = get_senders()

        else:

            campaigns_content = json.loads(sendgrid_campaigns)
            senders_content = json.loads(sendgrid_senders)

        campaigns = list(map(lambda x: {'id': x['id'], 'title': x['title'], 'status': x['status']}, campaigns_content))
        senders = reversed(list(map(lambda x: {'id': x['id'], 'nickname': x['nickname']}, senders_content)))
        statuses = list(set(map(lambda x: x['status'], campaigns)))

        return dict(statuses=statuses, campaigns=campaigns, senders=senders)


class PromotionPush(db.Model):
    _worker = {'email': 1, 'mobile': 6, 'fb_notification': 1}

    __bind_key__ = 'bi_promotion'
    id = db.Column(db.Integer, primary_key=True)
    admin_user_id = db.Column(db.Integer, nullable=False, index=True)

    cohort_name = db.Column(db.String(50))

    # 此推广使用的查询id，对于admin_user_query中的id
    based_query_id = db.Column(db.Integer)
    # 此推广使用的规则，前端筛选用户的功能的配置
    based_query_rules = db.Column(db.String(50))
    # 此推广使用的sql
    based_sql = db.Column(db.Text)
    # 这个push第几次被重新使用
    rearm_count = db.Column(db.Integer, nullable=False, default=0)

    push_type = db.Column(db.String(50), nullable=False, index=True)
    status = db.Column(db.String(50))
    # facebook message或者email内容
    message = db.Column(db.Text, nullable=False)
    message_key = db.Column(db.String(255), nullable=False, index=True)
    # 计划发送的时间，如果是cohort的自动化模式，会定期刷新
    scheduled_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)
    is_automated = db.Column(db.Boolean, nullable=False, default=False)
    updated_at = db.Column(AwareDateTime, onupdate=current_time, index=True)
    created_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)

    @property
    def requested_by(self):
        if self.admin_user_id is None:
            return None
        admin_user = db.session.query(AdminUser).filter_by(id=self.admin_user_id).first()
        if admin_user:
            return admin_user.name

    #### 提取用户 #####

    @staticmethod
    def extract_user_ids(based_query_id, based_sql, for_check=False, all_users=False):

        """
        根据用户提供的sql或者查询条件，提取收件人的user_id
        """

        if all_users:
            user_ids = [row[0] for row in db.session.query(BIUser.user_id).all()]
            return user_ids
        elif based_sql:
            sql = json.loads(based_sql)['formatted_sql']
            database = json.loads(based_sql)['database']
            if database == 'bi': database = None
        elif based_query_id:
            query = db.session.query(AdminUserQuery).filter_by(id=based_query_id).first()
            sql = query.sql
            database = query.target_db
        else:
            return

        stmt = sqlparse.parse(sql)[0]
        tokens = [str(item) for item in stmt.tokens]
        slim_sql = ' '.join(tokens)
        result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(slim_sql))
        column_names = [col[0] for col in result_proxy.cursor.description]
        column_names = [column_name.lower() for column_name in column_names]

        if for_check:
            if not ('user_id' in column_names or 'og_account' in column_names or 'username' in column_names):
                raise SQLException("In your SQL statement, you must provide username or user_id or org_account")
            else:
                return

        if 'user_id' in column_names:

            user_ids = [row['user_id'] for row in result_proxy if row['user_id'] is not None]

        elif 'og_account' in column_names:

            og_account = [row['og_account'] for row in result_proxy if row['og_account'] is not None]
            result_proxy = db.session.query(BIUser.user_id).filter(BIUser.og_account.in_(og_account)).all()
            user_ids = [row[0] for row in result_proxy]

        elif 'username' in column_names:

            username = [row['username'] for row in result_proxy if row['username'] is not None]
            result_proxy = db.session.query(BIUser.user_id).filter(BIUser.username.in_(username)).all()
            user_ids = [row[0] for row in result_proxy]

        else:
            user_ids = []
        return user_ids

    def generate_recipients(self, all_users):

        """
        根据user_id生成不同类型的推送内容
        """
        user_ids = self.__class__.extract_user_ids(self.based_query_id, self.based_sql, for_check=False,
                                                   all_users=all_users)
        data = BIUser.promotion_recipients(user_ids, self.push_type)

        return data

    #### 更新状态 #####

    def update_status(self, status):
        self.status = status
        db.session.flush()
        db.session.commit()

    #### 发布 #####

    def publish_to_push_history(self, cohort_id=None):
        scheduled_at = arrow.get(self.scheduled_at)
        worker = self.__class__._worker[self.push_type]

        try:

            self.update_status(PROMOTION_PUSH_STATUSES.PREPARING.value)

            if self.based_query_rules:
                rows = UsersGrouping.generate_recipients(self.based_query_rule, self.push_type)

            elif self.based_query_id or self.based_sql:

                rows = self.generate_recipients(all_users=False)

            else:
                rows = self.generate_recipients(all_users=True)

            if not rows:
                self.update_status(PROMOTION_PUSH_STATUSES.SCHEDULED.value)

                return

            status = PROMOTION_PUSH_HISTORY_STATUSES.SCHEDULED.value

            if self.push_type == PROMOTION_PUSH_TYPES.MOBILE.value:

                rows = [{'push_id': self.id,
                         'push_type': self.push_type,
                         'user_id': row['user_id'],
                         'target': row['endpoint_arn'],
                         'celery_worker': choice(range(1, worker + 1)),
                         'scheduled_at': scheduled_at,
                         'is_automated': self.is_automated,
                         'rearm_count': self.rearm_count,
                         'status': status,
                         } for row in rows]

                db.get_engine(db.get_app(), bind='bi_promotion').execute(PromotionPushHistory.__table__.insert(), rows)

                self.update_status(PROMOTION_PUSH_STATUSES.SCHEDULED.value)

                # 并发的下发任务，也可以使用for循环。这里指的并发，并不是所有任务一起执行，而是所有任务都下发到队列，而执行的并发数量，取决于work的数量。
                if app.config['ENV'] == 'prod':
                    for i in range(1, worker + 1):
                        mobile_promotion_push.apply_async(kwargs=dict(push_id=self.id, worker=i), queue='mobile')

                else:
                    for i in range(1, worker + 1):
                        mobile_promotion_push(push_id=self.id, worker=i)


            elif self.push_type == PROMOTION_PUSH_TYPES.EMAIL.value:

                cohort_name = db.session.query(AutoPromotionPush.cohort_name).filter(
                    AutoPromotionPush.id == cohort_id).scalar()
                for row in rows:
                    row['cohort_name'] = cohort_name

                rows = [{'push_id': self.id,
                         'push_type': self.push_type,
                         'user_id': row['user_id'],
                         'target': json.dumps(row),
                         'is_automated': self.is_automated,
                         'rearm_count': self.rearm_count,
                         'celery_worker': choice(range(1, worker + 1)),
                         'scheduled_at': scheduled_at,
                         'status': status,
                         } for row in rows]

                db.get_engine(db.get_app(), bind='bi_promotion').execute(PromotionPushHistory.__table__.insert(), rows)

                self.update_status(PROMOTION_PUSH_STATUSES.SCHEDULED.value)

                if app.config['ENV'] == 'prod':
                    for i in range(1, worker + 1):
                        email_promotion_push.apply_async(kwargs=dict(worker=i, push_id=self.id), queue='email')
                else:

                    for i in range(1, worker + 1):
                        email_promotion_push(worker=i, push_id=self.id)


            elif self.push_type == PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value:

                rows = [{'push_id': self.id,
                         'push_type': self.push_type,
                         'user_id': row['user_id'],
                         'target': row['facebook_id'],
                         'is_automated': self.is_automated,
                         'rearm_count': self.rearm_count,
                         'celery_worker': choice(range(1, worker + 1)),
                         'scheduled_at': scheduled_at,
                         'status': status,
                         } for row in rows]

                db.get_engine(db.get_app(), bind='bi_promotion').execute(PromotionPushHistory.__table__.insert(), rows)

                self.update_status(PROMOTION_PUSH_STATUSES.SCHEDULED.value)

                if app.config['ENV'] == 'prod':
                    for i in range(1, worker + 1):
                        facebook_promotion_push.apply_async(kwargs=dict(worker=i, push_id=self.id),
                                                            queue='facebook')
                else:
                    for i in range(1, worker + 1):
                        facebook_promotion_push(worker=i, push_id=self.id)

            else:
                return

        except Exception as e:

            print('process_promotion_{}_items exception: '.format(self.push_type) + error_msg_from_exception(e))

            self.update_status(PROMOTION_PUSH_STATUSES.FAILED.value)

    #### 重试 #####

    def retry(self):
        status_values = [PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value]

        db.session.query(PromotionPushHistory).filter_by(push_id=self.id) \
            .filter(PromotionPushHistory.status.in_(status_values)) \
            .update({PromotionPushHistory.status: PROMOTION_PUSH_STATUSES.SCHEDULED.value}, synchronize_session=False)

        db.session.commit()

        #### 统计 #####

    def display_message(self):

        if self.push_type == PROMOTION_PUSH_TYPES.MOBILE.value or self.push_type == PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value:

            message = json.loads(self.message)

            title = message.get('title')
            body = base64.b64decode(message.get('body')).decode()

            if title:

                title = base64.b64decode(title).decode()

                message = '<pre>' + '<b>' + title + '</b><br>' + body + '</pre>'

            else:
                message = body

        else:

            message = self.message

        return message

    def based_query_sql(self):
        if not self.based_query_id:
            return None

        query = db.session.query(AdminUserQuery).filter_by(id=self.based_query_id).first()
        if query:
            return query.sql

        return None

    def to_dict(self):

        if self.status == PROMOTION_PUSH_STATUSES.SCHEDULED.value:
            total_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text("SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id"), push_id=self.id).scalar()
            running_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text("SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND status='running'"),
                push_id=self.id).scalar()
            succeed_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text("SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND status='success'"),
                push_id=self.id).scalar()
            request_failed_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND status='request_failed'"),
                push_id=self.id).scalar()
            failed_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text("SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND status='failed'"),
                push_id=self.id).scalar()

        return {
            'id': self.id,
            'requested_by': self.requested_by,
            'based_query_id': self.based_query_id,
            'based_query_sql': self.based_query_sql(),
            'based_query_rules': self.based_query_rules,
            'push_type': self.push_type,
            'scheduled_at': arrow.get(self.scheduled_at).to(app.config['APP_TIMEZONE']).format(),
            'status': self.status if self.status != PROMOTION_PUSH_STATUSES.SCHEDULED.value else {
                'total_count': total_count,
                'running_count': running_count,
                'succeed_count': succeed_count,
                'request_failed_count': request_failed_count,
                'failed_count': failed_count
            },

            'message': self.display_message(),
            'created_at': self.created_at
        }

    def to_dict_for_cohort(self, count):

        if self.status == PROMOTION_PUSH_STATUSES.SCHEDULED.value:
            scheduled_at = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT MAX(scheduled_at) FROM promotion_push_history WHERE push_id = :push_id AND rearm_count = :rearm_count"),
                push_id=self.id, rearm_count=count).scalar()

            total_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND rearm_count = :rearm_count"),
                push_id=self.id, rearm_count=count).scalar()

            running_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND rearm_count = :rearm_count AND status='running' "),
                push_id=self.id, rearm_count=count).scalar()
            succeed_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND rearm_count = :rearm_count AND status='success'"),
                push_id=self.id, rearm_count=count).scalar()
            failed_count = db.get_engine(db.get_app(), bind='bi_promotion').execute(
                text(
                    "SELECT COUNT(*) FROM promotion_push_history WHERE push_id = :push_id AND rearm_count = :rearm_count AND status='failed'"),
                push_id=self.id, rearm_count=count).scalar()

            return {
                'scheduled_at': arrow.get(scheduled_at).to(app.config['APP_TIMEZONE']).format(),
                'status': self.status if self.status != PROMOTION_PUSH_STATUSES.SCHEDULED.value else {
                    'total_count': total_count,
                    'running_count': running_count,
                    'succeed_count': succeed_count,
                    'failed_count': failed_count
                },
            }


class PromotionPushHistory(db.Model):
    __bind_key__ = 'bi_promotion'
    id = db.Column(db.BIGINT, primary_key=True)
    push_id = db.Column(db.Integer, nullable=False, index=True)
    push_type = db.Column(db.String(50), nullable=False)
    user_id = db.Column(db.BIGINT, index=True)
    # facebook_id 或者email 或者 aws sns的endpoint_arn
    target = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(50))
    # 属于这个cohort的第几次触发
    rearm_count = db.Column(db.Integer, nullable=False, default=0)
    # 是否是cohort的自动化模式
    is_automated = db.Column(db.Boolean, nullable=False, default=False)
    error_message = db.Column(db.Text)
    # 此条记录的计划发送时间

    celery_worker = db.Column(db.Integer, nullable=False, index=True, default=1)
    scheduled_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)
    created_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)

    __table_args__ = (
        Index('ix_push_id_and_status', 'push_id', 'status'), Index('ix_push_type_and_status', 'push_type', 'status'))

    @staticmethod
    def update_status(data, status_value):
        where = PromotionPushHistory.__table__.c.id == bindparam('_id')

        if status_value == PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value:
            values = {'status': status_value, 'error_message': bindparam('error_message')}
        else:
            values = {'status': status_value, 'error_message': None}

        db.get_engine(db.get_app(), bind='bi_promotion').execute(
            PromotionPushHistory.__table__.update().where(where).values(values), data)

    @classmethod
    def send_email(cls, worker, push_id, batch=500):

        ## 获取发送内容 ########
        email_campaign = json.loads(db.session.query(PromotionPush).get(push_id).message)
        print('process_promotion_email: preparing')

        while True:

            pending_items = db.session.query(PromotionPushHistory.id, PromotionPushHistory.target).filter_by(
                push_type=PROMOTION_PUSH_TYPES.EMAIL.value, push_id=push_id, celery_worker=worker,
                status=PROMOTION_PUSH_HISTORY_STATUSES.SCHEDULED.value).limit(batch).all()

            if not pending_items:
                print('process_promotion_email: no data')
                break

            else:

                PromotionPushHistory.update_status([{'_id': item.id} for item in pending_items],
                                                   PROMOTION_PUSH_HISTORY_STATUSES.RUNNING.value)

                print('process_promotion_email : update  {}  items of  push_id = {}  to  running status'.format(
                    len(pending_items), push_id))

                # 根据发送记录id更新状态，根据target推送
                push_history_ids = [item[0] for item in pending_items]
                targets = [json.loads(item[1]) for item in pending_items]
                id_to_target_mapping = dict(zip(push_history_ids, targets))

                def generate_personalizations():
                    personalizations = []
                    send_time = current_time().format('YYYY/MM/DD HH:mm:ss')
                    for id in push_history_ids:
                        target = id_to_target_mapping[id]
                        personalizations.append({"to": [{'email': target.get('email')}],

                                                 "custom_args": {
                                                     "user_id": str(target.get('user_id', '')),
                                                     "cohort_name": str(target.get('cohort_name', '')),
                                                     "send_time": str(send_time),
                                                     "push_id": str(push_id),
                                                     "push_history_id": str(id),
                                                 },
                                                 "substitutions": {"-country-": target.get("country"),
                                                                   "-email-": target.get("email"),
                                                                   "-email_validated_url-": generate_email_validate_url(
                                                                       target.get("user_id")),
                                                                   "-Play_Username-": target.get("display_name")}})
                    return personalizations

                def build_email_campaign(email_campaign, personalizations):

                    email_campaign['personalizations'] = personalizations

                    #  ubsubscribe
                    email_content = email_campaign["content"][0]["value"]

                    # custom field

                    pattern = re.compile(r'\[%.*?%\]')
                    custom_fields = list(re.findall(pattern, email_content))
                    custom_fields_format = ['[%' + (field.split('%')[1]).split(' ')[0] + '%]' for field in
                                            custom_fields]
                    for i in range(0, len(custom_fields)):
                        email_content = email_content.replace(custom_fields[i], custom_fields_format[i])

                    email_content = email_content. \
                        replace("[Unsubscribe]", '<%asm_group_unsubscribe_raw_url%>'). \
                        replace("[Weblink]", "https://www.playwpt.com"). \
                        replace("[Unsubscribe_Preferences]", '<%asm_preferences_raw_url%>'). \
                        replace("[%country%]", "-country-"). \
                        replace("[%Play_Username%]", "-Play_Username-"). \
                        replace("[%email%]", "-email-")

                    email_campaign["content"][0]["value"] = email_content

                    # ubsubscribe
                    suppression = {"group_id": 2161, "groups_to_display": [2161]}
                    email_campaign['asm'] = suppression

                    return email_campaign

                personalizations = generate_personalizations()
                data = build_email_campaign(email_campaign, personalizations)

                try:

                    print('process_promotion_email: start sending')

                    publish_status = {'failed': [], 'success': []}
                    sendgrid.client.mail.send.post(request_body=data)

                except Exception as e:

                    print('process_promotion_email: ' + error_msg_from_exception(e))
                    error_message = error_msg_from_exception(e)
                    publish_status['failed'] = [{'_id': id, 'error_message': error_message} for id in push_history_ids]
                else:
                    publish_status['success'] = [{'_id': id} for id in push_history_ids]


                finally:

                    db.session.commit()

                    if publish_status['success']:
                        PromotionPushHistory.update_status(publish_status['success'],
                                                           PROMOTION_PUSH_HISTORY_STATUSES.SUCCESS.value)

                    if publish_status['failed']:
                        PromotionPushHistory.update_status(publish_status['failed'],
                                                           PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value)

                    print('process_promotion_email: {} request success have {}'.format(len(pending_items),
                                                                                       len(publish_status['success'])))
                    print('process_promotion_email: {} request failed have {}'.format(len(pending_items),
                                                                                      len(publish_status['failed'])))

        print('process_promotion_email: push_id = ' + str(push_id) + ' done')

    @classmethod
    def send_facebook(cls, worker, push_id=None):
        print('process_promotion_facebook_notification: preparing')
        now = current_time().format('YYYY-MM-DD HH:mm:ss')

        request_rate_limit_timing = redis_conn.get('fb_api_rate_limit')
        if request_rate_limit_timing is not None:
            interval = datetime.datetime.strptime(now, '%Y-%m-%d %H:%M:%S') - datetime.datetime.strptime(
                request_rate_limit_timing, '%Y-%m-%d %H:%M:%S')
            if interval.total_seconds() < 1800:
                print('process_promotion_facebook_notification: Request delay')
                return

        ######此处通过push_id这样进行判断，是因为以下原有：
        # 1. facebook 有限流。而且每次都发送给所有用户， 不能像mobile 和email 那也While True 一直发送没为止， 因为可能超速，或者程序长时间执行丢失的问题，重启采用定期轮循环，长时间发送为佳
        # 2. 当自动化推送时，如果同一时间并发多个活动到default队列，然后活动又通过，publish_to_history函数通过worker（worker数量可调）并发到facebook队列,此时Facebook队列中不同并发执行的程序
        # 还必须通过pushid来隔离

        if push_id is not None:
            push_histories = db.session.query(PromotionPushHistory).filter_by(
                celery_worker=worker, push_id=push_id,
                push_type=PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value,
                status=PROMOTION_PUSH_HISTORY_STATUSES.SCHEDULED.value) \
                .filter(PromotionPushHistory.scheduled_at <= now).limit(4000).all()


        else:
            push_histories = db.session.query(PromotionPushHistory).filter_by(
                celery_worker=worker,
                push_type=PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value,
                status=PROMOTION_PUSH_HISTORY_STATUSES.SCHEDULED.value) \
                .filter(PromotionPushHistory.scheduled_at <= now).limit(4000).all()

        if len(push_histories) == 0:
            print('process_promotion_facebook_notification: no data')
            return

        fb_name_identifier = '@[fb_name]'

        push_ids = [item.push_id for item in push_histories]
        pushes = db.session.query(PromotionPush).filter(PromotionPush.id.in_(list(set(push_ids)))).all()

        parse_message = lambda message: base64.b64decode(json.loads(message)["body"]).decode()
        id_to_message_mapping = {item.id: parse_message(item.message) for item in pushes}

        id_to_fb_name_replace_mapping = {item.id: fb_name_identifier in parse_message(item.message) for item in pushes}

        print('process_promotion_facebook_notification: start sending')
        PromotionPushHistory.update_status([{'_id': item.id} for item in push_histories],
                                           PROMOTION_PUSH_HISTORY_STATUSES.RUNNING.value)

        iter_step = 50
        for i in range(0, len(push_histories), iter_step):
            partitions = push_histories[i:i + iter_step]
            partition_ids = [item.id for item in partitions]
            data = {
                'access_token': '122212108221118|iY2aFJsHhyWUvTV_4oeoGrXX-TA',
                'include_headers': 'false',
                'batch': json.dumps([{'method': 'POST',
                                      'relative_url': 'v2.7/' + item.target + '/notifications',
                                      'body': parse.urlencode({'template': id_to_message_mapping[
                                          item.push_id].replace(fb_name_identifier, '@[%s]' % item.target) if
                                      id_to_fb_name_replace_mapping[item.push_id] else id_to_message_mapping[
                                          item.push_id]})} for item in partitions])
            }

            try:
                req = requests.post('https://graph.facebook.com', data=data)
            except Exception as e:
                print('process_promotion_facebook_notification send request exception: ' + error_msg_from_exception(e))
                PromotionPushHistory.update_status([{'_id': partition_id} for partition_id in partition_ids],
                                                   PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value)
            else:
                if req:

                    print('process_promotion_facebook_notification: starting process response.')

                    succeeded_partition_data = []
                    failed_partition_data = []
                    responses = req.json()

                    print(req.headers)
                    ### https://developers.facebook.com/docs/graph-api/advanced/rate-limiting
                    if 'X-App-Usage' in req.headers:
                        now = current_time().format('YYYY-MM-DD HH:mm:ss')
                        redis_conn.set('fb_api_rate_limit', now)

                    for idx, response in enumerate(responses):
                        push_history_id = str(partition_ids[idx])
                        resp_body = json.loads(response['body'])


                        if 'error' in resp_body:
                            failed_partition_data.append(
                                {'_id': push_history_id, 'error_message': resp_body['error']['message'].split('.')[0]})

                        else:
                            succeeded_partition_data.append({'_id': push_history_id})

                    print('process_promotion_facebook_notification succeeded count: ' + str(
                        len(succeeded_partition_data)) + ', failed count: ' + str(len(failed_partition_data)))

                    if len(succeeded_partition_data) > 0:
                        PromotionPushHistory.update_status(succeeded_partition_data,
                                                           PROMOTION_PUSH_HISTORY_STATUSES.SUCCESS.value)

                    if len(failed_partition_data) > 0:
                        PromotionPushHistory.update_status(failed_partition_data,
                                                           PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value)
                else:
                    print('process_promotion_facebook_notification: no response.')

        print('process_promotion_facebook_notification: done')

    @classmethod
    def send_mobile(cls, push_id, worker, batch=1000):

        ## 获取发送内容 ########
        message = json.loads(db.session.query(PromotionPush.message).filter(PromotionPush.id == push_id).one()[0])

        print('process_promotion_mobile: preparing')

        while True:

            # 获取用户发送地址

            disabled_arn = []
            pending_items = db.session.query(PromotionPushHistory.id, PromotionPushHistory.target).filter_by(
                push_type=PROMOTION_PUSH_TYPES.MOBILE.value, push_id=push_id, celery_worker=worker,
                status=PROMOTION_PUSH_HISTORY_STATUSES.SCHEDULED.value).limit(batch).all()

            if not pending_items:

                # 结束发送
                print('process_promotion_mobile: no data')
                break

            else:

                PromotionPushHistory.update_status([{'_id': item.id} for item in pending_items],
                                                   PROMOTION_PUSH_HISTORY_STATUSES.RUNNING.value)

                # 更新为running，防止并发重复
                print('process_promotion_email : update  {}  items of  push_id = {}  to  running status'.format(
                    len(pending_items), push_id))

                publish_status = {'failed': [], 'success': []}
                # 根据发送记录id更新状态，根据target推送
                for id, target in pending_items:

                    try:

                        def send_to_aws_sns(target, message):
                            title = base64.b64decode(message.get('title')).decode()
                            event_type = message.get('event_type')
                            body = base64.b64decode(message.get('body')).decode()

                            ios_payload = {'aps': {'content-available': 1, 'sound': 'default', 'alert': {'body': body}}}
                            android_payload = {"notification": {"body": body}}

                            if title:
                                ios_payload['aps']['alert'].update({'title': title})
                                android_payload['notification'].update({'title': title})

                                # -1为默认界面。无响应事件
                            if int(event_type) != -1:
                                ios_payload.update({'data': {'event_type': event_type}})
                                android_payload.update({'data': {'event_type': event_type}})

                            payload = {'default': body,
                                       'APNS': json.dumps(ios_payload),
                                       'gcm': json.dumps(android_payload)}

                            message = json.dumps(payload)

                            aws_sns_client.publish(TargetArn=target, Message=message, MessageStructure='json')

                        send_to_aws_sns(target, message)

                    except Exception as e:

                        print('process_promotion_mobile: ' + error_msg_from_exception(e))
                        publish_status['failed'].append({'_id': id, 'error_message': error_msg_from_exception(e)})
                        disabled_arn.append({'endpoint_arn': target})
                    else:

                        publish_status['success'].append({'_id': id})

                #####必须提交此会话 #####
                db.session.commit()

                # 更新状态发送响应状态， 统计发送结果

                if publish_status['success']:
                    PromotionPushHistory.update_status(publish_status['success'],
                                                       PROMOTION_PUSH_HISTORY_STATUSES.SUCCESS.value)

                if publish_status['failed']:
                    PromotionPushHistory.update_status(publish_status['failed'],
                                                       PROMOTION_PUSH_HISTORY_STATUSES.FAILED.value)

                if disabled_arn:
                    db.get_engine(db.get_app(), bind='bi_promotion').execute(DisabledARN.__table__.insert(),
                                                                             disabled_arn)

                print('process_promotion_mobile: {} request success have {}'.format(len(pending_items),
                                                                                    len(publish_status['success'])))
                print('process_promotion_mobile: {} request failed have {}'.format(len(pending_items),
                                                                                   len(publish_status['failed'])))

        print('process_promotion_mobile: push_id = ' + str(push_id) + ' done')


################################


class EmailEvent(db.Model):
    id = db.Column(db.BIGINT, primary_key=True)

    user_id = db.Column(db.String(255), index=True)
    cohort_name = db.Column(db.String(50), index=True)

    push_id = db.Column(db.String(50))
    push_history_id = db.Column(db.String(50))

    sg_user_id = db.Column(db.Integer)
    marketing_campaign_id = db.Column(db.Integer)
    marketing_campaign_name = db.Column(db.String(255))

    event = db.Column(db.String(50))
    email = db.Column(db.String(50))
    response = db.Column(db.String(255))
    category = db.Column(db.String(50))
    asm_group_id = db.Column(db.Integer)

    ip = db.Column(db.String(255))
    smtp_id = db.Column(db.String(255))
    url = db.Column(db.String(255))
    useragent = db.Column(db.String(255))

    tls = db.Column(db.String(255))
    cert_err = db.Column(db.String(255))

    url_offset = db.Column(db.String(255))
    types = db.Column(db.String(255))

    sg_event_id = db.Column(db.String(255))
    sg_message_id = db.Column(db.String(255))

    status = db.Column(db.String(255))
    reason = db.Column(db.String(255))
    attempt = db.Column(db.String(255))

    marketing_campaign_version = db.Column(db.String(255))
    marketing_campaign_split_id = db.Column(db.Integer)

    # 发送邮件时间
    send_time = db.Column(AwareDateTime, index=True)
    # 时间相应时间
    timestamp = db.Column(AwareDateTime, index=True)
    created_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)


class BasicProperty(object):
    @classmethod
    def reg_time(cls, field, operator, value):
        sql = """ SELECT user_id FROM( SELECT user_id, DATE_FORMAT(reg_time,'%Y-%m-%d') AS reg_time FROM bi_user )t """

        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def birthday(cls, field, operator, value):
        sql = 'SELECT user_id FROM bi_user WHERE birthday'

        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def reg_state(cls, field, operator, value):
        sql = "SELECT user_id FROM bi_user WHERE reg_state = :value"

        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id


################################

class GameBehaviour(BasicProperty):
    @classmethod
    def x_days_inactive(cls, field, operator, value):
        sql = """
                SELECT DISTINCT  user_id 
                FROM   bi_user
                WHERE  user_id NOT IN (SELECT DISTINCT user_id
                                       FROM   bi_user_currency
                                       WHERE  DATE(created_at)> DATE_ADD(CURDATE(), INTERVAL - :value DAY)
                                              AND transaction_type NOT IN (20132001, 999998301, 925011306, 
                                              925011307, 925011410, 925011411, 30007777, 925011311, 923118301, 
                                              923118302, 923118303, 923118304, 923118311, 923118312, 923118313, 
                                              923118314))
              """

        result_proxy = list(db.engine.execute(text(sql), value=value))
        user_id = [item[0] for item in result_proxy]

        return user_id

    @classmethod
    def average_active_days_weekly(cls, field, operator, value):
        sql = """

                SELECT user_id,  average_active_days_weekly FROM (
                SELECT c.user_id AS user_id,FLOOR(Count(DISTINCT DATE(c.created_at)) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 7)) AS average_active_days_weekly
                FROM   bi_user u
                       INNER JOIN bi_user_currency c
                               ON u.user_id = c.user_id
                GROUP BY c.user_id 
                )t
             """
        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def average_active_days_monthly(cls, field, operator, value):
        sql = """
                SELECT user_id , average_active_days_monthly FROM (
                SELECT c.user_id AS user_id,FLOOR(Count(DISTINCT DATE(c.created_at)) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 30)) AS average_active_days_monthly
                FROM   bi_user u
                       INNER JOIN bi_user_currency c
                               ON u.user_id = c.user_id

                GROUP BY c.user_id 
                ) t 
             """
        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id


class PaidBehaviour(GameBehaviour):
    @classmethod
    def never_purchased_users(cls, field, operator, value):
        sql = """
                SELECT user_id
                FROM   bi_user
                WHERE  user_id NOT IN (SELECT DISTINCT user_id
                                       FROM   bi_user_bill) 
              """

        result_proxy = list(db.engine.execute(text(sql)))
        user_id = [item[0] for item in result_proxy]

        return set(user_id)

    @classmethod
    def purchased_users(cls, field, operator, value):
        sql = """ SELECT DISTINCT user_id FROM bi_user_bill """

        result_proxy = db.engine.execute(text(sql))
        user_id = [item[0] for item in result_proxy]

        return user_id

    @classmethod
    def x_days_not_purchase(cls, field, operator, value):
        sql = """
                SELECT DISTINCT  user_id 
                FROM   bi_user_bill
                WHERE  user_id NOT IN (SELECT DISTINCT user_id
                                       FROM   bi_user_bill
                                       WHERE  DATE(created_at) > :value
                                              AND currency_type = 'Dollar') 
               """

        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def average_purchase_amount_monthly(cls, field, operator, value):
        sql = """ 
                SELECT user_id , average_purchase_amount_monthly FROM(
                SELECT b.user_id AS user_id,FLOOR(  SUM(currency_amount) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 30)) AS average_purchase_amount_monthly
                FROM   bi_user u
                       INNER JOIN bi_user_bill b
                               ON u.user_id = b.user_id
                WHERE b.currency_type = 'Dollar'
                GROUP BY b.user_id 
                ) t
               """

        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def average_purchase_amount_weekly(cls, field, operator, value):
        sql = """
                SELECT user_id ,average_purchase_amount_weekly FROM(
                SELECT b.user_id AS user_id, FLOOR(SUM(currency_amount) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 7)) AS average_purchase_amount_weekly
                FROM   bi_user u
                       INNER JOIN bi_user_bill b
                               ON u.user_id = b.user_id
                WHERE b.currency_type = 'Dollar'
                GROUP BY b.user_id 
                ) t
              """
        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def average_purchase_count_monthly(cls, field, operator, value):
        sql = """
                SELECT user_id, average_purchase_count_monthly FROM (
                SELECT b.user_id AS user_id, FLOOR(Count(DISTINCT DATE(b.created_at)) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 30)) AS average_purchase_count_monthly
                FROM   bi_user u
                       INNER JOIN bi_user_bill b
                               ON u.user_id = b.user_id
                WHERE b.currency_type = 'Dollar'
                GROUP BY b.user_id 
                ) t
         """
        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id

    @classmethod
    def average_purchase_count_weekly(cls, field, operator, value):
        sql = """
                SELECT  average_purchase_count_weekly FROM (
                SELECT b.user_id,FLOOR(Count(DISTINCT DATE(b.created_at)) / (DATEDIFF(CURDATE(), DATE
                (u.reg_time)) / 7)) AS average_purchase_count_weekly
                FROM   bi_user u
                       INNER JOIN bi_user_bill b
                               ON u.user_id = b.user_id
                WHERE b.currency_type = 'Dollar'
                GROUP BY b.user_id ) t
         """
        user_id = with_db_context(db, sql_filter_option, sql=sql, field=field, operator=operator, value=value)

        return user_id


class UsersGrouping(PaidBehaviour):
    @classmethod
    def parse_query_rules(cls, rules):

        field = rules["field"]
        operator = rules["operator"]
        try:
            value = int(rules["value"])
        except:
            value = rules["value"]

        if field == 'purchased' and value == 1:
            field = "purchased_users"
        if field == 'purchased' and value == 0:
            field = "never_purchased_users"

        fields = {"reg_time": super(UsersGrouping, cls).reg_time, "reg_state": super(UsersGrouping, cls).reg_state,
                  "birthday": super(UsersGrouping, cls).birthday,
                  "x_days_inactive": super(UsersGrouping, cls).x_days_inactive,
                  "average_active_days_weekly": super(UsersGrouping, cls).average_active_days_weekly,
                  "average_active_days_monthly": super(UsersGrouping, cls).average_active_days_monthly,
                  "purchased_users": super(UsersGrouping, cls).purchased_users,
                  "never_purchased_users": super(UsersGrouping, cls).never_purchased_users,
                  "X_days_not_purchase": super(UsersGrouping, cls).x_days_inactive,
                  "average_purchase_count_monthly": super(UsersGrouping, cls).average_purchase_count_monthly,
                  "average_purchase_count_weekly": super(UsersGrouping, cls).average_purchase_count_weekly,
                  "average_purchase_amount_monthly": super(UsersGrouping, cls).average_purchase_amount_monthly,
                  "average_purchase_amount_weekly": super(UsersGrouping, cls).average_purchase_amount_weekly}

        user_id = fields[field](field, operator, value)

        return set(user_id)

    @classmethod
    def get_user_id(cls, query_rules):

        condition = query_rules["condition"]
        rules = query_rules["rules"]

        def get_child_query_rules(rules):

            if "condition" in rules:

                return set(cls.get_user_id(rules))

            else:

                child_query_user_id = cls.parse_query_rules(rules)

                return child_query_user_id

        user_id = list(map(get_child_query_rules, rules))

        if condition == "AND":

            query_result = list(reduce(iand, user_id))

        else:

            query_result = list(reduce(ior, user_id))

        return query_result

    @classmethod
    def generate_recipients(cls, query_rules, notification_type):

        def get_user(query_rules, notification_type):

            user_ids = cls.get_user_id(query_rules)

            if user_ids:

                if notification_type == PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value:
                    result_proxy = db.get_engine(app, bind='orig_wpt').execute(text(
                        """ SELECT u_id AS user_id  ,pu_id  AS platform_user_id FROM  tb_platform_user_info WHERE u_id IN :user_ids """),
                        user_ids=tuple(user_ids))

                    return result_proxy

                elif notification_type == PROMOTION_PUSH_TYPES.EMAIL.value:
                    result_proxy = db.engine.execute(text(
                        """ SELECT user_id,  display_name,reg_country,reg_state,email  FROM bi_user WHERE user_id IN :user_ids """),
                        user_ids=tuple(user_ids))
                    return result_proxy

                else:

                    return []
            else:
                return []

        if notification_type == PROMOTION_PUSH_TYPES.FB_NOTIFICATION.value:
            result_proxy = get_user(query_rules=query_rules, notification_type=notification_type)

            recipients = [[row['user_id'], row['platform_user_id']] for row in result_proxy if
                          row['platform_user_id'] is not None]

            return recipients

        if notification_type == PROMOTION_PUSH_TYPES.EMAIL.value:
            result_proxy = get_user(query_rules=query_rules, notification_type=notification_type)

            recipients = [
                {'user_id': row['user_id'], 'display_name': row['display_name'], 'country': row['reg_country'],
                 'email': row['email']} for row in result_proxy if row['email'] is not None]

            return recipients


def sql_filter_option(connection, transaction, sql, field, operator, value):
    if operator == "less":

        result_proxy = connection.execute(text(sql + 'WHERE ' + field + '< :value'), value=value)

    elif operator == "equal":

        result_proxy = connection.execute(text(sql + 'WHERE ' + field + '= :value'), value=value)

    elif operator == "greater":

        result_proxy = connection.execute(text(sql + 'WHERE ' + field + '> :value'),
                                          value=value)
    else:

        result_proxy = connection.execute(text(sql + 'WHERE ' + field + ' BETWEEN :value1 AND :value2'),
                                          value1=value[0], value2=value[1])

    user_ids = list([item[0] for item in result_proxy])

    return user_ids
