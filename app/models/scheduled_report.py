from datetime import timedelta

import os
import pandas as pd
from flask import current_app as app
from sqlalchemy import text

from app.extensions import db
from app.libs.datetime_type import AwareDateTime
from app.tasks import celery
from app.tasks.mail import send_mail
from app.utils import current_time


@celery.task
def auto_report():
    ScheduledReport.check_auto_report()


@celery.task
def report_async(id):
    timezone_offset = app.config['APP_TIMEZONE']
    now = current_time(timezone_offset)
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')
    report = db.session.query(ScheduledReport).filter_by(id=id).one()
    title = report.report_name

    database = report.database
    if database == 'bi': database = None
    result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(report.sql))

    column_names = [col[0] for col in result_proxy.cursor.description]

    recipients = report.recipients.split(',')
    report_data = result_proxy.fetchall()

    email_subject = now.format('MM-DD-YYYY') + ' ' + report.report_name

    filename = email_subject + '.CSV'

    path = os.path.join(app.config["REPORT_FILE_FOLDER"], filename)
    result = pd.DataFrame(pd.DataFrame(report_data, columns=column_names))

    with open(path, 'w+') as f:
        result.to_csv(f, sep=',', encoding='utf-8')

    report.update_cohort_status()

    send_mail(to=recipients, subject=email_subject, template='cron_daily_report',
                                      attachment_content_type='text/csv', attachment=path, filename=filename,
                                      column_names=column_names, report_data=report_data[:50], title=title,
                                      generated_at=generated_at,
                                      auto=True)


class ScheduledReport(db.Model):
    __tablename__ = 'scheduled_report'

    id = db.Column(db.Integer, primary_key=True)

    report_name = db.Column(db.String(50), nullable=False, index=True)

    sql = db.Column(db.Text)

    database = db.Column(db.String(255))

    recipients = db.Column(db.Text)

    description = db.Column(db.Text, nullable=False)

    maximum_count = db.Column(db.Integer, nullable=False, default=0)

    remaining_count = db.Column(db.Integer, nullable=False, default=maximum_count)

    rearm = db.Column(db.Integer, nullable=False, index=True)

    scheduled_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)

    is_paused = db.Column(db.Boolean, nullable=False, default=False)

    is_activated = db.Column(db.Boolean, nullable=False, default=False)

    created_at = db.Column(AwareDateTime, default=current_time, nullable=False, index=True)

    def update_cohort_status(self):

        scheduled_at = self.scheduled_at.to('UTC')

        if self.remaining_count - 1 != 0:
            self.scheduled_at = (scheduled_at + timedelta(hours=self.rearm)).to('UTC')

        self.remaining_count -= 1

        db.session.flush()
        db.session.commit()
        print('update report status')

    def should_notify(self):

        scheduled_at = self.scheduled_at.to('UTC')

        start = current_time().to('-00:30').format('YYYY-MM-DD HH:mm:ss')
        now = current_time().format('YYYY-MM-DD HH:mm:ss')

        triggered_times = []

        def generate_triggered_time(scheduled_at, remaining_count):
            for i in range(remaining_count):
                trigger_time = (scheduled_at + timedelta(hours=self.rearm)).to('UTC')
                scheduled_at = trigger_time
                triggered_times.append(scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

            return triggered_times

        triggered_times = generate_triggered_time(scheduled_at, self.remaining_count)
        triggered_times.insert(0, scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

        for triggered_time in triggered_times:
            if start <= triggered_time <= now:

                if not self.is_paused and self.is_activated:
                    return True
                else:
                    self.update_cohort_status()
                    return False

    @classmethod
    def alive_ids(cls):
        ids = db.session.query(ScheduledReport.id).filter(ScheduledReport.remaining_count > 0).all()
        ids = [row[0] for row in ids]
        return ids

    @classmethod
    def get_report(cls, report_name):

        report = db.session.query(ScheduledReport).filter(ScheduledReport.report_name == report_name)
        return report

    @classmethod
    def check_auto_report(cls):

        ids = cls.alive_ids()

        for id in ids:

            report = db.session.query(ScheduledReport).filter(ScheduledReport.id == id).one()

            if report.should_notify():

                report_name = report.report_name

                print('Start To Process  Automated Report For {} '.format(report_name))

                if app.config['ENV'] == 'prod':

                    report_async.apply_async((report.id,), queue='default')

                else:

                    report_async(report.id)

                print('Update Automated Report For {} '.format(report_name))
