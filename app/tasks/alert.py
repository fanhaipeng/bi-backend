import datetime

import os
from sqlalchemy import text

import app.settings
from app.extensions import db
from app.tasks import celery


class Alert(object):
    _wechat = app.settings.WECHAT

    now = datetime.datetime.utcnow()

    _message = {'new_reg': '超过{}个小时没有新注册的用户了,可能存在异常',
                'bill': '超过{}个小时没有新的订单了,可能存在异常',
                'free_lucky_spin': '超过{}个小时没有人转过转盘了, 能存在异常',
                'poker': '超过{}个小时没有人玩过扑克了,可能存在异常', }

    @classmethod
    def poker_exception(cls, threshold):
        check_sql = "SELECT last_poker_time FROM bi_user ORDER BY last_poker_time DESC LIMIT 1"
        last_poker_time = db.get_engine(db.get_app()).execute(text(check_sql)).scalar()
        interval = (cls.now - last_poker_time).total_seconds()

        should_alarm = interval > threshold * 3600

        if should_alarm:
            cls.call_wechat(Alert._message['poker'].format(round(interval / 3600, 2)))

    @classmethod
    def new_reg_exception(cls, threshold):
        check_sql = """
                SELECT  reg_time
                FROM bi_user
                ORDER BY reg_time DESC
                LIMIT 1
                """
        last_new_reg_time = db.get_engine(db.get_app()).execute(text(check_sql)).scalar()
        interval = (cls.now - last_new_reg_time).total_seconds()

        should_alarm = interval > threshold * 3600

        if should_alarm:
            cls.call_wechat(Alert._message['new_reg'].format(round(interval / 3600, 2)))

    @classmethod
    def bill_exception(cls, threshold):
        check_sql = """
                    SELECT created_at
                    FROM bi_user_bill
                    WHERE currency_type ='Dollar'
                    ORDER BY created_at DESC LIMIT 1
                    """
        last_bill_time = db.get_engine(db.get_app()).execute(text(check_sql)).scalar()
        interval = (cls.now - last_bill_time).total_seconds()

        should_alarm = interval > threshold * 3600

        if should_alarm:
            cls.call_wechat(Alert._message['bill'].format(round(interval / 3600, 2)))

    @classmethod
    def free_lucky_spin_exception(cls, threshold):
        check_sql = "SELECT last_free_spin_time FROM bi_user ORDER BY last_free_spin_time DESC LIMIT 1"
        last_free_lucky_spin = db.get_engine(db.get_app()).execute(text(check_sql)).scalar()
        interval = (cls.now - last_free_lucky_spin).total_seconds()
        should_alarm = interval > threshold * 3600

        if should_alarm:
            cls.call_wechat(Alert._message['free_lucky_spin'].format(round(interval / 3600, 2)))

    @classmethod
    def call_wechat(cls, message):
        shell_scripts = '/bin/bash ' + cls._wechat + ' bi {}'.format(message)
        os.system(shell_scripts)


@celery.task
def check_alert():
    sql = """
        SELECT var,
           Round(( ( Convert_tz(Now(), '+00:00', '-8:00') - last_synced_at ) / 10000
                 ) * 60
           , 0) AS 'minutes'
    FROM   bi_import_config
    WHERE  var != 'last_imported_clubwpt_user_add_time'
           AND var != 'last_imported_promotion_history_id'
           AND var != 'last_imported_user_mall_bill_detail_order_update_time'
           AND var != 'last_imported_user_mall_bill_order_update_time'
           AND ( ( Convert_tz(Now(), '+00:00', '-8:00') - last_synced_at ) / 10000 )
           * 60 > 15
        """

    have_sync_issue = db.get_engine(db.get_app()).execute(text(sql)).fetchall()

    if not have_sync_issue:
        Alert.poker_exception(0.3)
        Alert.new_reg_exception(0.3)
        Alert.bill_exception(1)
        Alert.free_lucky_spin_exception(1)
