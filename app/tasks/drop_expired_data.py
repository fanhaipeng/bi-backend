from sqlalchemy import text, func

from app import db, BIUserCurrency
from app.models.bi import BIUserCurrencyLifeTime
from app.tasks import celery, get_config_value, set_config_value_with_db_instance
from app.tasks import with_db_context
from app.utils import current_time
from flask import current_app as app


@celery.task
def process_expired_data_for_bi_user_currency():
    timezone_offset = app.config['APP_TIMEZONE']
    expired_date = current_time(timezone_offset).replace(days=-90).format('YYYY-MM-DD')
    config_value = get_config_value(db, 'last_sync_bi_user_currency_for_lifetime')

    def collection(connection, transaction):

        if config_value is None:
            return connection.execute(text("""
                                           SELECT * FROM  bi_user_currency
                                           ORDER BY  created_at ASC
                                           LIMIT   50000000
                                               """))
        else:
            return connection.execute(text("""
                                           SELECT * FROM  bi_user_currency WHERE created_at >= :config_value
                                           ORDER BY  created_at ASC
                                           LIMIT   50000000
                                           """), config_value=config_value)

    result_proxy = with_db_context(db, collection)

    ########    #

    existed_data = []
    if config_value is not None:
        new_data = []
        new_ids = []
        for row in result_proxy:
            new_data.append(row)
            new_ids.append(row['id'])

        def existed_collection(connection, transaction):
            return connection.execute(text(
                "SELECT id FROM bi_user_currency_lifetime WHERE  id IN :new_ids"),
                new_ids=tuple(new_ids))

        existed_result_proxy = with_db_context(db, existed_collection, bind='bi_lifetime')
        existed_data = [row['id'] for row in existed_result_proxy]
        print('process_bi_user_currency_for_lifetime existed data: ' + str(len(existed_data)))

    #########

    proxy = new_data if config_value is not None else result_proxy

    rows = [dict(row.items()) for row in proxy if row['id'] not in existed_data]

    print('collection_new_data_for_bi_user_currency_liftime: ' + str(len(rows)))

    if rows:
        for i in range(0, len(rows), 100000):
            group = rows[i:i + 100000]
            new_config_value = group[-1]['created_at']

            def drop_expired_data_and_sync_new_data_for_bi_user_currency():
                try:
                    db.session.execute(BIUserCurrencyLifeTime.__table__.insert(), group)
                    db.session.query(BIUserCurrency).filter(func.Date(
                        func.convert_tz(BIUserCurrency.created_at, '+00:00', timezone_offset)) <= expired_date).delete(
                        synchronize_session=False)
                except:
                    print('drop_expired_data_and_sync_for_bi_user_currency_for_lifetime transaction.rollback()')
                    db.session.rollback()
                    raise
                else:
                    print('drop_expired_data_and_sync_for_bi_user_currency_for_lifetime transaction.commit()')
                    set_config_value_with_db_instance(db, 'last_sync_bi_user_currency_for_lifetime', new_config_value)
                    db.session.commit()

            drop_expired_data_and_sync_new_data_for_bi_user_currency()
