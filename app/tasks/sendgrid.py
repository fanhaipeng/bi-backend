import json

import requests
from flask import current_app as app
from sqlalchemy.sql.expression import bindparam, text

import app.settings
from app import BIUser
from app.constants import POKER_TRANSACTION_TYPES
from app.extensions import sendgrid, redis_conn, db
from app.models.bi import BriteverifyLog
from app.tasks import celery
from app.utils import error_msg_from_exception, current_time



@celery.task
def get_campaigns():
    try:
        response = sendgrid.client.campaigns.get(query_params={'limit': 999999, 'offset': 0})
        parsed_response = json.loads(response.body.decode())
        campaigns = parsed_response['result']
        redis_conn.set('campaigns', json.dumps(campaigns))
        return campaigns
    except Exception as e:
        print('get_campaigns exception: ' + error_msg_from_exception(e))
        return []


@celery.task
def get_senders():
    try:
        response = sendgrid.client.senders.get()
        senders = json.loads(response.body.decode())
        redis_conn.set('senders', json.dumps(senders))
        return senders
    except Exception as e:
        print('get_sender exception: ' + error_msg_from_exception(e))
        return []


@celery.task
def check_email():
    timezone_offset = app.settings.APP_TIMEZONE
    yesterday = current_time(timezone_offset).replace(days=-1).format('YYYY-MM-DD')

    def update_user_status(emails, flag_type, email_promotion_allowed=1, banned=False):

        if flag_type == 'sendgrid':
            where = BIUser.__table__.c.flag_type == 'sendgrid'
            values = {'email_promotion_allowed': 1}
            db.engine.execute(BIUser.__table__.update(where).values(values))
            db.session.commit()

        rows = [{'_email': email} for email in emails]
        where = BIUser.__table__.c.email == bindparam('_email')
        values = {'email_promotion_allowed': email_promotion_allowed, 'flag_type': flag_type}

        if banned:
            values.update({'account_status_orig': 5, 'account_status': 'Banned'})

        try:
            db.engine.execute(BIUser.__table__.update().where(where).values(values), list(rows))
            db.session.commit()

            print('Process daily check email :{}  update user status transaction.commit()'.format(flag_type))
        except Exception as e:
            db.session.rollback()

            print('Process daily check email :{}  update user status exception: '.format(
                flag_type) + error_msg_from_exception(e))

    def sync_suppressed_users_from_sendgrid():

        response = sendgrid.client.asm.suppressions.get(query_params={'limit': 999999, 'offset': 1})
        emails = [user['email'] for user in json.loads(response.body.decode())]

        return emails

    def wash_users_based_on_briteverify(user):

        user_id = user[0]
        email = user[1]
        result = dict(valid=True, disposable=False, role_address=False)

        session = requests.session()
        data = dict(address=email, apikey=app.settings.BRITEVERIFY_API_KEY)
        try:

            response = session.get('https://bpi.briteverify.com/emails.json', data=data, timeout=10)
            response.raise_for_status()
            email_info = response.json()
            email_info['user_id'] = user_id

            db.session.execute(BriteverifyLog.__table__.insert(), email_info)
            db.session.commit()

            result.update(dict(valid=email_info['status'] != 'invalid', disposable=email_info['disposable'],
                               role_address=email_info['role_address']))

        except Exception as e:

            print('Process daily check email : briteverify cleaning emails  exception: ' + error_msg_from_exception(
                e))
            return False

        is_invalid = not (result['valid'] and not result['disposable'] and not result['role_address'])

        return is_invalid

    unverified_users = db.engine.execute(text("""
                                            SELECT user_id,
                                                   email
                                            FROM   bi_user
                                            WHERE  DATE(CONVERT_TZ(reg_time, '+00:00', :timezone_offset)) = :yesterday
                                                   AND account_status_orig = 2 
                              """), yesterday=yesterday, timezone_offset=timezone_offset,
                                         poker_transaction_types=POKER_TRANSACTION_TYPES)
    unverified_users = [(row['user_id'], row['email']) for row in unverified_users]

    suppressed_emails = sync_suppressed_users_from_sendgrid()
    update_user_status(suppressed_emails, email_promotion_allowed=0, flag_type='sendgrid')

    invalid_users = list(filter(wash_users_based_on_briteverify, unverified_users))
    invalid_emails = [user[1] for user in invalid_users]
    update_user_status(invalid_emails, email_promotion_allowed=0, banned=True, flag_type='briteverify')

    return None
