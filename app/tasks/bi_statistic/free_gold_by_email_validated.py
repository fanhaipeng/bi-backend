import json

import arrow
from sqlalchemy import text, and_
from sqlalchemy.sql.expression import bindparam

from app.extensions import db
from app.models.bi import BIStatistic
from app.tasks import with_db_context
from app.utils import generate_sql_date


# 对于新用户，如果输入了名字，选择了头像，并且账号状态是Email_validated，应该送免费25k金币.
# 此脚本用于检测是否有发送失败的情况

def process_bi_statistic_about_send_free_gold_failure(target):
    _, someday, _, timezone_offset = generate_sql_date(target)
    start_index_time = arrow.get(someday).replace(days=-3).format('YYYY-MM-DD')

    if target == 'lifetime':
        return

    def collection_have_user_name(connection, transaction):
        return connection.execute(text("""
                                            SELECT user_id
                                            FROM   bi_user
                                            WHERE  account_status = 'Email validated'
                                                   AND username IS NOT NULL
                                                   AND DATE(CONVERT_TZ(reg_time, '+00:00', :timezone_offset)) = :on_day
                                          """), timezone_offset=timezone_offset, on_day=someday)

    result_proxy = with_db_context(db, collection_have_user_name)
    user_ids_have_user_name = [row['user_id'] for row in result_proxy]

    def collection_have_avatar_and_username(connection, transaction):
        return connection.execute(text("""
                                           SELECT user_id FROM avatar_user_assign WHERE user_id IN  :user_ids
                                           """), timezone_offset=timezone_offset, on_day=someday,
                                  user_ids=tuple(user_ids_have_user_name))

    result_proxy = with_db_context(db, collection_have_avatar_and_username, bind='orig_wpt')
    user_ids_have_avatar_and_username = [row['user_id'] for row in result_proxy]

    def collection_send_free_gold_failure(connection, transaction):
        return connection.execute(text("""
                                          SELECT DISTINCT user_id AS user_id
                                               FROM   bi_user_currency
                                               WHERE  transaction_type = 999998301
                                               AND  created_at > :start_index_time 
                                          """), timezone_offset=timezone_offset, start_index_time=start_index_time)

    result_proxy = with_db_context(db, collection_send_free_gold_failure)
    send_free_gold_user_ids = [row['user_id'] for row in result_proxy]

    # 存在这样一种可能：

    # 一个用户当时注册了，我们获得了注册时间，但是没有输入名字，也没有选择头像，
    # 但是在几天后，他又返回来，重新来注册，输入了名字，选择了头像，当我们统计是否送了金币时，bi_user_currency有恰好在同步维护
    # 但这中情况较小，所以估算应该小于１５
    failure_user_ids = list(set(user_ids_have_avatar_and_username) - set(send_free_gold_user_ids))

    if len(failure_user_ids) > 15:

        failure_user_ids = json.dumps(failure_user_ids)
        rows = [{'_on_day': someday, 'send_free_gold_failure': failure_user_ids}]

        def sync_collection_send_free_gold_failure(connection, transaction):

            where = and_(BIStatistic.__table__.c.on_day == bindparam('_on_day'),
                         BIStatistic.__table__.c.game == 'All Game',
                         BIStatistic.__table__.c.platform == 'All Platform')
            values = {'send_free_gold_failure': bindparam('send_free_gold_failure')}

            try:
                connection.execute(BIStatistic.__table__.update().where(where).values(values), rows)
            except:
                print(target + ' send_free_gold_failure_transaction.rollback()')
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print(target + ' send_free_gold_failure transaction.commit()')

        with_db_context(db, sync_collection_send_free_gold_failure)
