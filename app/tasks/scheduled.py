from flask import current_app as app

from app.extensions import db
from app.tasks import celery
from app.tasks.alert import check_alert
from app.tasks.bi_clubwpt_user import process_bi_clubwpt_user
from app.tasks.bi_statistic import process_bi_statistic
from app.tasks.bi_user import process_bi_user
from app.tasks.bi_user_bill import process_bi_user_bill
from app.tasks.bi_user_bill_detail import process_bi_user_bill_detail
from app.tasks.bi_user_currency import process_bi_user_currency
from app.tasks.bi_user_statistic import process_bi_user_statistic
from app.tasks.cron_daily_report import daily_report_dau, daily_report_game_table_statistic, daily_report_ccu, \
    daily_report_revenue_for_MyGameRoom, daily_report_new_reg_for_MyGameRoom
from app.tasks.drop_expired_data import process_expired_data_for_bi_user_currency
from app.tasks.sendgrid import get_campaigns, get_senders, check_email
from app.tasks.sync_wpt_bi import process_wpt_bi_user_statistic
from app.utils import error_msg_from_exception


#### Alarm ############


@celery.task
def run_alert_task():
    try:
        check_alert()
    except Exception as e:
        print(error_msg_from_exception(e))


#### REPORT ############

@celery.task
def daily_report():
    process_bi_statistic_for_yesterday()
    daily_report_dau()
    daily_report_ccu()
    daily_report_game_table_statistic()
    daily_report_new_reg_for_MyGameRoom()
    daily_report_revenue_for_MyGameRoom()


@celery.task
def update_email_promotion_allowed():
    check_email()
    db.session.close()


####  ETL  ############

@celery.task
def process_expired_data():
    process_expired_data_for_bi_user_currency()
    db.session.close()


@celery.task
def process_bi():
    process_bi_user()
    process_bi_user_bill()
    process_bi_user_bill_detail()

    if app.config['ENV'] == 'prod':
        process_bi_clubwpt_user()
        db.session.close()


@celery.task
def process_bi_currency():
    if app.config['ENV'] == 'prod':
        process_bi_user_currency()
        db.session.close()


@celery.task
def process_wpt_bi():
    process_wpt_bi_user_statistic()
    db.session.close()


@celery.task
def process_bi_statistic_for_yesterday():
    process_bi_statistic('yesterday')
    db.session.close()


@celery.task
def process_bi_statistic_for_today():
    process_bi_statistic('today')
    db.session.close()


@celery.task
def process_bi_user_statistic_for_yesterday():
    process_bi_user_statistic('yesterday')


##### OTHERS ######
@celery.task
def sync_sendgrid_campaign():
    get_campaigns()
    get_senders()
