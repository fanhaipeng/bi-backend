from itertools import groupby

from arrow import Arrow
from operator import itemgetter
from sqlalchemy import text, and_
from sqlalchemy.sql.expression import bindparam

from app.extensions import db
from app.models.bi import BIUserStatisticSharding
from app.tasks import with_db_context
from app.utils import generate_sql_date, get_timezone_offset_of_from_Beijing_to_EST

timezone_offset_of_from_Beijing_to_EST = get_timezone_offset_of_from_Beijing_to_EST()

def process_bi_user_statistic_dau(target):
    _, someday, _, timezone_offset = generate_sql_date(target)
    someday_arrow = Arrow.strptime(someday, '%Y-%m-%d')
    start_time = someday_arrow.replace(hours=timezone_offset_of_from_Beijing_to_EST).format('YYYY-MM-DD HH:mm:ss')
    end_time = someday_arrow.replace(days=1, hours=timezone_offset_of_from_Beijing_to_EST).format('YYYY-MM-DD HH:mm:ss')

    def collection_user_ring_game_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT username                                                   AS
                                                            username,
                                                           TO_CHAR(CAST(CAST(cgz.time_update AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  AS 
                                                            stats_date
                                            FROM   OGDZWEB.tj_cgz_flow_userpaninfo
                                           """))
        else:

            return connection.execute(text("""
                                            SELECT DISTINCT username AS username
                                            FROM   OGDZWEB.tj_cgz_flow_userpaninfo
                                            WHERE  time_update <= TO_DATE(:end_time, 'yyyy-mm-dd,hh24:mi:ss')
                                                   AND time_update >= TO_DATE(:start_time, 'yyyy-mm-dd,hh24:mi:ss')
                                           """), stats_date=someday, end_time=end_time, start_time=start_time)

    result_proxy = with_db_context(db, collection_user_ring_game_dau, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_username': row['username']} for row in result_proxy]

    else:

        rows = [{'_stats_date': someday, '_username': row['username']} for row in result_proxy]

    if rows:

        def sync_collection_user_ring_game_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.username == bindparam('_username'))

            values = {'ring_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about ring game dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about ring game dau transaction.commit()'.format(str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_ring_game_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')

    def collection_user_sng_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT r.username                                              AS
                                                            og_account,
                                                            TO_CHAR(CAST(CAST(cgz.endtime AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  AS 
                                                            stats_date
                                            FROM    OGDZWEB.usermatchrecord r
                                                   INNER JOIN tj_matchinfo m
                                                           ON r.matchid = m.matchid
                                            WHERE  m.type = 1
                                           """))
        else:

            return connection.execute(text("""
                                            SELECT DISTINCT r.username AS og_account
                                            FROM     OGDZWEB.usermatchrecord r
                                                   INNER JOIN OGDZWEB.tj_matchinfo m
                                                           ON r.matchid = m.matchid
                                            WHERE  m.type = 1
                                                   AND r.endtime >= TO_DATE(:start_time, 'yyyy-mm-dd,hh24:mi:ss')
                                                   AND r.endtime <= TO_DATE(:end_time, 'yyyy-mm-dd,hh24:mi:ss')
                                           """), stats_date=someday, end_time=end_time, start_time=start_time)

    result_proxy = with_db_context(db, collection_user_sng_dau, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_og_account': row['og_account']} for row in result_proxy if
                row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_og_account': row['og_account']} for row in result_proxy]

    if rows:

        def sync_collection_user_sng_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.og_account == bindparam('_og_account'))

            values = {'sng_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about sng dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about sng dau transaction.commit()'.format(str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_sng_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')

    def collection_user_mtt_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT username                                              AS
                                                            og_account,
                                                            TO_CHAR(CAST(CAST(r.endtime AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  AS 
                                                            stats_date
                                            FROM      OGDZWEB.usermatchrecord r
                                                   INNER JOIN tj_matchinfo m
                                                           ON r.matchid = m.matchid
                                            WHERE  m.type = 2 
                                           """), timezone_offset=timezone_offset)
        else:
            return connection.execute(text("""
                                            SELECT DISTINCT r.username AS og_account
                                            FROM   OGDZWEB.usermatchrecord r
                                                   INNER JOIN     OGDZWEB.tj_matchinfo m
                                                           ON r.matchid = m.matchid
                                            WHERE  m.type = 2
                                                   AND r.endtime >= TO_DATE(:start_time, 'yyyy-mm-dd,hh24:mi:ss')
                                                   AND r.endtime <= TO_DATE(:end_time, 'yyyy-mm-dd,hh24:mi:ss')
                                           """), stats_date=someday, end_time=end_time, start_time=start_time)

    result_proxy = with_db_context(db, collection_user_mtt_dau, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_og_account': row['og_account']} for row in result_proxy if
                row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_og_account': row['og_account']} for row in result_proxy]

    if rows:

        def sync_collection_user_mtt_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.og_account == bindparam('_og_account'))

            values = {'mtt_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about mtt dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about mtt dau transaction.commit()'.format(str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_mtt_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')

    def collection_user_store_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT user_id                                                  AS
                                                            user_id,
                                                            DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) AS
                                                            stats_date
                                            FROM   bi_user_bill 
                                           """), timezone_offset=timezone_offset)
        else:

            return connection.execute(text("""
                                            SELECT DISTINCT user_id AS user_id
                                            FROM   bi_user_bill
                                            WHERE  DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) = :stats_date 
                                           """), stats_date=someday, timezone_offset=timezone_offset)

    result_proxy = with_db_context(db, collection_user_store_dau)

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_user_id': row['user_id']} for row in result_proxy if
                row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_user_id': row['user_id']} for row in result_proxy]

    if rows:

        def sync_collection_user_store_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

            values = {'store_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about store dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about store dau transaction.commit()'.format(str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_store_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')

    def collection_user_slots_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT username                                              AS
                                                            og_account,
                                                            TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  AS 
                                                            stats_date
                                            FROM   gl.gamecoin_detail 
                                            """), timezone_offset=timezone_offset)
        else:

            return connection.execute(text("""
                                            SELECT DISTINCT username AS og_account
                                            FROM   gl.gamecoin_detail
                                            WHERE TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  = :stats_date 
                                            """), stats_date=someday, timezone_offset=timezone_offset)

    result_proxy = with_db_context(db, collection_user_slots_dau, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_og_account': row['og_account']} for row in result_proxy if
                row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_og_account': row['og_account']} for row in result_proxy]

    if rows:

        def sync_collection_user_slots_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)
            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.og_account == bindparam('_og_account'))

            values = {'slots_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about slots dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                print('process bi_user_statistic_{} about slots dau transaction.commit()'.format(str(stats_date)))
                transaction.commit()

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_slots_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')

    def collection_user_promotion_dau(connection, transaction):

        if target == 'lifetime':

            return connection.execute(text("""
                                            SELECT DISTINCT  user_id                                        AS 
                                                             user_id,
                                                   DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) AS 
                                                             stats_date
                                            FROM   bi_user_bill
                                            WHERE  is_promotion = 1
                                           """), timezone_offset=timezone_offset)
        else:

            return connection.execute(text("""
                                            SELECT DISTINCT  user_id                 AS user_id
                                            FROM   bi_user_bill
                                            WHERE  is_promotion = 1
                                                   AND DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) =
                                                       :stats_date  
                                           """), stats_date=someday, timezone_offset=timezone_offset)

    result_proxy = with_db_context(db, collection_user_promotion_dau)

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_user_id': row['user_id']} for row in result_proxy if
                row['stats_date'] if not None]

    else:

        rows = [{'_stats_date': someday, '_user_id': row['user_id']} for row in result_proxy]

    if rows:

        def sync_collection_promotion_dau(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

            values = {'promo_dau': 1}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print('process bi_user_statistic_{} about promotion dau transaction.rollback()'.format(str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about promtion dau transaction.commit()'.format(str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_promotion_dau, stats_date=str(stats_date), group_rows=group_rows,
                            bind='bi_user_statistic')
