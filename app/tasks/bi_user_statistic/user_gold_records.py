from datetime import date
from itertools import groupby

import pandas as pd
from operator import itemgetter
from sqlalchemy import bindparam, and_
from sqlalchemy import text

from app.constants import GOLD_FREE_TRANSACTION_TYPES_TUPLE
from app.extensions import db
from app.models.bi import BIUserStatisticSharding
from app.tasks import with_db_context
from app.utils import generate_sql_date


def process_bi_user_statistic_gold_spend_detail(target):
    today, someday, _, timezone_offset = generate_sql_date(target)

    product_category_column_name_mapping = {'Avatar Set': 'avatar_spend', 'Poker Lucky Charm Set': 'charms_spend',
                                            'Emoji Set': 'emoji_spend'}

    def collection_user_gold_spend_detail_records(connection, transaction, category, day):

        return connection.execute(text("""
                                        SELECT user_id,
                                               SUM(currency_amount) AS spend_amount
                                        FROM   bi_user_bill
                                        WHERE  currency_type = 'gold'
                                               AND goods_orig IN (SELECT goods_orig
                                                                  FROM   bi_user_bill_detail
                                                                  WHERE  category = :category)
                                               AND DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) =
                                                   :stats_date
                                        GROUP  BY user_id 
                                           """), stats_date=day, timezone_offset=timezone_offset, category=category)

    def collection_table_gift_records(connection, transaction, day):

        return connection.execute(text("""
                                        SELECT username,
                                               SUM(each_gift_spend) AS table_gift_spend
                                        FROM
                                          (SELECT u.from_user username,
                                                  COUNT(u.itemcode)* t.price each_gift_spend
                                           FROM OGDZWEB.prop_user_table_gift u
                                           INNER JOIN OGDZWEB.prop_table_gift t ON u.itemcode = t.itemcode
                                           AND TO_CHAR(CAST(CAST(u.create_time AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :stats_date
                                           GROUP BY u.from_user,
                                                    t.price) t
                                        GROUP BY username
                                       """), stats_date=day)

    def collection_user_gold_spend_records():

        table_gift_spend_result_proxy = []
        gold_spend_detail_records_result_proxy = []

        if target == 'lifetime':

            for day in sorted(pd.date_range(date(2016, 6, 1), today), reverse=True):

                day = day.strftime("%Y-%m-%d")

                for category in ['Poker Lucky Charm Set', 'Avatar Set', 'Emoji Set']:
                    each_category_sales_records = with_db_context(db, collection_user_gold_spend_detail_records,
                                                                  day=day, category=category)
                    each_category_sales_records_rows = [
                        {'_stats_date': str(day), '_user_id': row['user_id'], 'spend_amount': row['spend_amount']} for
                        row in each_category_sales_records]
                    each_category_sales_records_dict = dict([(category, each_category_sales_records_rows)])

                    gold_spend_detail_records_result_proxy.append(each_category_sales_records_dict)

            for day in sorted(pd.date_range(date(2016, 6, 1), today), reverse=True):
                day = day.strftime("%Y-%m-%d")

                table_gift_records = with_db_context(db, collection_table_gift_records, bind='orig_wpt_ods', day=day)
                table_gift_records_rows = [
                    {'_stats_date': str(day), '_username': row['username'], 'table_gift_spend': row['table_gift_spend']}
                    for row in table_gift_records]

                table_gift_spend_result_proxy.append(table_gift_records_rows)

        else:

            for category in ['Poker Lucky Charm Set', 'Avatar Set', 'Emoji Set']:
                each_category_sales_records = with_db_context(db, collection_user_gold_spend_detail_records,
                                                              day=someday, category=category)
                each_category_sales_records_rows = [
                    {'_stats_date': str(someday), '_user_id': row['user_id'], 'spend_amount': row['spend_amount']} for
                    row in each_category_sales_records]
                each_category_sales_records_dict = dict([(category, each_category_sales_records_rows)])
                gold_spend_detail_records_result_proxy.append(each_category_sales_records_dict)

            table_gift_records = with_db_context(db, collection_table_gift_records, day=someday, bind='orig_wpt_ods')
            table_gift_records_rows = [
                {'_stats_date': str(someday), '_username': row['username'], 'table_gift_spend': row['table_gift_spend']}
                for row in table_gift_records]
            table_gift_spend_result_proxy.append(table_gift_records_rows)

        return gold_spend_detail_records_result_proxy, table_gift_spend_result_proxy

    gold_spend_detail_records_result_proxy, table_gift_spend_result_proxy = collection_user_gold_spend_records()

    if gold_spend_detail_records_result_proxy:

        for every_category_sales_record_rows in gold_spend_detail_records_result_proxy:

            for category_name, rows in every_category_sales_record_rows.items():

                if rows:

                    def sync_collection_user_gold_spend_records_transaction(connection, transaction, stats_date,
                                                                            group_rows):

                        someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

                        where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                     someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                        values = {product_category_column_name_mapping[category_name]: bindparam('spend_amount')}

                        try:
                            connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                               list(group_rows))
                        except:

                            print('process bi_user_statistic_{} about gold spend on {} transaction.rollback()'.format(
                                str(stats_date), category_name))
                            transaction.rollback()
                            raise
                        else:
                            print('process bi_user_statistic_{} about gold spend on {} transaction.commit()'.format(
                                str(stats_date), category_name))
                            transaction.commit()

                    rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')),
                                                   itemgetter('_stats_date'))

                    for stats_date, group_rows in rows_group_by_states:
                        with_db_context(db, sync_collection_user_gold_spend_records_transaction,
                                        stats_date=str(stats_date), group_rows=group_rows, bind='bi_user_statistic')

    if table_gift_spend_result_proxy:

        for rows in table_gift_spend_result_proxy:

            if rows:

                def sync_collection_user_table_gift_spend_records_transaction(connection, transaction, stats_date,
                                                                              group_rows):
                    someday_bi_user_statistic = BIUserStatisticSharding.model(str(stats_date))

                    where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                 someday_bi_user_statistic.__table__.c.username == bindparam('_username'))

                    values = {'table_gift_spend': bindparam('table_gift_spend')}

                    try:
                        connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                           list(group_rows))
                    except:

                        print(
                            'process bi_user_statistic_{} about  table gift spend related transaction.rollback()'.format(
                                str(stats_date)))
                        transaction.rollback()
                        raise
                    else:
                        transaction.commit()
                        print('process bi_user_statistic_{} about table gift spend related transaction.commit()'.format(
                            str(stats_date)))

                rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))

                for stats_date, group_rows in rows_group_by_states:
                    with_db_context(db, sync_collection_user_table_gift_spend_records_transaction,
                                    stats_date=str(stats_date), group_rows=group_rows, bind='bi_user_statistic')


def process_bi_user_statistic_convert_to_silver(target):
    today, someday, _, timezone_offset = generate_sql_date(target)

    def collection_user_convert_records(connection, transaction, day):

        return connection.execute(text("""
                                            SELECT user_id,
                                                   SUM(currency_amount) AS convert_amount
                                            FROM   bi_user_bill
                                            WHERE  currency_type = 'gold'
                                                   AND goods_orig IN (SELECT goods_orig
                                                                      FROM   bi_user_bill_detail
                                                                      WHERE  product_orig = 2)
                                                   AND DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) =
                                                       :stats_date
                                            GROUP  BY user_id 
                                           """), stats_date=day, timezone_offset=timezone_offset)

    def get_user_convert_records():

        gold_convert_silver_records_result_proxy = []

        if target == 'lifetime':

            for day in sorted(pd.date_range(date(2016, 6, 1), today), reverse=True):
                day = day.strftime("%Y-%m-%d")

                gold_convert_to_silver_records = with_db_context(db, collection_user_convert_records, day=day)
                gold_convert_to_silver_records_rows = [
                    {'_stats_date': str(day), '_user_id': row['user_id'], 'convert_amount': row['convert_amount']} for
                    row in gold_convert_to_silver_records]

                gold_convert_silver_records_result_proxy.append(gold_convert_to_silver_records_rows)
        else:

            gold_convert_to_silver_records = with_db_context(db, collection_user_convert_records, day=someday)

            gold_convert_to_silver_records_rows = [
                {'_stats_date': str(someday), '_user_id': row['user_id'], 'convert_amount': row['convert_amount']} for
                row in gold_convert_to_silver_records]

            gold_convert_silver_records_result_proxy.append(gold_convert_to_silver_records_rows)

        return gold_convert_silver_records_result_proxy

    gold_convert_silver_records_result_proxy = get_user_convert_records()

    if gold_convert_silver_records_result_proxy:

        for rows in gold_convert_silver_records_result_proxy:

            if rows:

                def sync_collection_user_convert_records_transaction(connection, transaction, stats_date, group_rows):

                    someday_bi_user_statistic = BIUserStatisticSharding.model(str(stats_date))

                    where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                 someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                    values = {'gold_to_silver': bindparam('convert_amount')}

                    try:
                        connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                           list(group_rows))
                    except:

                        print('process bi_user_statistic_{} about gold to silver related transaction.rollback()'.format(
                            str(stats_date)))
                        transaction.rollback()
                        raise
                    else:
                        transaction.commit()

                        print('process bi_user_statistic_{} about gold to silver related transaction.commit()'.format(
                            str(stats_date)))

                rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
                for stats_date, group_rows in rows_group_by_states:
                    with_db_context(db, sync_collection_user_convert_records_transaction, group_rows=group_rows,
                                    stats_date=str(stats_date), bind='bi_user_statistic')


def process_bi_user_statistic_free_gold(target):
    _, someday, index_time, timezone_offset = generate_sql_date(target)

    def collection_user_gold_free_transaction(connection, transaction):
        if target == 'lifetime':
            return connection.execute(text("""
                                            SELECT stats_date,
                                                   og_account,
                                                   sum(gamecoin) AS gold_amount
                                            FROM(
                                            SELECT TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') AS stats_date,
                                                   username AS og_account,
                                                   gamecoin
                                            FROM gl.powergamecoin_detail
                                            WHERE producttype IN (20132001, 999998301, 925011306, 925011307, 925011410, 925011411, 30007777, 925011311, 999998302) )a
                                            GROUP BY og_account,
                                                     stats_date
                                           """), timezone_offset=timezone_offset,
                                      gold_free_transaction_types=GOLD_FREE_TRANSACTION_TYPES_TUPLE)

        else:

            return connection.execute(text("""
                                            SELECT  og_account, sum(gamecoin) AS gold_amount
                                            FROM( SELECT username AS og_account, gamecoin
                                            FROM gl.powergamecoin_detail
                                            WHERE producttype IN (20132001, 999998301, 925011306, 925011307, 925011410, 925011411, 30007777, 925011311, 999998302)
                                            AND TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :stats_date
                                            ) a
                                            GROUP BY og_account
                                           """), timezone_offset=timezone_offset, stats_date=someday,
                                      gold_free_transaction_types=GOLD_FREE_TRANSACTION_TYPES_TUPLE)


    result_proxy = with_db_context(db, collection_user_gold_free_transaction, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_og_account': row['og_account'], 'gold_amount': row['gold_amount']}
                for row in result_proxy if row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_og_account': row['og_account'], 'gold_amount': row['gold_amount']} for row in
                result_proxy]

    if rows:

        def sync_collection_user_gold_free_transaction(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(str(stats_date))

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.og_account == bindparam('_og_account'))

            values = {'free_gold': bindparam('gold_amount')}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:

                print('process bi_user_statistic_{} about free gold related transaction.rollback()'.format(
                    str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print('process bi_user_statistic_{} about free gold related transaction.commit()'.format(
                    str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))

        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_gold_free_transaction, stats_date=str(stats_date),
                            group_rows=group_rows, bind='bi_user_statistic')

    def collection_user_lucky_spin_transaction(connection, transaction):
        if target == 'lifetime':
            return connection.execute(text("""
                                            SELECT og_account, stats_date,
                                                   sum(gamecoin) AS gold_amount
                                            FROM
                                              (SELECT username AS og_account, 
                                              TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') AS stats_date,
                                                      gamecoin
                                               FROM gl.powergamecoin_detail
                                               WHERE producttype = 925011307 ) a
                                            GROUP BY stats_date,  og_account
                                           """), timezone_offset=timezone_offset)

        else:

            return connection.execute(text("""
                                            SELECT og_account,
                                                   sum(gamecoin) AS gold_amount
                                            FROM
                                              (SELECT username AS og_account,
                                                      gamecoin
                                               FROM gl.powergamecoin_detail
                                               WHERE producttype = 925011307
                                                 AND TO_CHAR(CAST(CAST(recdate AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :stats_date
                                                 ) a
                                            GROUP BY og_account
                                            """), timezone_offset=timezone_offset, stats_date=someday)

    result_proxy = with_db_context(db, collection_user_lucky_spin_transaction, bind='orig_wpt_ods')

    if target == 'lifetime':

        rows = [{'_stats_date': row['stats_date'], '_og_account': row['og_account'], 'gold_amount': row['gold_amount']}
                for row in result_proxy if row['stats_date'] is not None]

    else:

        rows = [{'_stats_date': someday, '_og_account': row['og_account'], 'gold_amount': row['gold_amount']} for row in
                result_proxy]

    if rows:

        def sync_collection_user_lucky_spin_transaction(connection, transaction, stats_date, group_rows):

            someday_bi_user_statistic = BIUserStatisticSharding.model(str(stats_date))

            where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                         someday_bi_user_statistic.__table__.c.og_account == bindparam('_og_account'))

            values = {'lucky_spins': bindparam('gold_amount')}

            try:
                connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values),
                                   list(group_rows))
            except:
                print(
                    'process bi_user_statistic_{} about total amount of free gold received by lucky spins transaction.rollback()'.format(
                        str(stats_date)))
                transaction.rollback()
                raise
            else:
                transaction.commit()
                print(
                    'process bi_user_statistic_{} about total amount of free gold received by lucky spins transaction.commit()'.format(
                        str(stats_date)))

        rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
        for stats_date, group_rows in rows_group_by_states:
            with_db_context(db, sync_collection_user_lucky_spin_transaction, stats_date=str(stats_date),
                            group_rows=group_rows, bind='bi_user_statistic')
