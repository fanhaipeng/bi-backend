from datetime import date
from itertools import groupby

import pandas as pd
from operator import itemgetter
from sqlalchemy import bindparam, and_
from sqlalchemy import text

from app.extensions import db
from app.models.bi import BIUserStatisticSharding
from app.tasks import with_db_context
from app.utils import generate_sql_date


def process_bi_user_statistic_dollar_records(target):
    today, someday, _, timezone_offset = generate_sql_date(target)

    product_category_column_name_mapping = {'Lucky Spin Set': 'lucky_spin_spend', 'Gold': 'dollar_gold_pkg_spend',
                                            'Silver Coin': 'dollar_silver_pkg_spend'}

    def collection_user_dollar_spend_detail_records(connection, transaction, category, day):

        return connection.execute(text("""
                                        SELECT user_id,
                                               SUM(currency_amount) AS spend_amount
                                        FROM   bi_user_bill
                                        WHERE  currency_type = 'Dollar'
                                               AND DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) =
                                                   :stats_date
                                               AND goods_orig IN (SELECT goods_orig
                                                                  FROM   bi_user_bill_detail
                                                                  WHERE  category = :category)
                                        GROUP  BY user_id;
                                     """), stats_date=day, timezone_offset=timezone_offset, category=category)

    def confirm_first_recharge_time(connection, transaction, day):

        return connection.execute(text("""
                                        SELECT user_id,
                                               CASE
                                                 WHEN MIN(DATE((CONVERT_TZ(created_at, '+00:00', :timezone_offset))))
                                                      = :stats_date
                                               THEN 1
                                                 ELSE 0
                                               END AS dollar_purchase_1st_time
                                        FROM   bi_user_bill
                                        WHERE  currency_type = 'Dollar'
                                        GROUP  BY user_id;
                                       """), stats_date=day, timezone_offset=timezone_offset)

    def confirm_first_recharge_time_for_gold(connection, transaction, day):

        return connection.execute(text("""
                                        SELECT user_id,
                                               CASE
                                                 WHEN MIN(DATE(( CONVERT_TZ(created_at, '+00:00', :timezone_offset) )))
                                                      = :stats_date
                                               THEN 1
                                                 ELSE 0
                                               END AS dollar_purchase_1st_time_gold
                                        FROM   bi_user_bill
                                        WHERE  currency_type = 'Dollar'
                                               AND goods_orig IN (SELECT goods_orig
                                                                  FROM   bi_user_bill_detail
                                                                  WHERE  category = 'Gold')
                                        GROUP  BY user_id;  
                                       """), stats_date=day, timezone_offset=timezone_offset)

    def collection_user_dollar_recharge_records(connection, transaction, day):

        return connection.execute(text("""
                                        SELECT user_id,
                                               COUNT(*)                       AS dollar_purchase_count,
                                               ROUND(SUM(currency_amount), 2) AS dollar_spend
                                        FROM   bi_user_bill
                                        WHERE  currency_type = 'Dollar'
                                               AND DATE(CONVERT_TZ(created_at, '+00:00', :timezone_offset)) =
                                                   :stats_date
                                        GROUP  BY user_id ;
                                       """), stats_date=day, timezone_offset=timezone_offset)

    def collection_user_dollar_related_records():

        dollar_spend_detail_records_result_proxy = []
        dollar_first_time_records_result_proxy = []
        dollar_first_time_records_for_gold_result_proxy = []
        dollar_recharge_records_result_proxy = []

        if target == 'lifetime':

            for day in sorted(pd.date_range(date(2016, 6, 1), today), reverse=True):
                day = day.strftime("%Y-%m-%d")

                for category in ['Lucky Spin Set', 'Gold', 'Silver Coin', ]:

                    each_category_sale_records = with_db_context(db, collection_user_dollar_spend_detail_records, day=day, category=category)
                    each_category_sale_records_rows = [ {'_stats_date': str(day), '_user_id': row['user_id'], 'spend_amount': row['spend_amount']} for row in each_category_sale_records]
                    each_category_sales_record_dict = dict([(category, each_category_sale_records_rows)])

                    dollar_spend_detail_records_result_proxy.append(each_category_sales_record_dict)

            for day in sorted(pd.date_range(date(2016, 6, 1), today), reverse=True):
                day = day.strftime("%Y-%m-%d")

                the_first_recharge_time = with_db_context(db, confirm_first_recharge_time, day=day)
                the_first_recharge_time_rows = [{'_stats_date': str(day), '_user_id': row['user_id'], 'dollar_purchase_1st_time': row['dollar_purchase_1st_time']} for row in the_first_recharge_time]
                dollar_first_time_records_result_proxy.append(the_first_recharge_time_rows)

                the_first_recharge_for_gold_time = with_db_context(db, confirm_first_recharge_time_for_gold, day=day)
                the_first_recharge_for_gold_time_rows = [{'_stats_date': str(day), '_user_id': row['user_id'], 'dollar_purchase_1st_time_gold': row[ 'dollar_purchase_1st_time_gold']} for row in the_first_recharge_for_gold_time]
                dollar_first_time_records_for_gold_result_proxy.append(the_first_recharge_for_gold_time_rows)

                dollar_recharge_records = with_db_context(db, collection_user_dollar_recharge_records, day=day)
                dollar_recharge_records_rows = [ {'_stats_date': str(day), '_user_id': row['user_id'], 'dollar_spend': row['dollar_spend'], 'dollar_purchase_count': row['dollar_purchase_count']} for row in dollar_recharge_records]
                dollar_recharge_records_result_proxy.append(dollar_recharge_records_rows)

        else:

            for category in ['Lucky Spin Set', 'Gold', 'Silver Coin', ]:

                each_category_sale_records = with_db_context(db, collection_user_dollar_spend_detail_records, day=someday, category=category)
                each_category_sale_records_rows = [ {'_stats_date': str(someday), '_user_id': row['user_id'], 'spend_amount': row['spend_amount']} for row in each_category_sale_records]
                each_category_sales_record_dict = dict([(category, each_category_sale_records_rows)])
                dollar_spend_detail_records_result_proxy.append(each_category_sales_record_dict)

            the_first_recharge_time = with_db_context(db, confirm_first_recharge_time, day=someday)
            the_first_recharge_time_rows = [{'_stats_date': str(someday), '_user_id': row['user_id'], 'dollar_purchase_1st_time': row['dollar_purchase_1st_time']} for row in the_first_recharge_time]
            dollar_first_time_records_result_proxy.append(the_first_recharge_time_rows)

            the_first_recharge_for_gold_time = with_db_context(db, confirm_first_recharge_time_for_gold, day=someday)
            the_first_recharge_for_gold_time_rows = [{'_stats_date': str(someday), '_user_id': row['user_id'], 'dollar_purchase_1st_time_gold': row[ 'dollar_purchase_1st_time_gold']} for row in the_first_recharge_for_gold_time]
            dollar_first_time_records_for_gold_result_proxy.append(the_first_recharge_for_gold_time_rows)

            dollar_recharge_records = with_db_context(db, collection_user_dollar_recharge_records, day=someday)
            ## TODO
            dollar_recharge_records_rows = [ {'_stats_date': str(someday), '_user_id': row['user_id'], 'dollar_spend': row['dollar_spend'], 'dollar_purchase_count': row['dollar_purchase_count']} for row in dollar_recharge_records]
            dollar_recharge_records_result_proxy.append(dollar_recharge_records_rows)

        return dollar_spend_detail_records_result_proxy, dollar_first_time_records_result_proxy, dollar_first_time_records_for_gold_result_proxy, dollar_recharge_records_result_proxy

    dollar_spend_detail_records_result_proxy, dollar_first_time_records_result_proxy, dollar_first_time_records_for_gold_result_proxy, dollar_recharge_records_result_proxy = collection_user_dollar_related_records()

    if dollar_spend_detail_records_result_proxy:

        for category_sales_record_rows in dollar_spend_detail_records_result_proxy:

            for category_name, rows in category_sales_record_rows.items():

                if rows:

                    def sync_collection_dollar_spend_records(connection, transaction, stats_date, group_rows):

                        someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

                        where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                     someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                        values = {product_category_column_name_mapping[category_name]: bindparam('spend_amount')}

                        try:
                            connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values), list(group_rows))

                        except:
                            print('process bi_user_statistic_{} about dollar spend on {} transaction.rollback()'.format(str(stats_date), category_name))
                            transaction.rollback()
                            raise
                        else:
                            print('process bi_user_statistic_{} about dollar spend on {} transaction.commit()'.format(str(stats_date), category_name))
                            transaction.commit()

                    rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
                    for stats_date, group_rows in rows_group_by_states:

                        with_db_context(db, sync_collection_dollar_spend_records, stats_date = str(stats_date), group_rows=group_rows, bind='bi_user_statistic')

    if dollar_first_time_records_result_proxy:

        for rows in dollar_first_time_records_result_proxy:

            if rows:

                def sync_collection_the_first_dollar_recharge_records(connection, transaction, stats_date, group_rows):

                    someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

                    where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                 someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                    values = {'dollar_purchase_1st_time': bindparam('dollar_purchase_1st_time')}

                    try:
                        connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values), list(group_rows))

                    except:
                        print('process bi_user_statistic_{} about dollar first recharge record transaction.rollback()'.format(str(stats_date)))
                        transaction.rollback()
                        raise

                    else:
                        print('process bi_user_statistic_{} about dollar first recharge record transaction.commit()'.format(str(stats_date)))
                        transaction.commit()

                rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
                for stats_date, group_rows in rows_group_by_states:

                    with_db_context(db, sync_collection_the_first_dollar_recharge_records, stats_date = str(stats_date), group_rows=group_rows, bind='bi_user_statistic')

    if dollar_first_time_records_for_gold_result_proxy:

        for rows in dollar_first_time_records_for_gold_result_proxy:

            if rows:
                def sync_collection_the_first_dollar_recharge_for_gold_records(connection, transaction, stats_date, group_rows):

                    someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

                    where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                 someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                    values = {'dollar_purchase_1st_time_gold': bindparam('dollar_purchase_1st_time_gold')}

                    try:
                        connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values), list(group_rows))
                    except:
                        print('process bi_user_statistic_{} about dollar first recharge for gold  record transaction.rollback()'.format(str(stats_date)))
                        transaction.rollback()
                        raise
                    else:
                        print('process bi_user_statistic_{} about dollar first recharge for gold  record transaction.commit()'.format(str(stats_date)))
                        transaction.commit()

                rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
                for stats_date, group_rows in rows_group_by_states:

                    with_db_context(db, sync_collection_the_first_dollar_recharge_for_gold_records, stats_date = str(stats_date), group_rows=group_rows, bind='bi_user_statistic')

    if dollar_recharge_records_result_proxy:

        for rows in dollar_recharge_records_result_proxy:

            if rows:

                def sync_collection_dollar_recharge_records(connection, transaction, stats_date, group_rows):

                    someday_bi_user_statistic = BIUserStatisticSharding.model(stats_date)

                    where = and_(someday_bi_user_statistic.__table__.c.stats_date == bindparam('_stats_date'),
                                 someday_bi_user_statistic.__table__.c.user_id == bindparam('_user_id'))

                    values = {'dollar_purchase_count': bindparam('dollar_purchase_count'), 'dollar_spend': bindparam('dollar_spend')}

                    try:
                        connection.execute(someday_bi_user_statistic.__table__.update().where(where).values(values), list(group_rows))
                    except:
                        print('process bi_user_statistic_{} about dollar recharge  record transaction.rollback()'.format(str(stats_date)))
                        transaction.rollback()
                        raise
                    else:
                        print('process bi_user_statistic_{} about dollar recharge  record transaction.commit()'.format(str(stats_date)))
                        transaction.commit()

                rows_group_by_states = groupby(sorted(rows, key=itemgetter('_stats_date')), itemgetter('_stats_date'))
                for stats_date, group_rows in rows_group_by_states:

                    with_db_context(db, sync_collection_dollar_recharge_records, stats_date = str(stats_date), group_rows=group_rows, bind='bi_user_statistic')


