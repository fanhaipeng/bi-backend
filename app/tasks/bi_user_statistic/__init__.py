import json
import time
from datetime import date

import pandas as pd
from arrow import Arrow
from flask import current_app as app
from sqlalchemy import or_
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

from app import db
from app.extensions import redis_conn
from app.models.bi import BIUserStatistic
from app.tasks import celery
from app.utils import generate_sql_date, row_to_dict
from .user_dau_records import process_bi_user_statistic_dau
from .user_dollar_records import process_bi_user_statistic_dollar_records
from .user_game_records import process_bi_user_statistic_game_records
from .user_gold_records import process_bi_user_statistic_convert_to_silver, process_bi_user_statistic_free_gold, \
    process_bi_user_statistic_gold_spend_detail
from .user_reg_records import process_bi_user_statistic_new_reg


@celery.task
def process_bi_user_statistic(target, dau_related=1, dollar_related=1, game_behaviour_related=1, gold_related=1,
                              reg_related=1):
    start = time.clock()

    today, someday, _, _ = generate_sql_date(target)
    if dau_related:
        process_bi_user_statistic_dau(target)
        print(
            '***** ' + target.capitalize() + ' ***** ' + ' bi_user_statistic users dau related records Done' + '*******')

    if dollar_related:
        process_bi_user_statistic_dollar_records(target)
        print(
            '***** ' + target.capitalize() + ' ***** ' + ' bi_user_statistic dollar related records Done' + ' *******')

    if game_behaviour_related:
        process_bi_user_statistic_game_records(target)
        print(
            '***** ' + target.capitalize() + ' ***** ' + ' bi_user_statistic game beahaviour related records Done' + '*******')

    if gold_related:
        process_bi_user_statistic_gold_spend_detail(target)
        process_bi_user_statistic_convert_to_silver(target)
        process_bi_user_statistic_free_gold(target)

        print('***** ' + target.capitalize() + ' ***** ' + ' bi_user_statistic gold related records Done' + '*******')

    if reg_related:
        process_bi_user_statistic_new_reg(target)
        print('***** ' + target.capitalize() + ' ***** ' + ' bi_user_statistic reg related records Done' + '*******')

    end = time.clock()
    print("used:" + str((end - start) / 60) + 'minutes')

    save_records_to_redis(target, end_date=today, start_date=date(2016, 6, 1))
    save_records_to_bi_user_statistic(target, end_date=today, start_date=date(2016, 6, 1))


@celery.task
def save_records_to_redis(target, end_date, start_date):
    start = time.clock()
    pipe = redis_conn.pipeline(transaction=True)

    AutoBase = automap_base()
    AutoBase.prepare(db.get_engine(app, bind='bi_user_statistic'), reflect=True)
    db_session = sessionmaker(bind=db.get_engine(db.get_app(), bind='bi_user_statistic'))

    today, someday, _, _ = generate_sql_date(target)

    def insert_to_redis(ModelClass):

        query_result = [row_to_dict(row) for row in db_session().query(ModelClass).all()]

        for per_user_record_the_day in query_result:

            ring_dau = per_user_record_the_day['ring_dau']
            mtt_dau = per_user_record_the_day['mtt_dau']
            sng_dau = per_user_record_the_day['sng_dau']
            slots_dau = per_user_record_the_day['slots_dau']

            game_dau = ring_dau + mtt_dau + sng_dau + slots_dau
            purchase = per_user_record_the_day['dollar_spend']

            if game_dau == 0 and purchase == 0.0:
                continue

            user_records = {}

            if game_dau:
                game_dau = 1

                user_records["game_dau"] = game_dau

            if purchase:
                user_records["purchase"] = purchase

            key_prefix = 'BI/user_records/'

            key = key_prefix + str(per_user_record_the_day['user_id'])

            short_date = Arrow.strptime(str(per_user_record_the_day['stats_date']), '%Y-%m-%d').strftime('%y%m%d')

            user_records = ({short_date: json.dumps(user_records)})

            redis_conn.hmset(key, user_records)

    if target == 'lifetime':

        for day in sorted(pd.date_range(start_date, end_date)):

            table_name = 'bi_user_statistic_{}'.format(day.strftime('%Y%m%d'))
            BIUserStatistic_Model_Class = getattr(AutoBase.classes, table_name, None)

            if BIUserStatistic_Model_Class is None:
                continue

            insert_to_redis(BIUserStatistic_Model_Class)

            print('process save records to redis for {}'.format(table_name))

        pipe.execute()

    else:

        table_name = 'bi_user_statistic_{}'.format(Arrow.strptime(someday, '%Y-%m-%d').strftime('%Y%m%d'))
        BIUserStatistic_Model_Class = getattr(AutoBase.classes, table_name, None)

        if BIUserStatistic_Model_Class is None:
            return None

        insert_to_redis(BIUserStatistic_Model_Class)
        print('process save records to redis for {}'.format(table_name))

        pipe.execute()

    end = time.clock()

    print('***** ' + target.capitalize() + ' ***** ' + ' user records have been saved to redis used : ' + str(
        (end - start) / 60) + ' minute *******')


@celery.task
def save_records_to_bi_user_statistic(target, start_date, end_date):
    """
     Integrate the  sharding table contents into a table
     """

    AutoBase = automap_base()
    AutoBase.prepare(db.get_engine(app, bind='bi_user_statistic'), reflect=True)
    db_session = sessionmaker(bind=db.get_engine(db.get_app(), bind='bi_user_statistic'))
    today, someday, _, _ = generate_sql_date(target)

    def insert_to_bi_user_statistic(ModelClass):

        def row_to_dict(row):
            result = {}
            for column in row.__table__.columns:
                if column.name != 'id':
                    result[column.name] = getattr(row, column.name)
            return result

        rows = [row_to_dict(row) for row in db_session().query(ModelClass).filter(or_(ModelClass.ring_dau == 1,
                                                                                      ModelClass.mtt_dau == 1,
                                                                                      ModelClass.sng_dau == 1,
                                                                                      ModelClass.slots_dau == 1,
                                                                                      ModelClass.new_reg == 1,
                                                                                      ModelClass.store_dau == 1)).all()]

        if rows:
            db.session.execute(BIUserStatistic.__table__.insert(), rows)
            db.session.commit()

    if target == 'lifetime':

        for day in sorted(pd.date_range(start_date, end_date)):

            table_name = 'bi_user_statistic_{}'.format(day.strftime('%Y%m%d'))
            BIUserStatistic_Model_Class = getattr(AutoBase.classes, table_name, None)

            if BIUserStatistic_Model_Class is None:
                continue

            insert_to_bi_user_statistic(BIUserStatistic_Model_Class)

            print('process save {} to bi_user_statistic'.format(table_name))

    else:

        table_name = 'bi_user_statistic_{}'.format(Arrow.strptime(someday, '%Y-%m-%d').strftime('%Y%m%d'))
        BIUserStatistic_Model_Class = getattr(AutoBase.classes, table_name, None)

        if BIUserStatistic_Model_Class is None:
            return None

        insert_to_bi_user_statistic(BIUserStatistic_Model_Class)

        print('process save {} to bi_user_statistic'.format(table_name))
