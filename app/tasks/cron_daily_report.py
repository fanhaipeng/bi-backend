import locale
import os
import pandas as pd
from flask import current_app as app
from sqlalchemy import text

from app.constants import DAILY_REPORT_DAU_RECIPIENTS, DAILY_REPORT_CCU_RECIPIENTS, DAILY_REPORT_RECIPIENTS, \
    DAILY_REPORT_MyGameRoom_RECIPIENTS
from app.extensions import db
from app.tasks import celery
from app.tasks.mail import send_mail
from app.utils import current_time


@celery.task
def daily_report_dau():
    now = current_time(app.config['APP_TIMEZONE'])
    today = now.format('YYYY-MM-DD')
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')

    sql = """
         SELECT DATE_FORMAT(a.on_day, '%m/%d/%y')           AS on_day,
               a.new_reg,
               a.new_reg_game_dau,
               a.new_reg_game_dau / a.new_reg              AS reg_to_dau,
               a.dau,
               a.dau - a.new_reg_game_dau                  AS return_dau,
               ( a.dau - a.new_reg_game_dau ) / b.dau_prev AS return_dau_rate,
               a.paid_user_count,
               a.paid_user_count / a.dau                   AS paid_user_rate,
               a.revenue,
               a.revenue / a.paid_user_count               AS ARPPU,
               a.revenue / a.dau                           AS ARPDAU
        FROM   (SELECT on_day,
                       new_reg,
                       new_reg_game_dau,
                       dau,
                       paid_user_count,
                       revenue
                FROM   bi_statistic
                WHERE  on_day >= DATE_ADD(:today, INTERVAL -31 DAY)
                       AND on_day < :today
                       AND game = 'all game'
                       AND platform = 'all platform') a
               INNER JOIN (SELECT on_day,
                                  dau dau_prev
                           FROM   bi_statistic
                           WHERE  on_day >= DATE_ADD(:today, INTERVAL -32 DAY)
                                  AND on_day < DATE_ADD(:today, INTERVAL -1 DAY)
                                  AND game = 'all game'
                                  AND platform = 'all platform') b
                       ON a.on_day = DATE_ADD(b.on_day, INTERVAL 1 DAY)
        ORDER  BY a.on_day DESC  
         """

    result_proxy = db.engine.execute(text(sql), today=today)
    column_names = ['Date', 'New Reg', 'New DAU', 'Reg to DAU', 'DAU', 'Return DAU', '% Return DAU', 'Paid Players',
                    '% Paid', 'Revenue', 'ARPPU', 'ARPDAU']

    last_thirty_days_data = result_proxy.fetchall()
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

    last_thirty_days_data_formatted = [
        [row['on_day'],
         format(row['new_reg'], ',d'),
         format(row['new_reg_game_dau'], ',d'),
         format(row['reg_to_dau'], '0.2%'),
         format(row['dau'], ',d'),
         format(row['return_dau'], ',d'),
         format(row['return_dau_rate'], '0.2%'),
         format(row['paid_user_count'], ',d'),
         format(row['paid_user_rate'], '0.2%'),
         locale.currency(float(format(row['revenue'], '0.2f')), grouping=True),
         locale.currency(float(format(row['ARPPU'], '0.2f')), grouping=True),
         locale.currency(float(format(row['ARPDAU'], '0.2f')), grouping=True),
         ] for row in db.engine.execute(text(sql), today=today)]

    report_data = last_thirty_days_data_formatted[0:7]

    title = 'Daily Report – DAU Related'
    email_subject = now.format('MM-DD-YYYY') + '_DAU_REPORT'
    filename = 'DAU_REPORT.csv'
    path = os.path.join(app.config["REPORT_FILE_FOLDER"], filename)
    result = pd.DataFrame(pd.DataFrame(last_thirty_days_data, columns=column_names))

    with open(path, 'w+') as f:
        result.to_csv(f, sep=',', encoding='utf-8')

    send_mail(to=DAILY_REPORT_DAU_RECIPIENTS, subject=email_subject, template='cron_daily_report', attachment=path,
              attachment_content_type='text/csv', filename=filename, column_names=column_names,
              report_data=report_data, title=title, generated_at=generated_at, DAU=True)


@celery.task
def daily_report_game_table_statistic():
    now = current_time(app.config['APP_TIMEZONE'])
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')

    sql = """
          SELECT  on_hour, table_name, stakes_level, 
                 COUNT(DISTINCT username)                                                 uniq_players,
                 COUNT(DISTINCT pan_id)                                                   uniq_hands_played,
                 COUNT(pan_id)                                                            total_hands_played
          FROM ( SELECT TO_CHAR(CAST(CAST(cgz.time_update AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd hh24')  AS on_hour,
                 CASE mi.blindname
                          WHEN '1-2WPT盲注' THEN 'Beginner1'
                          WHEN 'WPT-2-4' THEN 'Beginner2'
                          WHEN 'WPT-3-6' THEN 'Beginner3'
                          WHEN 'cgz_5/10' THEN 'Beginner4'
                          WHEN '万能豆_10\\20_新' THEN 'Amateur1'
                          WHEN 'cgz_50/100' THEN 'Amateur2'
                          WHEN 'cgz_100/200' THEN 'Amateur3'
                          WHEN 'cgz_250/500' THEN 'Amateur4'
                          WHEN 'cgz_500/1000' THEN 'Pro1'
                          WHEN '万能豆_1000/2000' THEN 'Pro2'
                          WHEN '万能豆_2500/5000_马' THEN 'Pro3'
                          WHEN 'cgz_5000/10000' THEN 'Pro4'
                          WHEN '万能豆_10000/20000_大' THEN 'Elite1'
                          WHEN 'cgz_2w/4w' THEN 'Elite2'
                          WHEN '万能豆_50000/100000_紫' THEN 'Elite3'
                          WHEN 'cgz_10w/20w' THEN 'Elite4'
                          ELSE mi.blindname
                 END                                                                          table_name,
                 CASE mi.blindname
                          WHEN '1-2WPT盲注' THEN '1/2'
                          WHEN 'WPT-2-4' THEN '2/4'
                          WHEN 'WPT-3-6' THEN '3/6'
                          WHEN 'cgz_5/10' THEN '5/10'
                          WHEN '万能豆_10\\20_新' THEN '10/20'
                          WHEN 'cgz_50/100' THEN '50/100'
                          WHEN 'cgz_100/200' THEN '100/200'
                          WHEN 'cgz_250/500' THEN '250/500'
                          WHEN 'cgz_500/1000' THEN '500/1000'
                          WHEN '万能豆_1000/2000' THEN '1000/2000'
                          WHEN '万能豆_2500/5000_马' THEN '2500/5000'
                          WHEN 'cgz_5000/10000' THEN '5000/10000'
                          WHEN '万能豆_10000/20000_大' THEN '10000/20000'
                          WHEN 'cgz_2w/4w' THEN '20000/40000'
                          WHEN '万能豆_50000/100000_紫' THEN '50000/100000'
                          WHEN 'cgz_10w/20w' THEN '100000/200000'
                          ELSE mi.blindname
                 END                                                                         stakes_level,
                  cgz.username                                                               username,
                  cgz.pan_id                                                                 pan_id
          FROM   OGDZWEB.TJ_MATCHINFO mi
          JOIN   OGDZWEB.TJ_CGZ_FLOW_USERPANINFO cgz
          ON     cgz.matchid=mi.matchid
          WHERE TO_CHAR(CAST(CAST(cgz.time_update AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :yesterday) a
          GROUP BY table_name,  stakes_level,  on_hour
          ORDER BY decode(table_name, 'Beginner1', 1, 'Beginner2', 2, 'Beginner3', 3 , 'Beginner4', 4, 'Amateur1', 5,'Amateur2', 6, 'Amateur3', 7, 
                                     'Amateur4',8, 'Pro1',9, 'Pro2',10, 'Pro3',11, 'Pro4',12, 'Elite1',13, 'Elite2',14, 'Elite3',15, 'Elite4',16), 
                                     on_hour
                 """

    result_proxy = db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(str(sql)), yesterday=yesterday)

    column_names_attached = ['Date', 'Table Name', 'Table Stakes', 'Players', 'Hands Played', 'Total Hands Played']

    last_24_hours_data = result_proxy.fetchall()

    title = 'Daily Report – Game Table Statistic Related'
    email_subject = now.format('MM-DD-YYYY') + '_Game_Table_Statistic_REPORT'
    filename = 'STAKES_LEVEL_REPORT.csv'

    path = os.path.join(app.config["REPORT_FILE_FOLDER"], filename)
    result = pd.DataFrame(pd.DataFrame(last_24_hours_data, columns=column_names_attached))

    with open(path, 'w+', encoding='utf-8') as f:
        result.to_csv(f, sep=',', encoding='utf-8')

    sql = """
          SELECT on_day, table_name, stakes_level, 
                 COUNT(DISTINCT username)                                                 uniq_players,
                 COUNT(DISTINCT pan_id)                                                   uniq_hands_played,
                 COUNT(pan_id)                                                            total_hands_played
          FROM ( SELECT TO_CHAR(CAST(CAST(cgz.time_update AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd')  AS on_day,
                 CASE mi.blindname
                          WHEN '1-2WPT盲注' THEN 'Beginner1'
                          WHEN 'WPT-2-4' THEN 'Beginner2'
                          WHEN 'WPT-3-6' THEN 'Beginner3'
                          WHEN 'cgz_5/10' THEN 'Beginner4'
                          WHEN '万能豆_10\\20_新' THEN 'Amateur1'
                          WHEN 'cgz_50/100' THEN 'Amateur2'
                          WHEN 'cgz_100/200' THEN 'Amateur3'
                          WHEN 'cgz_250/500' THEN 'Amateur4'
                          WHEN 'cgz_500/1000' THEN 'Pro1'
                          WHEN '万能豆_1000/2000' THEN 'Pro2'
                          WHEN '万能豆_2500/5000_马' THEN 'Pro3'
                          WHEN 'cgz_5000/10000' THEN 'Pro4'
                          WHEN '万能豆_10000/20000_大' THEN 'Elite1'
                          WHEN 'cgz_2w/4w' THEN 'Elite2'
                          WHEN '万能豆_50000/100000_紫' THEN 'Elite3'
                          WHEN 'cgz_10w/20w' THEN 'Elite4'
                          ELSE mi.blindname
                 END                                                                          table_name,
                 CASE mi.blindname
                          WHEN '1-2WPT盲注' THEN '1/2'
                          WHEN 'WPT-2-4' THEN '2/4'
                          WHEN 'WPT-3-6' THEN '3/6'
                          WHEN 'cgz_5/10' THEN '5/10'
                          WHEN '万能豆_10\\20_新' THEN '10/20'
                          WHEN 'cgz_50/100' THEN '50/100'
                          WHEN 'cgz_100/200' THEN '100/200'
                          WHEN 'cgz_250/500' THEN '250/500'
                          WHEN 'cgz_500/1000' THEN '500/1000'
                          WHEN '万能豆_1000/2000' THEN '1000/2000'
                          WHEN '万能豆_2500/5000_马' THEN '2500/5000'
                          WHEN 'cgz_5000/10000' THEN '5000/10000'
                          WHEN '万能豆_10000/20000_大' THEN '10000/20000'
                          WHEN 'cgz_2w/4w' THEN '20000/40000'
                          WHEN '万能豆_50000/100000_紫' THEN '50000/100000'
                          WHEN 'cgz_10w/20w' THEN '100000/200000'
                          ELSE mi.blindname
                 END                                                                         stakes_level,
                  cgz.username                                                               username,
                  cgz.pan_id                                                                 pan_id
          FROM   OGDZWEB.TJ_MATCHINFO mi
          JOIN   OGDZWEB.TJ_CGZ_FLOW_USERPANINFO cgz
          ON     cgz.matchid=mi.matchid
          WHERE TO_CHAR(CAST(CAST(cgz.time_update AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :yesterday
          )  a
          GROUP BY on_day, table_name,  stakes_level  
          ORDER BY decode(table_name, 'Beginner1', 1, 'Beginner2', 2, 'Beginner3', 3 , 'Beginner4', 4, 'Amateur1', 5,'Amateur2', 6, 'Amateur3', 7, 
                                     'Amateur4',8, 'Pro1',9, 'Pro2',10, 'Pro3',11, 'Pro4',12, 'Elite1',13, 'Elite2',14, 'Elite3',15, 'Elite4',16) 
                 """

    result_proxy = db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(str(sql)), yesterday=yesterday)
    column_names = ['Date', 'Table Name', 'Table Stakes', 'Players', 'Hands Played', 'Total Hands Played']
    yesterday_data = result_proxy.fetchall()

    yesterday_data = [[row['on_day'],
                       row['table_name'],
                       row['stakes_level'],
                       format(row['uniq_players'], ',d'),
                       format(row['uniq_hands_played'], ',d'),
                       format(row['total_hands_played'], ',d')]
                      for row in yesterday_data]

    send_mail(to=DAILY_REPORT_RECIPIENTS, subject=email_subject, template='cron_daily_report', attachment=path,
              attachment_content_type='text/csv', filename=filename, column_names=column_names,
              report_data=yesterday_data, title=title, generated_at=generated_at, game_table_statistic=True)


@celery.task
def daily_report_ccu():
    now = current_time(app.config['APP_TIMEZONE'])
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')

    ccu_by_hour = """
                    SELECT hour,
                           MAX(all_ccu) AS WPT_MAX_CCU,
                           AVG(all_ccu) AS WPT_AVG_CCU
                    FROM   (SELECT TO_CHAR(CAST(TIME AS TIMESTAMP) AT TIME zone 'America/New_York',
                                          'yyyy-mm-dd,hh24') AS
                                   hour,
                                   all_ccu
                            FROM   ogdzweb.tj_flow_ccu
                            WHERE TO_CHAR(CAST(CAST(TIME AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :yesterday
                            ) a
                    GROUP  BY hour
                    ORDER  BY hour 
                 """

    ccu_by_day = """
                SELECT MAX(all_ccu) AS WPT_MAX_CCU,
                       AVG(all_ccu) AS WPT_AVG_CCU
                FROM   ogdzweb.tj_flow_ccu
                WHERE TO_CHAR(CAST(CAST(TIME AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :yesterday
                
                """

    column_names = ['Hour', 'WPT_MAX_CCU', 'WPT_AVG_CCU']
    ccu_data_by_hour = [[row[0], row[1], int(row[2])] for row in
                        db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(ccu_by_hour),
                                                                                 yesterday=yesterday)]
    WPT_MAX_CCU, WPT_AVG_CCU = \
        list(db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(ccu_by_day), yesterday=yesterday))[0]
    WPT_AVG_CCU = int(WPT_AVG_CCU)

    title = 'Daily Report – CCU Related'
    email_subject = now.format('MM-DD-YYYY') + '_CCU_REPORT'
    filename = 'CCU_REPORT.csv'
    path = os.path.join(app.config["REPORT_FILE_FOLDER"], filename)
    result = pd.DataFrame(pd.DataFrame(ccu_data_by_hour, columns=column_names))

    with open(path, 'w+') as f:
        result.to_csv(f, sep=',', encoding='utf-8')

    send_mail(to=DAILY_REPORT_CCU_RECIPIENTS, subject=email_subject, template='cron_daily_report', attachment=path,
              attachment_content_type='text/csv', filename=filename, column_names=column_names,
              report_data=ccu_data_by_hour, title=title, generated_at=generated_at, WPT_MAX_CCU=WPT_MAX_CCU,
              WPT_AVG_CCU=WPT_AVG_CCU, CCU=True)


@celery.task
def daily_report_new_reg_for_MyGameRoom():
    timezone_offset = app.config['APP_TIMEZONE']
    now = current_time(timezone_offset)
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')

    sql = """
                SELECT DATE_FORMAT( CONVERT_Tz(reg_time, '+00:00',:timezone_offset) , '%m/%d/%y')  AS reg_date,
                       reg_affiliate,
                       reg_campaign,
                       count(*)  AS new_reg
                FROM bi_user
                WHERE reg_affiliate='Cumulus@wpt.com'
                      AND DATE(CONVERT_TZ(reg_time, '+00:00', :timezone_offset))  = :yesterday
                GROUP BY reg_date,
                         reg_affiliate,
                         reg_campaign 
           """

    result_proxy = db.engine.execute(text(sql), yesterday=yesterday, timezone_offset=timezone_offset)
    column_names = ['reg_date', 'reg_affiliate', 'reg_campaign', 'new_reg']
    report_data = result_proxy.fetchall()

    title = 'MyGameRoom New Reg'
    email_subject = now.format('MM-DD-YYYY') + 'MyGameRoom NewReg'

    send_mail(to=DAILY_REPORT_MyGameRoom_RECIPIENTS, subject=email_subject, template='cron_daily_report', column_names=column_names,
              report_data=report_data, title=title, generated_at=generated_at, my_game_room_for_new_reg=True)


@celery.task
def daily_report_revenue_for_MyGameRoom():
    timezone_offset = app.config['APP_TIMEZONE']
    now = current_time(timezone_offset)
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')
    generated_at = now.format('MM-DD-YYYY HH:mm:ss')

    sql = """
            SELECT DATE_FORMAT( CONVERT_Tz(b.created_at, '+00:00',:timezone_offset) , '%m/%d/%y')  AS trans_date,
                   u.reg_affiliate,
                   u.reg_campaign,
                   Sum(b.currency_amount)                                 revenue
            FROM   bi_user u
                   INNER JOIN bi_user_bill b
                           ON u.user_id = b.user_id
            WHERE  u.reg_affiliate = 'Cumulus@wpt.com'
                   AND b.currency_type = 'dollar'
                   AND DATE(CONVERT_TZ(b.created_at, '+00:00', :timezone_offset))   = :yesterday
            GROUP  BY trans_date,
                      u.reg_affiliate,
                      u.reg_campaign 
           """

    result_proxy = db.engine.execute(text(sql), yesterday=yesterday, timezone_offset=timezone_offset)
    column_names = ['trans_data', 'revenue']
    report_data = result_proxy.fetchall()

    title = 'MyGameRoom Revenue'
    email_subject = now.format('MM-DD-YYYY') + 'MyGameRoom Revenue'

    send_mail(to=DAILY_REPORT_MyGameRoom_RECIPIENTS, subject=email_subject, template='cron_daily_report', column_names=column_names,
              report_data=report_data, title=title, generated_at=generated_at, my_game_room_for_revenue=True)

