import arrow
import pandas as pd
from sqlalchemy import text, and_
from sqlalchemy.sql.expression import bindparam

from app.extensions import db, geoip_reader
from app.models.bi import BIUser
from app.tasks import celery, get_config_value, set_config_value, with_db_context


def parse_user_source(reg_device, p_id, u_type):
    if reg_device == 1 or reg_device == 10:  # PlayWPT
        if p_id == 100:
            if u_type == 1:
                return 'Web Facebook'
            if u_type == 2:
                return 'Facebook Game'

        if p_id == 0:
            return 'Web'

    if reg_device == 2:  # PlayWPT Mobile
        if p_id == 100:
            return 'Web Mobile Facebook'

        if p_id == 0:
            return 'Web Mobile'

    if reg_device == 5:  # iOS
        if p_id == 100:
            return 'iOS Facebook'

        if p_id == 0:
            return 'iOS'

    if reg_device == 6:  # Android
        if p_id == 100:
            return 'Android Facebook'

        if p_id == 0:
            return 'Android'

    return 'Unknown'


def parse_user_platform(reg_device, p_id, u_type):
    if reg_device == 1 or reg_device == 10:  # PlayWPT
        if p_id == 100:
            if u_type == 1:
                return 'Web'
            if u_type == 2:
                return 'Facebook Game'

        if p_id == 0:
            return 'Web'

    if reg_device == 2:  # PlayWPT Mobile
        return 'Web Mobile'

    if reg_device == 5:  # iOS
        return 'iOS'

    if reg_device == 6:  # Android
        return 'Android'

    return 'Unknown'


def parse_user_facebook_connect(p_id):
    if p_id == 100:
        return True

    if p_id == 0:
        return False

    return None


def parse_user_account_status(status):
    if status == 10:
        return 'Email validated'
    if status == 2:
        return 'Waiting for email validation'
    if status == 3:
        return 'Email validation timeout'
    if status == 4:
        return 'Locked'
    if status == 5:
        return 'Banned'
    if status == 0:
        return 'Guest'

    return 'Unknown'


def parse_user_gender(gender):
    if gender == 0:
        return 'Confidential'
    if gender == 1:
        return 'Male'
    if gender == 2:
        return 'Female'

    return 'Unknown'


def parse_user_billing_info_contact(contact):
    if contact is not None:
        return ' '.join([x.strip().capitalize() for x in contact.split('-') if x.strip()])
    return None


def process_user_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_id')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT u.*,
                                                  ( CASE
                                                      WHEN u.u_email IS NOT NULL THEN u.u_email
                                                      WHEN pu.pu_email IS NOT NULL THEN pu.pu_email
                                                      ELSE NULL
                                                    END )           AS final_email,
                                                  aff.email         AS reg_affiliate,
                                                  aff.affiliate_id AS reg_affiliate_id_orig,
                                                  
                                                  pu.pu_id          AS facebook_id,
                                                  
                                                  com.compaign_id AS reg_campaign_id_orig,
                                                  com.compaign_desc AS reg_compaign
                                                  
                                           FROM   tb_user_base u
                                                  LEFT JOIN tb_platform_info p
                                                         ON u.p_id = p.p_id
                                                  LEFT JOIN tb_platform_user_info pu
                                                         ON u.u_id = pu.u_id
                                                  LEFT JOIN tb_compaign com
                                                         ON u.compaign_pk = com.compaign_pk
                                                  LEFT JOIN tb_affiliate aff
                                                         ON u.affiliate_pk = aff.affiliate_pk
                                           ORDER  BY u.u_id ASC
                                           """))
        return connection.execute(text("""
                                       SELECT u.*,
                                              ( CASE
                                                  WHEN u.u_email IS NOT NULL THEN u.u_email
                                                  WHEN pu.pu_email IS NOT NULL THEN pu.pu_email
                                                  ELSE NULL
                                                END )             AS final_email,
                                                
                                                aff.email         AS reg_affiliate,
                                                aff.affiliate_id AS reg_affiliate_id_orig,
                                                
                                                pu.pu_id          AS facebook_id,
                                                
                                                com.compaign_id AS reg_campaign_id_orig,
                                                com.compaign_desc AS reg_compaign
                                                
                                       FROM   tb_user_base u
                                              LEFT JOIN tb_platform_info p
                                                     ON u.p_id = p.p_id
                                              LEFT JOIN tb_platform_user_info pu
                                                     ON u.u_id = pu.u_id
                                              LEFT JOIN tb_compaign com
                                                     ON u.compaign_pk = com.compaign_pk
                                              LEFT JOIN tb_affiliate aff
                                                     ON u.affiliate_pk = aff.affiliate_pk
                                       WHERE  u.u_id > :user_id
                                       ORDER  BY u.u_id ASC
                                       """), user_id=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        'user_id': row['u_id'],
        'email': row['final_email'],
        'facebook_id': row['facebook_id'],
        'email_validate_time': row['email_validate_time'],
        'reg_ip': row['reg_ip'],
        'reg_time': row['reg_time'],
        'reg_source': parse_user_source(row['reg_device'], row['p_id'], row['u_type']),
        'reg_platform': parse_user_platform(row['reg_device'], row['p_id'], row['u_type']),
        'reg_facebook_connect': parse_user_facebook_connect(row['p_id']),
        'reg_type_orig': row['u_type'],
        'reg_platform_orig': row['p_id'],
        'reg_device_orig': row['reg_device'],

        'reg_affiliate': row['reg_affiliate'],
        'reg_affiliate_orig': row['affiliate_pk'],
        'reg_affiliate_id_orig': row['reg_affiliate_id_orig'],

        'reg_campaign': row['reg_compaign'],
        'reg_campaign_orig': row['compaign_pk'],
        'reg_campaign_id_orig': row[ 'reg_campaign_id_orig'],

        'account_status': parse_user_account_status(row['u_status']),
        'account_status_orig': row['u_status']
    } for row in result_proxy]

    print('process_user_newly_added_records new data: ' + str(len(rows)))

    if rows:
        new_config_value = rows[-1]['user_id']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            try:
                connection.execute(BIUser.__table__.insert(), rows)
                set_config_value(connection, 'last_imported_user_id', new_config_value)
            except:
                print('process_user_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_newly_updated_records():
    config_value = get_config_value(db, 'last_imported_user_update_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT u.*,
                                                  ( CASE
                                                      WHEN u.u_email IS NOT NULL THEN u.u_email
                                                      WHEN pu.pu_email IS NOT NULL THEN pu.pu_email
                                                      ELSE NULL
                                                    END )           AS final_email,
                                                  aff.email         AS reg_affiliate,
                                                  com.compaign_desc AS reg_compaign
                                           FROM   tb_user_base u
                                                  LEFT JOIN tb_platform_info p
                                                         ON u.p_id = p.p_id
                                                  LEFT JOIN tb_platform_user_info pu
                                                         ON u.u_id = pu.u_id
                                                  LEFT JOIN tb_compaign com
                                                         ON u.compaign_pk = com.compaign_pk
                                                  LEFT JOIN tb_affiliate aff
                                                         ON u.affiliate_pk = aff.affiliate_pk
                                           WHERE  u.update_time IS NOT NULL
                                           ORDER  BY u.u_id ASC
                                           """))
        return connection.execute(text("""
                                       SELECT u.*,
                                              ( CASE
                                                  WHEN u.u_email IS NOT NULL THEN u.u_email
                                                  WHEN pu.pu_email IS NOT NULL THEN pu.pu_email
                                                  ELSE NULL
                                                END )             AS final_email,
                                                aff.email         AS reg_affiliate,
                                                com.compaign_desc AS reg_compaign
                                       FROM   tb_user_base u
                                              LEFT JOIN tb_platform_info p
                                                     ON u.p_id = p.p_id
                                              LEFT JOIN tb_platform_user_info pu
                                                     ON u.u_id = pu.u_id
                                              LEFT JOIN tb_compaign com
                                                     ON u.compaign_pk = com.compaign_pk
                                              LEFT JOIN tb_affiliate aff
                                                     ON u.affiliate_pk = aff.affiliate_pk
                                       WHERE  u.update_time >= :update_time
                                       ORDER  BY u.update_time ASC
                                       """), update_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        '_user_id': row['u_id'],
        'email': row['final_email'],
        'email_validate_time': row['email_validate_time'],
        'account_status': parse_user_account_status(row['u_status']),
        'account_status_orig': row['u_status'],
        'update_time': row['update_time']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['update_time']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'email': bindparam('email'),
                'email_validate_time': bindparam('email_validate_time'),
                'account_status': bindparam('account_status'),
                'account_status_orig': bindparam('account_status_orig')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_update_time', new_config_value)
            except:
                print('process_user_newly_updated_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_newly_updated_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_info_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_info_add_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text('SELECT * FROM tb_user_info ORDER BY add_time ASC'))
        return connection.execute(text('SELECT * FROM tb_user_info WHERE add_time >= :add_time ORDER BY add_time ASC'),
                                  add_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        '_user_id': row['u_id'],
        'gender': parse_user_gender(row['u_sex']),
        'gender_orig': row['u_sex'],
        'first_name': row['name_first'],
        'middle_name': row['name_middle'],
        'last_name': row['name_last'],
        'address': row['addr_detail'],
        'city': row['addr_city'],
        'state': row['addr_state'],
        'zip_code': row['addr_post_code'],
        'country': row['addr_country'],
        'phone': row['u_phone'],
        'birthday': row['u_birth'],
        'add_time': row['add_time'],
        'update_time': row['update_time']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['add_time']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'gender': bindparam('gender'),
                'gender_orig': bindparam('gender_orig'),
                'first_name': bindparam('first_name'),
                'middle_name': bindparam('middle_name'),
                'last_name': bindparam('last_name'),
                'address': bindparam('address'),
                'city': bindparam('city'),
                'state': bindparam('state'),
                'zip_code': bindparam('zip_code'),
                'country': bindparam('country'),
                'phone': bindparam('phone'),
                'birthday': bindparam('birthday')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_info_add_time', new_config_value)
            except:
                print('process_user_info_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_info_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_info_newly_updated_records():
    config_value = get_config_value(db, 'last_imported_user_info_update_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(
                text('SELECT * FROM tb_user_info WHERE update_time IS NOT NULL ORDER BY update_time ASC'))
        return connection.execute(
            text('SELECT * FROM tb_user_info WHERE update_time >= :update_time ORDER BY update_time ASC'),
            update_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        '_user_id': row['u_id'],
        'gender': parse_user_gender(row['u_sex']),
        'gender_orig': row['u_sex'],
        'first_name': row['name_first'],
        'middle_name': row['name_middle'],
        'last_name': row['name_last'],
        'address': row['addr_detail'],
        'city': row['addr_city'],
        'state': row['addr_state'],
        'zip_code': row['addr_post_code'],
        'country': row['addr_country'],
        'phone': row['u_phone'],
        'birthday': row['u_birth'],
        'add_time': row['add_time'],
        'update_time': row['update_time']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['update_time']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'gender': bindparam('gender'),
                'gender_orig': bindparam('gender_orig'),
                'first_name': bindparam('first_name'),
                'middle_name': bindparam('middle_name'),
                'last_name': bindparam('last_name'),
                'address': bindparam('address'),
                'city': bindparam('city'),
                'state': bindparam('state'),
                'zip_code': bindparam('zip_code'),
                'country': bindparam('country'),
                'phone': bindparam('phone'),
                'birthday': bindparam('birthday')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_info_update_time', new_config_value)
            except:
                print('process_user_info_newly_updated_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_info_newly_updated_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_billing_info_added_records():
    config_value = get_config_value(db, 'last_imported_user_billing_info_id')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT t1.*
                                           FROM   billing_info t1
                                                  INNER JOIN (SELECT u_id,
                                                                     Max(billing_info_id) max_id
                                                              FROM   billing_info
                                                              WHERE  billing_info_state = 1
                                                              GROUP  BY u_id) t2
                                                          ON t1.u_id = t2.u_id
                                                             AND t1.billing_info_id = t2.max_id
                                           ORDER  BY t1.billing_info_id ASC
                                           """))
        return connection.execute(text("""
                                       SELECT t1.*
                                       FROM   billing_info t1
                                              INNER JOIN (SELECT u_id,
                                                                 Max(billing_info_id) max_id
                                                          FROM   billing_info
                                                          WHERE  billing_info_state = 1
                                                                 AND billing_info_id > :billing_info_id
                                                          GROUP  BY u_id) t2
                                                      ON t1.u_id = t2.u_id
                                                         AND t1.billing_info_id = t2.max_id
                                       ORDER  BY t1.billing_info_id ASC
                                       """), billing_info_id=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_payment')

    rows = [{
        '_user_id': row['u_id'],
        'billing_info_id': row['billing_info_id'],
        'billing_contact': parse_user_billing_info_contact(row['contact_name']),
        'billing_address': row['address_line'],
        'billing_city': row['city'],
        'billing_state': row['state'],
        'billing_zip_code': row['zip'],
        'billing_country': row['country']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['billing_info_id']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'billing_contact': bindparam('billing_contact'),
                'billing_address': bindparam('billing_address'),
                'billing_city': bindparam('billing_city'),
                'billing_state': bindparam('billing_state'),
                'billing_zip_code': bindparam('billing_zip_code'),
                'billing_country': bindparam('billing_country')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_billing_info_id', new_config_value)
            except:
                print('process_user_billing_info_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_billing_info_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_login_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_login_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT t1.*
                                           FROM   tb_user_login_log t1
                                                  INNER JOIN (SELECT u_id,
                                                                     Max(id) max_id
                                                              FROM   tb_user_login_log
                                                              WHERE  remark = "true~"
                                                              GROUP  BY u_id) t2
                                                          ON t1.u_id = t2.u_id
                                                             AND t1.id = t2.max_id
                                           ORDER  BY t1.login_time ASC
                                           """))
        return connection.execute(text("""
                                       SELECT t1.*
                                       FROM   tb_user_login_log t1
                                              INNER JOIN (SELECT u_id,
                                                                 Max(id) max_id
                                                          FROM   tb_user_login_log
                                                          WHERE  remark = "true~"
                                                                 AND login_time >= :login_time
                                                          GROUP  BY u_id) t2
                                                      ON t1.u_id = t2.u_id
                                                         AND t1.id = t2.max_id
                                       ORDER  BY t1.login_time ASC
                                       """), login_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        '_user_id': row['u_id'],
        'last_login_ip': row['login_ip'],
        'last_login_time': row['login_time']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['last_login_time']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'last_login_ip': bindparam('last_login_ip'),
                'last_login_time': bindparam('last_login_time')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_login_time', new_config_value)
            except:
                print('process_user_login_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_login_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_ourgame_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_ourgame_add_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text('SELECT * FROM tb_user_ourgame ORDER BY create_time ASC'))
        return connection.execute(
            text('SELECT * FROM tb_user_ourgame WHERE create_time >= :create_time ORDER BY create_time ASC'),
            create_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt')

    rows = [{
        '_user_id': row['u_id'],
        'username': row['og_role_name'],
        'display_name': row['og_display_name'],
        'og_account': row['og_account'],
        'create_time': row['create_time']
    } for row in result_proxy]

    if rows:

        new_config_value = rows[-1]['create_time']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'username': bindparam('username'),
                'display_name': bindparam('display_name'),
                'og_account': bindparam('og_account')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_ourgame_add_time', new_config_value)
            except:
                print('process_user_ourgame_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_ourgame_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_ourgame_newly_updated_records():
    def collection_new_data(connection, transaction):

        """ Get newly added. """

        return connection.execute(
            text('SELECT u_id,og_display_name  FROM tb_user_ourgame WHERE og_role_name !=og_display_name'))

    new_data = with_db_context(db, collection_new_data, 'orig_wpt')

    def collection_old_data(connection, transaction):

        return connection.execute(text('SELECT user_id, display_name FROM bi_user WHERE username !=display_name'))

    old_data = with_db_context(db, collection_old_data)

    user_ourgame_display_name = set([(row['u_id'], row['og_display_name']) for row in new_data])
    bi_user_display_name = set([(row['user_id'], row['display_name']) for row in old_data])
    to_update_user = user_ourgame_display_name - bi_user_display_name

    print('process_user_ourgame_newly_update_records:  users who need to update displayname have {}'.format(
        len(to_update_user)))

    rows = [{'_user_id': user_id_to_displayname_map[0], 'display_name': user_id_to_displayname_map[1]} for
            user_id_to_displayname_map in to_update_user]

    if rows:

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'display_name': bindparam('display_name')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
            except:
                print('process_user_ourgame_newly_updated_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_ourgame_newly_updated_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_payment_spin_purchase_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_payment_spin_purchase_add_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT u_id,
                                                  SUM(CASE
                                                        WHEN user_paylog_status_id = 3 THEN 1
                                                        ELSE 0
                                                      END)                 AS count_of_dollar_exchanged_for_spin_purchase,
                                                  ROUND(SUM(CASE
                                                              WHEN user_paylog_status_id = 3 THEN order_price
                                                              ELSE 0
                                                            END) / 100, 2) AS amount_of_dollar_exchanged_for_spin_purchase,
                                                  MIN(CASE
                                                        WHEN user_paylog_status_id = 3 THEN createtime
                                                        ELSE NULL
                                                      END)                 AS first_time_of_dollar_exchanged_for_spin_purchase,
                                                  MAX(CASE
                                                        WHEN user_paylog_status_id = 3 THEN createtime
                                                        ELSE NULL
                                                      END)                 AS last_time_of_dollar_exchanged_for_spin_purchase,
                                                  Max(createtime)          AS max_createtime
                                           FROM   user_paylog
                                           WHERE  tb_product_id = 925011306 AND user_paylog_status_id = 3
                                           GROUP  BY u_id
                                           ORDER  BY max_createtime ASC
                                           """))
        fixed_config_value = arrow.get(config_value).replace(hours=-24).format('YYYY-MM-DD HH:mm:ss')
        return connection.execute(text("""
                                       SELECT u_id,
                                              SUM(CASE
                                                    WHEN user_paylog_status_id = 3 THEN 1
                                                    ELSE 0
                                                  END)                 AS count_of_dollar_exchanged_for_spin_purchase,
                                              ROUND(SUM(CASE
                                                          WHEN user_paylog_status_id = 3 THEN order_price
                                                          ELSE 0
                                                        END) / 100, 2) AS amount_of_dollar_exchanged_for_spin_purchase,
                                              MIN(CASE
                                                    WHEN user_paylog_status_id = 3 THEN createtime
                                                    ELSE NULL
                                                  END)                 AS first_time_of_dollar_exchanged_for_spin_purchase,
                                              MAX(CASE
                                                    WHEN user_paylog_status_id = 3 THEN createtime
                                                    ELSE NULL
                                                  END)                 AS last_time_of_dollar_exchanged_for_spin_purchase,
                                              Max(createtime)          AS max_createtime
                                       FROM   user_paylog
                                       WHERE  tb_product_id = 925011306 AND user_paylog_status_id = 3
                                              AND u_id IN (SELECT DISTINCT u_id
                                                           FROM   user_paylog
                                                           WHERE  createtime >= :add_time)
                                       GROUP  BY u_id
                                       ORDER  BY max_createtime ASC 
                                       """), add_time=fixed_config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_payment')

    rows = [{
        '_user_id': row['u_id'],
        'count_of_dollar_exchanged_for_spin_purchase': row['count_of_dollar_exchanged_for_spin_purchase'],
        'amount_of_dollar_exchanged_for_spin_purchase': row['amount_of_dollar_exchanged_for_spin_purchase'],
        'first_time_of_dollar_exchanged_for_spin_purchase': row['first_time_of_dollar_exchanged_for_spin_purchase'],
        'last_time_of_dollar_exchanged_for_spin_purchase': row['last_time_of_dollar_exchanged_for_spin_purchase'],
        'max_createtime': row['max_createtime']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_createtime']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'count_of_dollar_exchanged_for_spin_purchase': bindparam('count_of_dollar_exchanged_for_spin_purchase'),
                'amount_of_dollar_exchanged_for_spin_purchase': bindparam(
                    'amount_of_dollar_exchanged_for_spin_purchase'),
                'last_time_of_dollar_exchanged_for_spin_purchase': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_purchase')
            }

            column_keys = ['first_time_of_dollar_exchanged_for_spin_purchase']
            column_params = {}
            for column in column_keys:
                column_params[column + '_where'] = and_(
                    BIUser.__table__.c.user_id == bindparam('_user_id'),
                    BIUser.__table__.c[column] == None
                )
                column_params[column + '_values'] = {column: bindparam(column)}

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                for column in column_keys:
                    connection.execute(BIUser.__table__.update().where(column_params[column + '_where']).values(
                        column_params[column + '_values']), rows)
                set_config_value(connection, 'last_imported_user_payment_spin_purchase_add_time', new_config_value)
            except:
                print('process_user_payment_spin_purchase_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_payment_spin_purchase_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_payment_spin_purchase_newly_updated_records():
    config_value = get_config_value(db, 'last_imported_user_payment_spin_purchase_update_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT u_id,
                                                  SUM(CASE
                                                        WHEN user_paylog_status_id = 3 THEN 1
                                                        ELSE 0
                                                      END)                 AS count_of_dollar_exchanged_for_spin_purchase,
                                                  ROUND(SUM(CASE
                                                              WHEN user_paylog_status_id = 3 THEN order_price
                                                              ELSE 0
                                                            END) / 100, 2) AS amount_of_dollar_exchanged_for_spin_purchase,
                                                  MIN(CASE
                                                        WHEN user_paylog_status_id = 3 THEN createtime
                                                        ELSE NULL
                                                      END)                 AS first_time_of_dollar_exchanged_for_spin_purchase,
                                                  MAX(CASE
                                                        WHEN user_paylog_status_id = 3 THEN createtime
                                                        ELSE NULL
                                                      END)                 AS last_time_of_dollar_exchanged_for_spin_purchase,
                                                  MAX(platform_return_time)          AS max_updatetime
                                           FROM   user_paylog
                                           WHERE  tb_product_id = 925011306 AND user_paylog_status_id = 3
                                           GROUP  BY u_id
                                           ORDER  BY max_updatetime ASC
                                           """))
        fixed_config_value = arrow.get(config_value).replace(hours=-24).format('YYYY-MM-DD HH:mm:ss')
        return connection.execute(text("""
                                       SELECT u_id,
                                              SUM(CASE
                                                    WHEN user_paylog_status_id = 3 THEN 1
                                                    ELSE 0
                                                  END)                 AS count_of_dollar_exchanged_for_spin_purchase,
                                              ROUND(SUM(CASE
                                                          WHEN user_paylog_status_id = 3 THEN order_price
                                                          ELSE 0
                                                        END) / 100, 2) AS amount_of_dollar_exchanged_for_spin_purchase,
                                              MIN(CASE
                                                    WHEN user_paylog_status_id = 3 THEN createtime
                                                    ELSE NULL
                                                  END)                 AS first_time_of_dollar_exchanged_for_spin_purchase,
                                              MAX(CASE
                                                    WHEN user_paylog_status_id = 3 THEN createtime
                                                    ELSE NULL
                                                  END)                 AS last_time_of_dollar_exchanged_for_spin_purchase,
                                              Max(platform_return_time)          AS max_updatetime
                                       FROM   user_paylog
                                       WHERE  tb_product_id = 925011306 AND user_paylog_status_id = 3
                                              AND u_id IN (SELECT DISTINCT u_id
                                                           FROM   user_paylog
                                                           WHERE  platform_return_time >= :update_time)
                                       GROUP  BY u_id
                                       ORDER  BY max_updatetime ASC 
                                       """), update_time=fixed_config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_payment')

    rows = [{
        '_user_id': row['u_id'],
        'count_of_dollar_exchanged_for_spin_purchase': row['count_of_dollar_exchanged_for_spin_purchase'],
        'amount_of_dollar_exchanged_for_spin_purchase': row['amount_of_dollar_exchanged_for_spin_purchase'],
        'first_time_of_dollar_exchanged_for_spin_purchase': row['first_time_of_dollar_exchanged_for_spin_purchase'],
        'last_time_of_dollar_exchanged_for_spin_purchase': row['last_time_of_dollar_exchanged_for_spin_purchase'],
        'max_updatetime': row['max_updatetime']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_updatetime']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'count_of_dollar_exchanged_for_spin_purchase': bindparam('count_of_dollar_exchanged_for_spin_purchase'),
                'amount_of_dollar_exchanged_for_spin_purchase': bindparam(
                    'amount_of_dollar_exchanged_for_spin_purchase'),
                'last_time_of_dollar_exchanged_for_spin_purchase': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_purchase')
            }

            column_keys = ['first_time_of_dollar_exchanged_for_spin_purchase']
            column_params = {}
            for column in column_keys:
                column_params[column + '_where'] = and_(
                    BIUser.__table__.c.user_id == bindparam('_user_id'),
                    BIUser.__table__.c[column] == None
                )
                column_params[column + '_values'] = {column: bindparam(column)}

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                for column in column_keys:
                    connection.execute(BIUser.__table__.update().where(column_params[column + '_where']).values(
                        column_params[column + '_values']), rows)
                set_config_value(connection, 'last_imported_user_payment_spin_purchase_update_time', new_config_value)
            except:
                print('process_user_payment_spin_purchase_newly_updated_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_payment_spin_purchase_newly_updated_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_gold_balance_related_records():
    # 为了加快查询速度,改用pandas

    config_value = get_config_value(db, 'last_imported_og_powergamecoin_add_time')

    df = pd.read_sql_query(
        "SELECT * FROM gl.powergamecoin_detail WHERE recdate >= TO_DATE(:add_time, 'yyyy-mm-dd,hh24:mi:ss')",
        params={'add_time': config_value},
        con=db.get_engine(db.get_app(), bind='orig_wpt_ods'))

    max_redate = df['recdate'].max()

    # cal last_free_spin_time

    last_free_spin_time = df.query('producttype==925011310').groupby(['username'])['recdate'].max().to_dict()
    last_free_spin_time_rows = [{'_og_account': key, 'last_free_spin_time': last_free_spin_time[key]} for key in
                                last_free_spin_time]
    if last_free_spin_time_rows:

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.og_account == bindparam('_og_account')

            values = {
                'last_free_spin_time': bindparam('last_free_spin_time'),
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), last_free_spin_time_rows)
            except:
                print('process_user_gold_balance_related_records last free spin time transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_gold_balance_related_records  last free spin time transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    # cal  last_premium_spin_time

    last_premium_spin_time = df.query('producttype==925011308 | producttype==925011309  ').groupby(['username'])[
        'recdate'].max().to_dict()
    last_premium_spin_time_rows = [{'_og_account': key, 'last_premium_spin_time': last_premium_spin_time[key]} for key
                                   in last_premium_spin_time]

    if last_premium_spin_time_rows:

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.og_account == bindparam('_og_account')

            values = {
                'last_premium_spin_time': bindparam('last_premium_spin_time'),
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), last_premium_spin_time_rows)
            except:
                print('process_user_gold_balance_related_records  last premium spin time transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_gold_balance_related_records last premium spin time transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    # cal last_porker_time

    last_poker_time = df.query('gamecoin < 0').groupby(['username'])['recdate'].max().to_dict()
    last_poker_time_rows = [{'_og_account': key, 'last_poker_time': last_poker_time[key]} for key in last_poker_time]

    if last_poker_time_rows:
        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.og_account == bindparam('_og_account')

            values = {

            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), last_poker_time_rows)
            except:
                print('process_user_gold_balance_related_records  last poker time rows transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_gold_balance_related_records  last poker time rows transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    # cal  gold_balance

    df['gold_balance'] = df["coin"].add(df["gamecoin"])
    gold_balance_sort_by_recdate = df.sort_values('recdate', ascending=False).groupby('username').head(1)
    gold_balance_df = gold_balance_sort_by_recdate.loc[:, ["username", "gold_balance"]]
    gold_balance = pd.Series(gold_balance_df.gold_balance.values, index=gold_balance_df.username).to_dict()
    gold_balance_rows = [{'_og_account': key, 'gold_balance': int(gold_balance[key])} for key in gold_balance]

    if gold_balance_rows:

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.og_account == bindparam('_og_account')

            values = {
                'gold_balance': bindparam('gold_balance'),

            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), gold_balance_rows)
            except:
                print('process_user_gold_balance_related_records  gold balance transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_gold_balance_related_records  gold balance transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    # cal first_free_spin_time and porker_time

    iter_step = 900
    first_poker_time_rows = []
    first_free_spin_time_rows = []

    users = list(set(df['username']))

    #  IN 最多只能有1000个用户
    for i in range(0, len(users), iter_step):
        users_partitions = list(users)[i:i + iter_step]
        query_sql = "SELECT * FROM gl.powergamecoin_detail WHERE username IN " + str(tuple(users_partitions))

        df = pd.read_sql_query(sql=query_sql, con=db.get_engine(db.get_app(), bind='orig_wpt_ods'))

        first_poker_time = df.query('gamecoin < 0').groupby(['username'])['recdate'].min().to_dict()
        first_poker_time_partition = [{'_og_account': key, 'first_poker_time': first_poker_time[key]} for key in
                                      first_poker_time]
        first_poker_time_rows.extend(first_poker_time_partition)

        first_free_spin_time = df.query('producttype==925011307 | producttype==925011310 ').groupby(['username'])[
            'recdate'].min().to_dict()
        first_free_spin_time_partition = [{'_og_account': key, 'first_free_spin_time': first_free_spin_time[key]} for
                                          key in first_free_spin_time]
        first_free_spin_time_rows.extend(first_free_spin_time_partition)

    def sync_collection(connection, transaction):
        """ Sync newly added. """
        where = BIUser.__table__.c.og_account == bindparam('_og_account')

        values = {
            'first_poker_time': bindparam('first_poker_time'),
        }

        try:
            connection.execute(BIUser.__table__.update().where(where).values(values), first_poker_time_rows)
        except:
            print('process_user_gold_balance_related_records  first poker time transaction.rollback()')
            transaction.rollback()
            raise
        else:
            print('process_user_gold_balance_related_records first poker time transaction.commit()')
            transaction.commit()
        return

    with_db_context(db, sync_collection)

    new_config_value = str(max_redate)

    def sync_collection(connection, transaction):
        """ Sync newly added. """
        where = BIUser.__table__.c.og_account == bindparam('_og_account')

        values = {
            'first_free_spin_time': bindparam('first_free_spin_time'),
        }

        try:
            connection.execute(BIUser.__table__.update().where(where).values(values), first_free_spin_time_rows)
            set_config_value(connection, 'last_imported_og_powergamecoin_add_time', new_config_value)
        except:
            print('process_user_gold_balance_related_records  first free spin time transaction.rollback()')
            transaction.rollback()
            raise
        else:
            print('process_user_gold_balance_related_records first free spin time transaction.commit()')
            transaction.commit()
        return

    with_db_context(db, sync_collection)


def process_user_silver_balance_related_records():
    config_value = get_config_value(db, 'last_imported_og_gamecoin_add_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                            SELECT tb.username         AS og_account,
                                                   (SELECT recdate
                                                    FROM   (SELECT recdate
                                                            FROM   gl.gamecoin_detail
                                                            WHERE  tb.username = username
                                                                   AND gamecoin < 0
                                                            ORDER  BY recdate ASC) a
                                                    WHERE  ROWNUM = 1) AS first_slots_time,
                                                   (SELECT recdate
                                                    FROM   (SELECT recdate
                                                            FROM   gl.gamecoin_detail
                                                            WHERE  tb.username = username
                                                                   AND gamecoin < 0
                                                            ORDER  BY recdate DESC) b
                                                    WHERE  ROWNUM = 1) AS last_slots_time,
                                                   (SELECT silver_balance
                                                    FROM   (SELECT ( gamecoin + coin ) AS silver_balance
                                                            FROM   gl.gamecoin_detail
                                                            WHERE  tb.username = username
                                                            ORDER  BY recdate DESC) c
                                                    WHERE  ROWNUM = 1) AS silver_balance,
                                                   MAX(tb.recdate)     AS max_recdate
                                            FROM   gl.gamecoin_detail tb
                                            GROUP  BY tb.username
                                            ORDER  BY max_recdate ASC;
                                           """))
        return connection.execute(text(""" SELECT tb.username         AS og_account,
                                                       (SELECT recdate
                                                        FROM   (SELECT recdate
                                                                FROM   gl.gamecoin_detail
                                                                WHERE  tb.username = username
                                                                       AND gamecoin < 0
                                                                ORDER  BY recdate ASC) a
                                                        WHERE  ROWNUM = 1) AS first_slots_time,
                                                       (SELECT recdate
                                                        FROM   (SELECT recdate
                                                                FROM   gl.gamecoin_detail
                                                                WHERE  tb.username = username
                                                                       AND gamecoin < 0
                                                                ORDER  BY recdate DESC) b
                                                        WHERE  ROWNUM = 1) AS last_slots_time,
                                                       (SELECT silver_balance
                                                        FROM   (SELECT ( gamecoin + coin ) AS silver_balance
                                                                FROM   gl.gamecoin_detail
                                                                WHERE  tb.username = username
                                                                ORDER  BY recdate DESC) c
                                                        WHERE  ROWNUM = 1) AS silver_balance,
                                                       MAX(tb.recdate)     AS max_recdate
                                                FROM   gl.gamecoin_detail tb
                                                WHERE  tb.recdate >= TO_DATE(:add_time, 'yyyy-mm-dd,hh24:mi:ss')
                                                GROUP  BY tb.username
                                                ORDER  BY max_recdate ASC
                                       """), add_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_ods')

    rows = [{
        '_og_account': row['og_account'],
        'silver_balance': row['silver_balance'],
        'first_slots_time': row['first_slots_time'],
        'last_slots_time': row['last_slots_time'],
        'max_recdate': row['max_recdate']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_recdate']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.og_account == bindparam('_og_account')
            values = {
                'silver_balance': bindparam('silver_balance'),
                'first_slots_time': bindparam('first_slots_time'),
                'last_slots_time': bindparam('last_slots_time')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_og_gamecoin_add_time', new_config_value)
            except:
                print('process_user_silver_balance_related_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_silver_balance_related_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_reward_point_related_records():
    pass


def process_user_mall_order_newly_added_records():
    config_value = get_config_value(db, 'last_imported_user_mall_order_add_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT o.UserId,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_masterpoint_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_masterpoint_exchanged_for_gold,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_masterpoint_exchanged_for_gold,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_masterpoint_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_gold,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_gold,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_silver,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_silver,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_silver,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_silver,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_lucky_spin,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_lucky_spin,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_lucky_spin,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_lucky_spin,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_lucky_charm,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_lucky_charm,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_lucky_charm,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_lucky_charm,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_avatar,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_avatar,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_avatar,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_avatar,
                                                  SUM(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_emoji,
                                                  SUM(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_emoji,
                                                  MIN(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_emoji,
                                                  MAX(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_emoji,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_spin_booster,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_spin_booster,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_spin_booster,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_spin_booster,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_spin_ticket,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_spin_ticket,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_spin_ticket,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_spin_ticket,
                                                  Max(o.CDate) AS max_createtime
                                           FROM   Mall_tOrder o
                                                  LEFT JOIN Mall_tOrderProductLog op
                                                         ON op.OrderId = o.OrderId
                                                  LEFT JOIN Mall_tProduct p
                                                         ON op.ProductId = p.Id
                                                  LEFT JOIN Mall_tCurrency c
                                                         ON o.CurrencyCode = c.CurrencyCode
                                           WHERE  o.OrderStatus != 1 AND o.OrderStatus != 41 AND (o.PaymentMode IS NULL OR o.PaymentMode = 1)
                                           GROUP  BY o.UserId
                                           ORDER  BY max_createtime ASC
                                           """))
        return connection.execute(text("""
                                       SELECT o.UserId,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_masterpoint_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_masterpoint_exchanged_for_gold,
                                              MIN(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_masterpoint_exchanged_for_gold,
                                              MAX(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_masterpoint_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_gold,
                                              MIN(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_gold,
                                              MAX(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_silver,
                                              MIN(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_silver,
                                              MAX(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_silver,
                                              MIN(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_silver,
                                              MAX(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_lucky_spin,
                                              SUM(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  End)     AS amount_of_dollar_exchanged_for_lucky_spin,
                                              MIN(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_lucky_spin,
                                              MAX(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  End)     AS last_time_of_dollar_exchanged_for_lucky_spin,
                                              SUM(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_lucky_charm,
                                              SUM(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_lucky_charm,
                                              MIN(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_lucky_charm,
                                              MAX(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_lucky_charm,
                                              SUM(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_avatar,
                                              SUM(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_avatar,
                                              MIN(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_avatar,
                                              MAX(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_avatar,
                                              SUM(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_emoji,
                                              SUM(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_emoji,
                                              MIN(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_emoji,
                                              MAX(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_emoji,
                                              SUM(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_spin_booster,
                                              SUM(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_spin_booster,
                                              MIN(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_spin_booster,
                                              MAX(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_spin_booster,
                                              SUM(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_spin_ticket,
                                              SUM(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_spin_ticket,
                                              MIN(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_spin_ticket,
                                              MAX(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_spin_ticket,
                                              Max(o.CDate) AS max_createtime
                                       FROM   Mall_tOrder o
                                              LEFT JOIN Mall_tOrderProductLog op
                                                     ON op.OrderId = o.OrderId
                                              LEFT JOIN Mall_tProduct p
                                                     ON op.ProductId = p.Id
                                              LEFT JOIN Mall_tCurrency c
                                                     ON o.CurrencyCode = c.CurrencyCode
                                       WHERE  o.OrderStatus != 1 AND o.OrderStatus != 41 AND (o.PaymentMode IS NULL OR o.PaymentMode = 1)
                                              AND o.UserId IN (SELECT DISTINCT UserId
                                                               FROM   Mall_tOrder
                                                               WHERE  CDate >= :add_time)
                                       GROUP  BY o.UserId
                                       ORDER  BY max_createtime ASC
                                       """), add_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_mall')

    rows = [{
        '_user_id': row['UserId'],

        'count_of_masterpoint_exchanged_for_gold': row['count_of_masterpoint_exchanged_for_gold'],
        'amount_of_masterpoint_exchanged_for_gold': row['amount_of_masterpoint_exchanged_for_gold'],
        'first_time_of_masterpoint_exchanged_for_gold': row['first_time_of_masterpoint_exchanged_for_gold'],
        'last_time_of_masterpoint_exchanged_for_gold': row['last_time_of_masterpoint_exchanged_for_gold'],

        'count_of_dollar_exchanged_for_gold': row['count_of_dollar_exchanged_for_gold'],
        'amount_of_dollar_exchanged_for_gold': row['amount_of_dollar_exchanged_for_gold'],
        'first_time_of_dollar_exchanged_for_gold': row['first_time_of_dollar_exchanged_for_gold'],
        'last_time_of_dollar_exchanged_for_gold': row['last_time_of_dollar_exchanged_for_gold'],

        'count_of_gold_exchanged_for_silver': row['count_of_gold_exchanged_for_silver'],
        'amount_of_gold_exchanged_for_silver': row['amount_of_gold_exchanged_for_silver'],
        'first_time_of_gold_exchanged_for_silver': row['first_time_of_gold_exchanged_for_silver'],
        'last_time_of_gold_exchanged_for_silver': row['last_time_of_gold_exchanged_for_silver'],

        'count_of_dollar_exchanged_for_silver': row['count_of_dollar_exchanged_for_silver'],
        'amount_of_dollar_exchanged_for_silver': row['amount_of_dollar_exchanged_for_silver'],
        'first_time_of_dollar_exchanged_for_silver': row['first_time_of_dollar_exchanged_for_silver'],
        'last_time_of_dollar_exchanged_for_silver': row['last_time_of_dollar_exchanged_for_silver'],

        'count_of_dollar_exchanged_for_lucky_spin': row['count_of_dollar_exchanged_for_lucky_spin'],
        'amount_of_dollar_exchanged_for_lucky_spin': row['amount_of_dollar_exchanged_for_lucky_spin'],
        'first_time_of_dollar_exchanged_for_lucky_spin': row['first_time_of_dollar_exchanged_for_lucky_spin'],
        'last_time_of_dollar_exchanged_for_lucky_spin': row['last_time_of_dollar_exchanged_for_lucky_spin'],

        'count_of_gold_exchanged_for_lucky_charm': row['count_of_gold_exchanged_for_lucky_charm'],
        'amount_of_gold_exchanged_for_lucky_charm': row['amount_of_gold_exchanged_for_lucky_charm'],
        'first_time_of_gold_exchanged_for_lucky_charm': row['first_time_of_gold_exchanged_for_lucky_charm'],
        'last_time_of_gold_exchanged_for_lucky_charm': row['last_time_of_gold_exchanged_for_lucky_charm'],

        'count_of_gold_exchanged_for_avatar': row['count_of_gold_exchanged_for_avatar'],
        'amount_of_gold_exchanged_for_avatar': row['amount_of_gold_exchanged_for_avatar'],
        'first_time_of_gold_exchanged_for_avatar': row['first_time_of_gold_exchanged_for_avatar'],
        'last_time_of_gold_exchanged_for_avatar': row['last_time_of_gold_exchanged_for_avatar'],

        'count_of_gold_exchanged_for_emoji': row['count_of_gold_exchanged_for_emoji'],
        'amount_of_gold_exchanged_for_emoji': row['amount_of_gold_exchanged_for_emoji'],
        'first_time_of_gold_exchanged_for_emoji': row['first_time_of_gold_exchanged_for_emoji'],
        'last_time_of_gold_exchanged_for_emoji': row['last_time_of_gold_exchanged_for_emoji'],

        'count_of_dollar_exchanged_for_spin_booster': row['count_of_dollar_exchanged_for_spin_booster'],
        'amount_of_dollar_exchanged_for_spin_booster': row['amount_of_dollar_exchanged_for_spin_booster'],
        'first_time_of_dollar_exchanged_for_spin_booster': row['first_time_of_dollar_exchanged_for_spin_booster'],
        'last_time_of_dollar_exchanged_for_spin_booster': row['last_time_of_dollar_exchanged_for_spin_booster'],

        'count_of_dollar_exchanged_for_spin_ticket': row['count_of_dollar_exchanged_for_spin_ticket'],
        'amount_of_dollar_exchanged_for_spin_ticket': row['amount_of_dollar_exchanged_for_spin_ticket'],
        'first_time_of_dollar_exchanged_for_spin_ticket': row['first_time_of_dollar_exchanged_for_spin_ticket'],
        'last_time_of_dollar_exchanged_for_spin_ticket': row['last_time_of_dollar_exchanged_for_spin_ticket'],

        'max_createtime': row['max_createtime']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_createtime']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'count_of_masterpoint_exchanged_for_gold': bindparam('count_of_masterpoint_exchanged_for_gold'),
                'amount_of_masterpoint_exchanged_for_gold': bindparam('amount_of_masterpoint_exchanged_for_gold'),
                'last_time_of_masterpoint_exchanged_for_gold': bindparam('last_time_of_masterpoint_exchanged_for_gold'),

                'count_of_dollar_exchanged_for_gold': bindparam('count_of_dollar_exchanged_for_gold'),
                'amount_of_dollar_exchanged_for_gold': bindparam('amount_of_dollar_exchanged_for_gold'),
                'last_time_of_dollar_exchanged_for_gold': bindparam('last_time_of_dollar_exchanged_for_gold'),

                'count_of_gold_exchanged_for_silver': bindparam('count_of_gold_exchanged_for_silver'),
                'amount_of_gold_exchanged_for_silver': bindparam('amount_of_gold_exchanged_for_silver'),
                'last_time_of_gold_exchanged_for_silver': bindparam('last_time_of_gold_exchanged_for_silver'),

                'count_of_dollar_exchanged_for_silver': bindparam('count_of_dollar_exchanged_for_silver'),
                'amount_of_dollar_exchanged_for_silver': bindparam('amount_of_dollar_exchanged_for_silver'),
                'last_time_of_dollar_exchanged_for_silver': bindparam('last_time_of_dollar_exchanged_for_silver'),

                'count_of_dollar_exchanged_for_lucky_spin': bindparam('count_of_dollar_exchanged_for_lucky_spin'),
                'amount_of_dollar_exchanged_for_lucky_spin': bindparam('amount_of_dollar_exchanged_for_lucky_spin'),
                'last_time_of_dollar_exchanged_for_lucky_spin': bindparam(
                    'last_time_of_dollar_exchanged_for_lucky_spin'),

                'count_of_gold_exchanged_for_lucky_charm': bindparam('count_of_gold_exchanged_for_lucky_charm'),
                'amount_of_gold_exchanged_for_lucky_charm': bindparam('amount_of_gold_exchanged_for_lucky_charm'),
                'last_time_of_gold_exchanged_for_lucky_charm': bindparam('last_time_of_gold_exchanged_for_lucky_charm'),

                'count_of_gold_exchanged_for_avatar': bindparam('count_of_gold_exchanged_for_avatar'),
                'amount_of_gold_exchanged_for_avatar': bindparam('amount_of_gold_exchanged_for_avatar'),
                'last_time_of_gold_exchanged_for_avatar': bindparam('last_time_of_gold_exchanged_for_avatar'),

                'count_of_gold_exchanged_for_emoji': bindparam('count_of_gold_exchanged_for_emoji'),
                'amount_of_gold_exchanged_for_emoji': bindparam('amount_of_gold_exchanged_for_emoji'),
                'last_time_of_gold_exchanged_for_emoji': bindparam('last_time_of_gold_exchanged_for_emoji'),

                'count_of_dollar_exchanged_for_spin_booster': bindparam('count_of_dollar_exchanged_for_spin_booster'),
                'amount_of_dollar_exchanged_for_spin_booster': bindparam('amount_of_dollar_exchanged_for_spin_booster'),
                'last_time_of_dollar_exchanged_for_spin_booster': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_booster'),

                'count_of_dollar_exchanged_for_spin_ticket': bindparam('count_of_dollar_exchanged_for_spin_ticket'),
                'amount_of_dollar_exchanged_for_spin_ticket': bindparam('amount_of_dollar_exchanged_for_spin_ticket'),
                'last_time_of_dollar_exchanged_for_spin_ticket': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_ticket'),
            }

            column_keys = ['first_time_of_masterpoint_exchanged_for_gold',
                           'first_time_of_dollar_exchanged_for_gold',
                           'first_time_of_gold_exchanged_for_silver',
                           'first_time_of_dollar_exchanged_for_silver',
                           'first_time_of_dollar_exchanged_for_lucky_spin',
                           'first_time_of_gold_exchanged_for_lucky_charm',
                           'first_time_of_gold_exchanged_for_avatar',
                           'first_time_of_gold_exchanged_for_emoji',
                           'first_time_of_dollar_exchanged_for_spin_booster',
                           'first_time_of_dollar_exchanged_for_spin_ticket']
            column_params = {}
            for column in column_keys:
                column_params[column + '_where'] = and_(
                    BIUser.__table__.c.user_id == bindparam('_user_id'),
                    BIUser.__table__.c[column] == None
                )
                column_params[column + '_values'] = {column: bindparam(column)}

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                for column in column_keys:
                    connection.execute(BIUser.__table__.update().where(column_params[column + '_where']).values(
                        column_params[column + '_values']), rows)
                set_config_value(connection, 'last_imported_user_mall_order_add_time', new_config_value)
            except:
                print('process_user_mall_order_newly_added_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_mall_order_newly_added_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_mall_order_newly_updated_records():
    config_value = get_config_value(db, 'last_imported_user_mall_order_update_time')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT o.UserId,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_masterpoint_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_masterpoint_exchanged_for_gold,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_masterpoint_exchanged_for_gold,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_masterpoint_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_gold,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_gold,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_gold,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_silver,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_silver,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_silver,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_silver,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_silver,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_lucky_spin,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_lucky_spin,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_lucky_spin,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_lucky_spin,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_lucky_charm,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_lucky_charm,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_lucky_charm,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_lucky_charm,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_avatar,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_avatar,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_avatar,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_avatar,
                                                  SUM(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_gold_exchanged_for_emoji,
                                                  SUM(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_gold_exchanged_for_emoji,
                                                  MIN(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_gold_exchanged_for_emoji,
                                                  MAX(CASE
                                                        WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_gold_exchanged_for_emoji,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_spin_booster,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_spin_booster,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_spin_booster,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_spin_booster,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                        ELSE 0
                                                      END)     AS count_of_dollar_exchanged_for_spin_ticket,
                                                  SUM(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                        ELSE 0
                                                      END)     AS amount_of_dollar_exchanged_for_spin_ticket,
                                                  MIN(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS first_time_of_dollar_exchanged_for_spin_ticket,
                                                  MAX(CASE
                                                        WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                        ELSE NULL
                                                      END)     AS last_time_of_dollar_exchanged_for_spin_ticket,
                                                  Max(o.UDate) AS max_updatetime
                                           FROM   Mall_tOrder o
                                                  LEFT JOIN Mall_tOrderProductLog op
                                                         ON op.OrderId = o.OrderId
                                                  LEFT JOIN Mall_tProduct p
                                                         ON op.ProductId = p.Id
                                                  LEFT JOIN Mall_tCurrency c
                                                         ON o.CurrencyCode = c.CurrencyCode
                                           WHERE  o.OrderStatus != 1 AND o.OrderStatus != 41 AND (o.PaymentMode IS NULL OR o.PaymentMode = 1) AND o.udate IS NOT NULL
                                           GROUP  BY o.UserId
                                           ORDER  BY max_updatetime ASC
                                           """))
        return connection.execute(text("""
                                       SELECT o.UserId,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_masterpoint_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_masterpoint_exchanged_for_gold,
                                              MIN(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_masterpoint_exchanged_for_gold,
                                              MAX(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 106 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_masterpoint_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_gold,
                                              MIN(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_gold,
                                              MAX(CASE
                                                    WHEN ( p.Id = 1 OR p.ParentId = 1 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_gold,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  End)     AS amount_of_gold_exchanged_for_silver,
                                              MIN(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_silver,
                                              MAX(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_silver,
                                              MIN(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_silver,
                                              MAX(CASE
                                                    WHEN ( p.Id = 2 OR p.ParentId = 2 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_silver,
                                              SUM(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_lucky_spin,
                                              SUM(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_lucky_spin,
                                              MIN(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_lucky_spin,
                                              MAX(CASE
                                                    WHEN ( p.Id = 3 OR p.ParentId = 3 ) AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_lucky_spin,
                                              SUM(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_lucky_charm,
                                              SUM(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_lucky_charm,
                                              MIN(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_lucky_charm,
                                              MAX(CASE
                                                    WHEN ( p.Id = 5 OR p.ParentId = 5 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_lucky_charm,
                                              SUM(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_avatar,
                                              SUM(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_avatar,
                                              MIN(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_avatar,
                                              MAX(CASE
                                                    WHEN ( p.Id = 6 OR p.ParentId = 6 ) AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_avatar,
                                              SUM(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_gold_exchanged_for_emoji,
                                              SUM(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_gold_exchanged_for_emoji,
                                              MIN(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_gold_exchanged_for_emoji,
                                              MAX(CASE
                                                    WHEN p.ParentId = 35 AND o.CurrencyCode = 101 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_gold_exchanged_for_emoji,
                                              SUM(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_spin_booster,
                                              SUM(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_spin_booster,
                                              MIN(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_spin_booster,
                                              MAX(CASE
                                                    WHEN ( p.Id = 8 OR p.Id = 15 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_spin_booster,
                                              SUM(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN 1
                                                    ELSE 0
                                                  END)     AS count_of_dollar_exchanged_for_spin_ticket,
                                              SUM(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.TotalPrice
                                                    ELSE 0
                                                  END)     AS amount_of_dollar_exchanged_for_spin_ticket,
                                              MIN(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS first_time_of_dollar_exchanged_for_spin_ticket,
                                              MAX(CASE
                                                    WHEN ( p.Id = 9 OR p.Id = 16 ) AND p.ParentId = 3 AND o.CurrencyCode = 201 THEN o.CDate
                                                    ELSE NULL
                                                  END)     AS last_time_of_dollar_exchanged_for_spin_ticket,
                                              Max(o.UDate) AS max_updatetime
                                       FROM   Mall_tOrder o
                                              LEFT JOIN Mall_tOrderProductLog op
                                                     ON op.OrderId = o.OrderId
                                              LEFT JOIN Mall_tProduct p
                                                     ON op.ProductId = p.Id
                                              LEFT JOIN Mall_tCurrency c
                                                     ON o.CurrencyCode = c.CurrencyCode
                                       WHERE  o.OrderStatus != 1 AND o.OrderStatus != 41 AND (o.PaymentMode IS NULL OR o.PaymentMode = 1)
                                              AND o.UserId IN (SELECT DISTINCT UserId
                                                               FROM   Mall_tOrder
                                                               WHERE  UDate >= :update_time)
                                       GROUP  BY o.UserId
                                       ORDER  BY max_updatetime ASC
                                       """), update_time=config_value)

    result_proxy = with_db_context(db, collection, 'orig_wpt_mall')

    rows = [{
        '_user_id': row['UserId'],

        'count_of_masterpoint_exchanged_for_gold': row['count_of_masterpoint_exchanged_for_gold'],
        'amount_of_masterpoint_exchanged_for_gold': row['amount_of_masterpoint_exchanged_for_gold'],
        'first_time_of_masterpoint_exchanged_for_gold': row['first_time_of_masterpoint_exchanged_for_gold'],
        'last_time_of_masterpoint_exchanged_for_gold': row['last_time_of_masterpoint_exchanged_for_gold'],

        'count_of_dollar_exchanged_for_gold': row['count_of_dollar_exchanged_for_gold'],
        'amount_of_dollar_exchanged_for_gold': row['amount_of_dollar_exchanged_for_gold'],
        'first_time_of_dollar_exchanged_for_gold': row['first_time_of_dollar_exchanged_for_gold'],
        'last_time_of_dollar_exchanged_for_gold': row['last_time_of_dollar_exchanged_for_gold'],

        'count_of_gold_exchanged_for_silver': row['count_of_gold_exchanged_for_silver'],
        'amount_of_gold_exchanged_for_silver': row['amount_of_gold_exchanged_for_silver'],
        'first_time_of_gold_exchanged_for_silver': row['first_time_of_gold_exchanged_for_silver'],
        'last_time_of_gold_exchanged_for_silver': row['last_time_of_gold_exchanged_for_silver'],

        'count_of_dollar_exchanged_for_silver': row['count_of_dollar_exchanged_for_silver'],
        'amount_of_dollar_exchanged_for_silver': row['amount_of_dollar_exchanged_for_silver'],
        'first_time_of_dollar_exchanged_for_silver': row['first_time_of_dollar_exchanged_for_silver'],
        'last_time_of_dollar_exchanged_for_silver': row['last_time_of_dollar_exchanged_for_silver'],

        'count_of_dollar_exchanged_for_lucky_spin': row['count_of_dollar_exchanged_for_lucky_spin'],
        'amount_of_dollar_exchanged_for_lucky_spin': row['amount_of_dollar_exchanged_for_lucky_spin'],
        'first_time_of_dollar_exchanged_for_lucky_spin': row['first_time_of_dollar_exchanged_for_lucky_spin'],
        'last_time_of_dollar_exchanged_for_lucky_spin': row['last_time_of_dollar_exchanged_for_lucky_spin'],

        'count_of_gold_exchanged_for_lucky_charm': row['count_of_gold_exchanged_for_lucky_charm'],
        'amount_of_gold_exchanged_for_lucky_charm': row['amount_of_gold_exchanged_for_lucky_charm'],
        'first_time_of_gold_exchanged_for_lucky_charm': row['first_time_of_gold_exchanged_for_lucky_charm'],
        'last_time_of_gold_exchanged_for_lucky_charm': row['last_time_of_gold_exchanged_for_lucky_charm'],

        'count_of_gold_exchanged_for_avatar': row['count_of_gold_exchanged_for_avatar'],
        'amount_of_gold_exchanged_for_avatar': row['amount_of_gold_exchanged_for_avatar'],
        'first_time_of_gold_exchanged_for_avatar': row['first_time_of_gold_exchanged_for_avatar'],
        'last_time_of_gold_exchanged_for_avatar': row['last_time_of_gold_exchanged_for_avatar'],

        'count_of_gold_exchanged_for_emoji': row['count_of_gold_exchanged_for_emoji'],
        'amount_of_gold_exchanged_for_emoji': row['amount_of_gold_exchanged_for_emoji'],
        'first_time_of_gold_exchanged_for_emoji': row['first_time_of_gold_exchanged_for_emoji'],
        'last_time_of_gold_exchanged_for_emoji': row['last_time_of_gold_exchanged_for_emoji'],

        'count_of_dollar_exchanged_for_spin_booster': row['count_of_dollar_exchanged_for_spin_booster'],
        'amount_of_dollar_exchanged_for_spin_booster': row['amount_of_dollar_exchanged_for_spin_booster'],
        'first_time_of_dollar_exchanged_for_spin_booster': row['first_time_of_dollar_exchanged_for_spin_booster'],
        'last_time_of_dollar_exchanged_for_spin_booster': row['last_time_of_dollar_exchanged_for_spin_booster'],

        'count_of_dollar_exchanged_for_spin_ticket': row['count_of_dollar_exchanged_for_spin_ticket'],
        'amount_of_dollar_exchanged_for_spin_ticket': row['amount_of_dollar_exchanged_for_spin_ticket'],
        'first_time_of_dollar_exchanged_for_spin_ticket': row['first_time_of_dollar_exchanged_for_spin_ticket'],
        'last_time_of_dollar_exchanged_for_spin_ticket': row['last_time_of_dollar_exchanged_for_spin_ticket'],

        'max_updatetime': row['max_updatetime']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_updatetime']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'count_of_masterpoint_exchanged_for_gold': bindparam('count_of_masterpoint_exchanged_for_gold'),
                'amount_of_masterpoint_exchanged_for_gold': bindparam('amount_of_masterpoint_exchanged_for_gold'),
                'last_time_of_masterpoint_exchanged_for_gold': bindparam('last_time_of_masterpoint_exchanged_for_gold'),

                'count_of_dollar_exchanged_for_gold': bindparam('count_of_dollar_exchanged_for_gold'),
                'amount_of_dollar_exchanged_for_gold': bindparam('amount_of_dollar_exchanged_for_gold'),
                'last_time_of_dollar_exchanged_for_gold': bindparam('last_time_of_dollar_exchanged_for_gold'),

                'count_of_gold_exchanged_for_silver': bindparam('count_of_gold_exchanged_for_silver'),
                'amount_of_gold_exchanged_for_silver': bindparam('amount_of_gold_exchanged_for_silver'),
                'last_time_of_gold_exchanged_for_silver': bindparam('last_time_of_gold_exchanged_for_silver'),

                'count_of_dollar_exchanged_for_silver': bindparam('count_of_dollar_exchanged_for_silver'),
                'amount_of_dollar_exchanged_for_silver': bindparam('amount_of_dollar_exchanged_for_silver'),
                'last_time_of_dollar_exchanged_for_silver': bindparam('last_time_of_dollar_exchanged_for_silver'),

                'count_of_dollar_exchanged_for_lucky_spin': bindparam('count_of_dollar_exchanged_for_lucky_spin'),
                'amount_of_dollar_exchanged_for_lucky_spin': bindparam('amount_of_dollar_exchanged_for_lucky_spin'),
                'last_time_of_dollar_exchanged_for_lucky_spin': bindparam(
                    'last_time_of_dollar_exchanged_for_lucky_spin'),

                'count_of_gold_exchanged_for_lucky_charm': bindparam('count_of_gold_exchanged_for_lucky_charm'),
                'amount_of_gold_exchanged_for_lucky_charm': bindparam('amount_of_gold_exchanged_for_lucky_charm'),
                'last_time_of_gold_exchanged_for_lucky_charm': bindparam('last_time_of_gold_exchanged_for_lucky_charm'),

                'count_of_gold_exchanged_for_avatar': bindparam('count_of_gold_exchanged_for_avatar'),
                'amount_of_gold_exchanged_for_avatar': bindparam('amount_of_gold_exchanged_for_avatar'),
                'last_time_of_gold_exchanged_for_avatar': bindparam('last_time_of_gold_exchanged_for_avatar'),

                'count_of_gold_exchanged_for_emoji': bindparam('count_of_gold_exchanged_for_emoji'),
                'amount_of_gold_exchanged_for_emoji': bindparam('amount_of_gold_exchanged_for_emoji'),
                'last_time_of_gold_exchanged_for_emoji': bindparam('last_time_of_gold_exchanged_for_emoji'),

                'count_of_dollar_exchanged_for_spin_booster': bindparam('count_of_dollar_exchanged_for_spin_booster'),
                'amount_of_dollar_exchanged_for_spin_booster': bindparam('amount_of_dollar_exchanged_for_spin_booster'),
                'last_time_of_dollar_exchanged_for_spin_booster': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_booster'),

                'count_of_dollar_exchanged_for_spin_ticket': bindparam('count_of_dollar_exchanged_for_spin_ticket'),
                'amount_of_dollar_exchanged_for_spin_ticket': bindparam('amount_of_dollar_exchanged_for_spin_ticket'),
                'last_time_of_dollar_exchanged_for_spin_ticket': bindparam(
                    'last_time_of_dollar_exchanged_for_spin_ticket'),
            }

            column_keys = ['first_time_of_masterpoint_exchanged_for_gold',
                           'first_time_of_dollar_exchanged_for_gold',
                           'first_time_of_gold_exchanged_for_silver',
                           'first_time_of_dollar_exchanged_for_silver',
                           'first_time_of_dollar_exchanged_for_lucky_spin',
                           'first_time_of_gold_exchanged_for_lucky_charm',
                           'first_time_of_gold_exchanged_for_avatar',
                           'first_time_of_gold_exchanged_for_emoji',
                           'first_time_of_dollar_exchanged_for_spin_booster',
                           'first_time_of_dollar_exchanged_for_spin_ticket']
            column_params = {}
            for column in column_keys:
                column_params[column + '_where'] = and_(
                    BIUser.__table__.c.user_id == bindparam('_user_id'),
                    BIUser.__table__.c[column] == None
                )
                column_params[column + '_values'] = {column: bindparam(column)}

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                for column in column_keys:
                    connection.execute(BIUser.__table__.update().where(column_params[column + '_where']).values(
                        column_params[column + '_values']), rows)
                set_config_value(connection, 'last_imported_user_mall_order_update_time', new_config_value)
            except:
                print('process_user_mall_order_newly_updated_records transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_mall_order_newly_updated_records transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_promotion_time():
    config_value = get_config_value(db, 'last_imported_promotion_history_id')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT user_id,
                                                  MIN(CASE
                                                        WHEN push_type = 'fb_notification' THEN scheduled_at
                                                        ELSE NULL
                                                      END) AS first_promotion_fb_notification_time,
                                                  MIN(CASE
                                                        WHEN push_type = 'email' THEN scheduled_at
                                                        ELSE NULL
                                                      END) AS first_promotion_email_time,
                                                  MAX(CASE
                                                        WHEN push_type = 'fb_notification' THEN scheduled_at
                                                        ELSE NULL
                                                      END) AS last_promotion_fb_notification_time,
                                                  MAX(CASE
                                                        WHEN push_type = 'email' THEN scheduled_at
                                                        ELSE NULL
                                                      END) AS last_promotion_email_time,
                                                  MAX(id)  AS max_id
                                           FROM   promotion_push_history
                                           WHERE  status = 'success'
                                           GROUP  BY user_id
                                           ORDER  BY max_id ASC
                                           """))
        return connection.execute(text("""
                                       SELECT user_id,
                                              MIN(CASE
                                                    WHEN push_type = 'fb_notification' THEN scheduled_at
                                                    ELSE NULL
                                                  END) AS first_promotion_fb_notification_time,
                                              MIN(CASE
                                                    WHEN push_type = 'email' THEN scheduled_at
                                                    ELSE NULL
                                                  END) AS first_promotion_email_time,
                                              MAX(CASE
                                                    WHEN push_type = 'fb_notification' THEN scheduled_at
                                                    ELSE NULL
                                                  END) AS last_promotion_fb_notification_time,
                                              MAX(CASE
                                                    WHEN push_type = 'email' THEN scheduled_at
                                                    ELSE NULL
                                                  END) AS last_promotion_email_time,
                                              MAX(id)  AS max_id
                                       FROM   promotion_push_history
                                       WHERE  status = 'success'
                                              AND user_id IN (SELECT DISTINCT user_id
                                                              FROM   promotion_push_history
                                                              WHERE  id > :max_id)
                                       GROUP  BY user_id
                                       ORDER  BY max_id ASC
                                       """), max_id=config_value)

    result_proxy = with_db_context(db, collection)

    rows = [{
        '_user_id': row['user_id'],
        'first_promotion_fb_notification_time': row['first_promotion_fb_notification_time'],
        'first_promotion_email_time': row['first_promotion_email_time'],
        'last_promotion_fb_notification_time': row['last_promotion_fb_notification_time'],
        'last_promotion_email_time': row['last_promotion_email_time'],
        'max_id': row['max_id']
    } for row in result_proxy]

    if rows:
        new_config_value = rows[-1]['max_id']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'last_promotion_email_time': bindparam('last_promotion_email_time'),
                'last_promotion_fb_notification_time': bindparam('last_promotion_fb_notification_time')
            }

            column_keys = ['first_promotion_fb_notification_time',
                           'first_promotion_email_time']
            column_params = {}
            for column in column_keys:
                column_params[column + '_where'] = and_(
                    BIUser.__table__.c.user_id == bindparam('_user_id'),
                    BIUser.__table__.c[column] == None
                )
                column_params[column + '_values'] = {column: bindparam(column)}

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                for column in column_keys:
                    connection.execute(BIUser.__table__.update().where(column_params[column + '_where']).values(
                        column_params[column + '_values']), rows)
                set_config_value(connection, 'last_imported_promotion_history_id', new_config_value)
            except:
                print('process_user_promotion_time transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_promotion_time transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


def process_user_geo_data():
    config_value = get_config_value(db, 'last_imported_user_geo_data_user_id')

    def collection(connection, transaction):
        """ Get newly added. """
        if config_value is None:
            return connection.execute(text("""
                                           SELECT user_id, reg_ip
                                           FROM   bi_user
                                           ORDER  BY user_id ASC
                                           """))
        return connection.execute(text("""
                                       SELECT user_id, reg_ip
                                       FROM   bi_user
                                       WHERE  user_id > :user_id
                                       ORDER  BY user_id ASC
                                       """), user_id=config_value)

    result_proxy = with_db_context(db, collection)

    rows = []
    for row in result_proxy:
        if row['reg_ip'] is None or len(row['reg_ip']) == 0:
            continue

        try:
            resp = geoip_reader.city(row['reg_ip'])
            rows.append({
                '_user_id': row['user_id'],
                'reg_country': resp.country.name,
                'reg_state': resp.subdivisions.most_specific.name,
                'reg_city': resp.city.name
            })
        except:
            continue

    if rows:
        new_config_value = rows[-1]['_user_id']

        def sync_collection(connection, transaction):
            """ Sync newly added. """
            where = BIUser.__table__.c.user_id == bindparam('_user_id')
            values = {
                'reg_country': bindparam('reg_country'),
                'reg_state': bindparam('reg_state'),
                'reg_city': bindparam('reg_city')
            }

            try:
                connection.execute(BIUser.__table__.update().where(where).values(values), rows)
                set_config_value(connection, 'last_imported_user_geo_data_user_id', new_config_value)
            except:
                print('process_user_geo_data transaction.rollback()')
                transaction.rollback()
                raise
            else:
                print('process_user_geo_data transaction.commit()')
                transaction.commit()
            return

        with_db_context(db, sync_collection)

    return


@celery.task
def process_bi_user():
    process_user_newly_added_records()
    print('process_user_newly_added_records() done.')

    process_user_newly_updated_records()
    print('process_user_newly_updated_records() done.')

    process_user_billing_info_added_records()
    print('process_user_billing_info_added_records() done.')

    process_user_info_newly_added_records()
    print('process_user_info_newly_added_records() done.')

    process_user_info_newly_updated_records()
    print('process_user_info_newly_updated_records() done.')

    process_user_login_newly_added_records()
    print('process_user_login_newly_added_records() done.')

    process_user_ourgame_newly_added_records()
    print('process_user_ourgame_newly_added_records() done.')

    process_user_ourgame_newly_updated_records()
    print('process_user_ourgame_newly_updated_records() done.')

    process_user_payment_spin_purchase_newly_added_records()
    print('process_user_payment_spin_purchase_newly_added_records() done.')

    process_user_payment_spin_purchase_newly_updated_records()
    print('process_user_payment_spin_purchase_newly_updated_records() done.')

    process_user_mall_order_newly_added_records()
    print('process_user_mall_order_newly_added_records() done.')

    process_user_mall_order_newly_updated_records()
    print('process_user_mall_order_newly_updated_records() done.')

    process_user_geo_data()
    print('process_user_geo_data() done.')

    process_user_gold_balance_related_records()
    print('process_user_gold_balance_related_records() done.')

    process_user_silver_balance_related_records()
    print('process_user_silver_balance_related_records() done.')

    process_user_reward_point_related_records()
    print('process_user_reward_point_related_records() done.')

#
# @celery.task
# def process_bi_user_for_bi_user_currency_database():
#     def process_bi_user_sync_records_for_bi_user_currency_database():
#
#         config_value = get_config_value(db, 'last_sync_bi_user_for_bi_user_currency_database')
#
#         def collection(connection, transaction):
#             """ Get newly added. """
#             if config_value is None:
#                 return connection.execute(text("""
#                                            SELECT * FROM  bi_user
#                                            ORDER BY  id ASC
#                                                """))
#             return connection.execute(text("""
#                                            SELECT * FROM  bi_user WHERE id > :config_id
#                                            ORDER BY  id ASC
#                                            """), config_id=config_value)
#
#         result_proxy = with_db_context(db, collection)
#
#         rows = [dict(row.items()) for row in result_proxy]
#
#         # def remove_user_id_row(rows):
#         #     for row_dict in rows:
#         #         row_dict['_user_id'] = row_dict.pop('user_id')
#         #
#         # remove_user_id_row(rows)
#         # column_names = BIUser.__table__.columns.keys()
#         # del column_names[1]
#
#         print('process_user_newly_added_records new data for bi_user_currency database: ' + str(len(rows)))
#
#         if rows:
#             new_config_value = rows[-1]['id']
#
#             def sync_collection(connection, transaction):
#                 """ Sync newly added. """
#
#                 try:
#                     connection.execute(BIUserInBIUserCurrencyDatabase.__table__.insert(), rows)
#                     set_config_value_with_db_instance(db, 'last_sync_bi_user_for_bi_user_currency_database', new_config_value)
#
#                 except:
#
#                     print('process_bi_user_addly_records for bi user currency transaction.rollback()')
#                     transaction.rollback()
#                     raise
#
#                 else:
#                     print('process_bi_user_addly_records for bi user currency transaction.commit()')
#                     transaction.commit()
#                 return
#
#             with_db_context(db, sync_collection, bind='bi_currency')
#
#         return
#
#     def process_bi_user_updated_records_for_bi_user_currency_dabase():
#         config_value = get_config_value(db, 'last_update_bi_user_for_bi_user_currency_database')
#
#         def collection(connection, transaction):
#             """ Get newly added. """
#             if config_value is None:
#                 return connection.execute(text("""
#                                            SELECT * FROM  bi_user
#                                            ORDER BY  updated_at ASC
#                                                """))
#
#             return connection.execute(text("""
#                                            SELECT * FROM  bi_user  WHERE  updated_at >= :update_time
#                                            ORDER BY  updated_at ASC
#                                                """), update_time=config_value)
#
#         result_proxy = with_db_context(db, collection)
#
#         rows = [dict(row.items()) for row in result_proxy]
#
#         def remove_user_id_row(rows):
#             for row_dict in rows:
#                 row_dict['_user_id'] = row_dict.pop('user_id')
#
#         remove_user_id_row(rows)
#
#         column_names = BIUserInBIUserCurrencyDatabase.__table__.columns.keys()
#
#         del column_names[1]
#
#         if rows:
#             new_config_value = rows[-1]['updated_at']
#
#             def sync_collection(connection, transaction):
#                 """ Sync newly added. """
#                 where = BIUserInBIUserCurrencyDatabase.__table__.c.user_id == bindparam('_user_id')
#
#                 values = {column_name: lambda column_name: bindparam(column_name) for column_name in column_names}
#
#                 try:
#                     connection.execute(BIUserInBIUserCurrencyDatabase.__table__.update().where(where).values(values), rows)
#                     set_config_value_with_db_instance(db, 'last_update_bi_user_for_bi_user_currency_database', new_config_value)
#                 except:
#                     print('process_bi_user uppdate records for bi_user_currency_database transaction.rollback()')
#                     transaction.rollback()
#                     raise
#                 else:
#                     print('process_bi_user uppdate records for bi_user_currency_database transaction.commit()')
#                     transaction.commit()
#                 return
#
#             with_db_context(db, sync_collection, bind='bi_currency')
#
#         return
#
#     process_bi_user_sync_records_for_bi_user_currency_database()
#     process_bi_user_updated_records_for_bi_user_currency_dabase()
