import numpy
import os
import pandas as pd
import sqlparse
from flask import current_app as app
from sqlalchemy import text

from app import BIUser
from app.constants import ADMIN_USER_QUERY_STATUSES, SQL_RESULT_STRATEGIES
from app.extensions import db
from app.libs.dataframe import DataFrame
from app.models.main import AdminUser, AdminUserQuery
from app.tasks import celery
from app.tasks.mail import send_mail
from app.utils import current_time, current_time_as_float, error_msg_from_exception, dedup


@celery.task
def get_sql_results(database, query_id, strategy=SQL_RESULT_STRATEGIES.RENDER_JSON.value, include_email=False):
    """Executes the sql query returns the results."""

    query = db.session.query(AdminUserQuery).filter_by(id=query_id).one()
    query_id = query.id

    def handle_error(msg):
        """Local method handling error while processing the SQL"""
        query.error_message = msg
        query.status = ADMIN_USER_QUERY_STATUSES.FAILED.value
        db.session.commit()
        raise Exception(msg)

    try:

        variables = [sql for sql in sqlparse.parse(query.sql) if
                     str([tokens for tokens in sql.tokens if tokens.is_keyword][0]).upper() == 'SET']

        if variables:
            for variable in variables:
                db.get_engine(db.get_app(), bind=database).execute(text(str(variable)))
                db.session.flush()
            parsed_sql = [sql for sql in sqlparse.parse(query.sql) if
                          str([tokens for tokens in sql.tokens if tokens.is_keyword][0]).upper() != 'SET'][0]


        else:

            parsed_sql = sqlparse.parse(query.sql)[0]

        sql_tokens = [str(tokens).upper() for tokens in [tokens for tokens in parsed_sql.tokens if tokens.is_keyword]]
        sql_limit = any([tokens in ['INSERT', 'UPDATE', 'DELETE', 'USE', 'DROP', 'TRUNCATE'] for tokens in sql_tokens])

        if sql_limit:
            handle_error("Your SQL statements are not allowed against this database")

        start_time = current_time_as_float()

        result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(str(parsed_sql)))

        query.status = ADMIN_USER_QUERY_STATUSES.RUNNING.value
        db.session.flush()

        result = None
        if result_proxy.cursor:
            column_names = dedup([col[0] for col in result_proxy.cursor.description])
            data = result_proxy.fetchall()
            result = DataFrame(pd.DataFrame(data, columns=column_names))

            # 跨库获取用户邮箱等信息,额外添加到查询的结果上

            if include_email:

                def add_extra_data_to_result(result, data, column_names):

                    if 'user_id'.upper() in column_names:
                        user_identification = list(result.dateframe['user_id'.upper()])
                        user_identification = [int(i) for i in user_identification]
                        extra_result_proxy = db.session.query(BIUser.user_id, BIUser.username, BIUser.og_account,
                                                              BIUser.email).filter(
                            BIUser.user_id.in_(user_identification)).filter(
                            BIUser.email_promotion_allowed == True, BIUser.account_status_orig == 10,
                            BIUser.email is not None)


                    elif 'username'.upper() in column_names:
                        user_identification = list(result.dateframe['username'.upper()])
                        user_identification = [str(i) for i in user_identification if isinstance(i, (numpy.int64, str))]
                        extra_result_proxy = db.session.query(BIUser.user_id, BIUser.username, BIUser.og_account,
                                                              BIUser.email).filter(
                            BIUser.username.in_(user_identification)).filter(
                            BIUser.email_promotion_allowed == True, BIUser.account_status_orig == 10,
                            BIUser.email is not None)


                    elif 'og_account'.upper() in column_names:
                        user_identification = list(result.dateframe['og_account'.upper()])
                        user_identification = [str(i) for i in user_identification if isinstance(i, (numpy.int64, str))]
                        extra_result_proxy = db.session.query(BIUser.user_id, BIUser.username, BIUser.og_account,
                                                              BIUser.email).filter(
                            BIUser.og_account.in_(user_identification)).filter(
                            BIUser.email_promotion_allowed == True, BIUser.account_status_orig == 10,
                            BIUser.email is not None)

                    else:
                        return result

                    extra_column_names = [c["name"] for c in extra_result_proxy.column_descriptions]
                    extra_column_names = [column_name.upper() for column_name in extra_column_names]
                    extra_data = extra_result_proxy.all()

                    result = pd.DataFrame(data, columns=column_names)
                    extra_result = pd.DataFrame(extra_data, columns=extra_column_names)
                    result = DataFrame(pd.merge(result, extra_result))

                    return result

                result = add_extra_data_to_result(result, data, column_names)

        # counting rows
        query.rows = result_proxy.rowcount
        if query.rows == -1 and result:
            query.rows = result.size

        if strategy == SQL_RESULT_STRATEGIES.RENDER_JSON.value and query.rows > 50000:
            handle_error(
                "The query exceeded the maximum record limit: 50000. You may want to run your query with a LIMIT.")
        else:
            query.run_time = round(current_time_as_float() - start_time, 3)
            query.status = ADMIN_USER_QUERY_STATUSES.SUCCESS.value

            db.session.flush()
            db.session.commit()

        #########

        now = current_time(app.config['APP_TIMEZONE'])
        sql_key = query.sql_key
        current_user_id = query.admin_user_id

        if strategy == SQL_RESULT_STRATEGIES.RENDER_JSON.value:
            return {
                'query_id': query_id,
                'columns': result.columns if result else [],
                'data': result.data if result else [],
                'rows': query.rows,
                'run_time': query.run_time
            }
        elif strategy == SQL_RESULT_STRATEGIES.SEND_TO_MAIL.value:
            admin_user = db.session.query(AdminUser).filter_by(id=current_user_id).one()

            filename = '%s_%s.%s' % (sql_key, now.timestamp, app.config['REPORT_FILE_EXTENSION'])
            path = os.path.join(app.config['REPORT_FILE_FOLDER'], filename)
            result.dateframe.to_csv(path, compression=app.config['REPORT_FILE_COMPRESSION'], encoding='utf-8')

            send_mail(admin_user.email,
                      'SQL Lab result - %s' % sql_key,
                      'sql_result_report',
                      path,
                      app.config['REPORT_FILE_CONTENT_TYPE'],
                      username=admin_user.name,
                      sql=str(parsed_sql),
                      filename=filename,
                      generated_at=now.format())
        elif strategy == SQL_RESULT_STRATEGIES.GENERATE_DOWNLOAD_LINK.value:
            # filename = '%s.%s' % (sql_key, app.config['REPORT_FILE_EXTENSION'])
            filename = '%s_%s.%s' % (sql_key, now.timestamp, app.config['REPORT_FILE_EXTENSION'])
            path = os.path.join(app.config['REPORT_FILE_FOLDER'], filename)
            result.dateframe.to_csv(path, compression=app.config['REPORT_FILE_COMPRESSION'], encoding='utf-8')
            print('%s/sql_lab/download?key=%s&ts=%s' % (app.config['APP_HOST'], sql_key, now.timestamp))

            return {
                'query_id': query_id,
                'download_link': '%s/sql_lab/download?key=%s&ts=%s' % (app.config['APP_HOST'], sql_key, now.timestamp)
            }
        return None
    except Exception as e:
        handle_error(error_msg_from_exception(e))
