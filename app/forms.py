from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import EqualTo, DataRequired, ValidationError

from app.models.main import AdminUser


class PasswordCheck:
    message = """
        <br> At least 8 characters length <br>
    """

    def __init__(self):
        self.message = self.__class__.message

    def __call__(self, form, field):
        password = field.data
        # calculating the length
        length_error = len(password) < 8

        if length_error:
            raise ValidationError(self.message)

        else:
            return True


class ResetPasswordForm(FlaskForm):
    password1 = PasswordField('New password',
                              validators=[DataRequired(), EqualTo('password2', message='Passwords must match'),
                                          PasswordCheck()])
    password2 = PasswordField('Confirm new password', validators=[DataRequired()])


class ForgetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])

    def validate(self):
        check_validate = super(ForgetForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        # Does our the exist
        user = AdminUser.query.filter_by(email=self.email.data.strip()).first()
        if not user:
            self.email.errors.append('Invalid email ')
            return False
        else:
            return True


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    def validate(self):
        check_validate = super(LoginForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        # Does our the exist
        user = AdminUser.query.filter_by(email=self.email.data.strip()).first()
        if not user:
            self.email.errors.append('Invalid email ')
            return False

        if not user.active:
            self.email.errors.append('Your account has been frozen, please contact the administrator')
            return False

        # Do the passwords match
        if not user.check_password(self.password.data):
            self.email.errors.append('Invalid password')
            return False

        return True
