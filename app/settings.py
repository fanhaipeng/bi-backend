from datetime import timedelta

import tempfile
from celery.schedules import crontab
from kombu import Queue, Exchange

db_file = tempfile.NamedTemporaryFile()

APP_HOST = 'https://bi.play-wpt.com'
SECRET_KEY = 'AKIAIPR2RGTM6SOLMKBQ'
APP_TIMEZONE = 'America/New_York'
BRITEVERIFY_API_KEY = '9faf46ca-87f1-4d49-8a0e-b3a9d8d03b24'
WECHAT = '/home/deployer/wpt-dwh/wechat.sh'


class Config(object):
    SECRET_KEY = 'REPLACE ME'


class ProdConfig(Config):
    ENV = 'prod'
    DEBUG = False
    ASSETS_DEBUG = False

    USE_X_SENDFILE = False
    WTF_CSRF_ENABLED = True

    AWS_SNS_KEY = 'AKIAJL4CIDW4R33JVTAQ'
    AWS_SNS_SECRET = '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'

    APP_HOST = 'https://bi.play-wpt.com'
    APP_TIMEZONE = 'America/New_York'

    PREFERRED_URL_SCHEME = 'https'

    REPORT_FILE_EXTENSION = 'csv.gz'
    REPORT_FILE_COMPRESSION = 'gzip'
    REPORT_FILE_CONTENT_TYPE = 'application/gzip'
    REPORT_FILE_FOLDER = '/data/data/app_exported'

    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_POOL_SIZE = 50
    SQLALCHEMY_POOL_RECYCLE = 3000
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://bi_prod_all:gyFiv1d3Cv8ueL29Vi48Y0neAEWOwSvu@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/bi_prod'
    SQLALCHEMY_BINDS = {

        'bi_lifetime': 'mysql+pymysql://bi_lifetime_prod_rw:32GU8NPWwqNUeOcc0sW9@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/bi_lifetime_prod',
        'bi_promotion': 'mysql+pymysql://bi_promotion_prod_all:Z4sTBGqLcxDBcQaBTG192RaYLMrezzvl@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/bi_promotion_prod',
        'bi_user_statistic': 'mysql+pymysql://bi_user_statistic_prod_all:5dHirf0nugxpMjpgnL22PBzjzQVO7x7n@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/bi_user_statistic_prod',

        'wpt_log': 'mysql+pymysql://wptlog_r:at9RCUI8wzJzF28irNcc@playwpt-mariadb-replica-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/wptlog?charset=utf8',
        'orig_wpt': 'mysql+pymysql://wpt_rw:bDGC5LmV3Ec4WNbITTR0@playwpt-mariadb-replica-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/wpt',
        'orig_wpt_bi': 'mysql+pymysql://wptbi_rw:ol6aOtAum977CmObNGKA@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/wptbi',
        'orig_wpt_mall': 'mysql+pymysql://wptmall_r:HLbCBFVvTUZqeMXLfSpW@playwpt-mariadb-replica-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/wptmall',
        'orig_wpt_payment': 'mysql+pymysql://wptpayment_r:S4aamSBldOFTwIGTyITM@playwpt-mariadb-replica-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/wptpayment',
        'orig_wpt_master_points': 'mysql+pymysql://wptmasterpoints_r:wOL90sHAZHAeSWBt88gk@playwpt-mariadb-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/MasterPoints',

        'orig_wpt_ods': 'oracle+cx_oracle://wpt_bi:RNMjta6JMYm387lAjH1X@playwpt-oracle-prod.cjx2abxosmhx.us-east-1.rds.amazonaws.com/orcl?chartset=utf8'

    }

    CELERY_IGNORE_RESULT = True
    CELERY_BROKER_URL = 'amqp://bi:Y4rPXacMcEsmAK4kCes4gUVC@10.10.0.96:5672/bi'
    CELERY_TIMEZONE = 'America/New_York'
    CELERY_DEFAULT_QUEUE = 'default'
    CELERY_CREATE_MISSING_QUEUES = 'default'

    BROKER_HEARTBEAT = 24 * 60 * 60 * 2

    CELERY_ENABLE_UTC = False
    CELERY_DISABLE_RATE_LIMITS = True
    CELERYD_MAX_TASKS_PER_CHILD = 100
    BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 1800}
    CELERYD_FORCE_EXECV = True
    BROKER_POOL_LIMIT = 200

    CELERY_QUEUES = (

        Queue('default', Exchange('default'), routing_key='default'),
        Queue('sync_user_and_bill', Exchange('sync_user_and_bill'), routing_key='sync_user_and_bill'),
        Queue('for_wpt_bi', Exchange('for_wpt_bi'), routing_key='for_wpt_bi'),
        Queue('sync_gold_and_silver', Exchange('sync_gold_and_silver'), routing_key='sync_gold_and_silver'),
        Queue('email', Exchange('email'), routing_key='email'),
        Queue('facebook', Exchange('facebook'), routing_key='facebook'),
        Queue('mobile', Exchange('mobile'), routing_key='mobile'),

        Queue('statistic', Exchange('statistic'), routing_key='statistic'),
        Queue('slow', Exchange('slow'), routing_key='slow'),
    )

    CELERY_IMPORTS = (
        'app.tasks.bi_user',
        'app.tasks.bi_user_bill',
        'app.tasks.bi_user_bill_detail',
        'app.tasks.bi_user_currency',
        'app.tasks.bi_statistic',
        'app.tasks.bi_user_statistic',
        'app.tasks.bi_clubwpt_user',
        'app.tasks.scheduled',
        'app.tasks.sql_lab',
        'app.tasks.sync_wpt_bi',
        'app.tasks.cron_daily_report',
        'app.models.promotion',
        'app.tasks.sendgrid'
    )
    CELERYBEAT_SCHEDULE = {

        ##############sync_user_and_bill####################

        'process_scheduled_bi': {
            'task': 'app.tasks.scheduled.process_bi',
            'schedule': timedelta(seconds=180),
            'options': {'queue': 'sync_user_and_bill'},
        },

        'process_scheduled_bi_user_coin': {
            'task': 'app.tasks.scheduled.process_bi_user_coin',
            'schedule': timedelta(seconds=60 * 60),
            'options': {'queue': 'slow'}
        },
        ##################################

        'process_scheduled_sync_wpt_bi': {
            'task': 'app.tasks.scheduled.process_wpt_bi',
            'schedule': timedelta(seconds=120),
            'options': {'queue': 'for_wpt_bi'}
        },

        ##############sync_gold_and_silver####################

        'process_scheduled_bi_currency': {
            'task': 'app.tasks.scheduled.process_bi_currency',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'sync_gold_and_silver'}
        },

        #################statistic #################3

        'process_scheduled_bi_statistic_for_yesterday': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_yesterday',
            'schedule': crontab(hour=0, minute=20),
            'options': {'queue': 'statistic'}
        },
        'process_scheduled_bi_statistic_for_today': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_today',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'statistic'}
        },
        'process_scheduled_bi_user_statistic_for_yesterday': {
            'task': 'app.tasks.scheduled.process_bi_user_statistic_for_yesterday',
            'schedule': crontab(hour=3, minute=30),
            'options': {'queue': 'default'}
        },

        ##############daily report####################

        'process_scheduled_daily_report': {
            'task': 'app.tasks.scheduled.daily_report',
            'schedule': crontab(hour=0, minute=35),
            'options': {'queue': 'email'}
        },

        ################# auto-promotion #################

        'process_scheduled_auto_promotion': {

            'task': 'app.models.promotion.auto_promotion',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'default'}
        },

        ################# facebook-promotion #################

        'process_scheduled_facebook_promotion_push_1': {
            'task': 'app.models.promotion.facebook_promotion_push',
            'schedule': timedelta(seconds=20 * 60),
            'options': {'queue': 'facebook'},
            'args': (1,),

        },

        'process_scheduled_facebook_promotion_push_2': {
            'task': 'app.models.promotion.facebook_promotion_push',
            'schedule': timedelta(seconds=20 * 60),
            'options': {'queue': 'facebook'},
            'args': (2,)
        },

        #################email_promotion_allowed#################3

        'process_scheduled_update_email_promotion_allowed': {
            'task': 'app.tasks.scheduled.update_email_promotion_allowed',
            'schedule': crontab(hour=0, minute=15),
            'options': {'queue': 'default'}
        },

        #################sendgrid#################3

        'process_scheduled_sync_sendgrid_campaigns': {
            'task': 'app.tasks.scheduled.sync_sendgrid_campaign',
            'schedule': crontab(hour=8, minute=0),
            'options': {'queue': 'default'}
        },

    }

    MAIL_SUBJECT_PREFIX = '[BI System] '
    MAIL_USE_TLS = True

    MAIL_SERVER = 'email-smtp.us-east-1.amazonaws.com'
    MAIL_PORT = 25
    MAIL_USERNAME = 'AKIAI7RN2NTQR2I2LDPQ'
    MAIL_PASSWORD = 'Aq9DT+dPc7ny8n+/r9jgx/HIiHk2oECMqroNJ25bQslm'
    MAIL_DEFAULT_SENDER = 'WPT Report <bi@play-wpt.com>'

    CACHE_TYPE = 'simple'


class DevConfig(Config):

    ENV = 'dev'
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS=False
    ASSETS_DEBUG = True

    USE_X_SENDFILE = False
    WTF_CSRF_ENABLED = True

    AWS_SNS_KEY = 'AKIAJL4CIDW4R33JVTAQ'
    AWS_SNS_SECRET = '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'

    APP_HOST = 'http://localhost:5000'
    APP_TIMEZONE = 'America/New_York'

    PREFERRED_URL_SCHEME = 'https'

    REPORT_FILE_EXTENSION = 'csv.gz'
    REPORT_FILE_COMPRESSION = 'gzip'
    REPORT_FILE_CONTENT_TYPE = 'application/gzip'
    REPORT_FILE_FOLDER = '/data/data/app_exported'

    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_POOL_SIZE = 50
    SQLALCHEMY_POOL_RECYCLE = 3000
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123@127.0.0.1:3306/bi_dev'
    SQLALCHEMY_BINDS = {

        'bi_lifetime': 'mysql+pymysql://root:123@127.0.0.1:3306/bi_lifetime_dev',
        'bi_promotion': 'mysql+pymysql://root:123@127.0.0.1:3306/bi_promotion_dev',
        'bi_user_statistic': 'mysql+pymysql://root:123@127.0.0.1:3306/bi_user_statistic_dev',

        'orig_wpt': 'mysql+pymysql://wpt_rw:a6XJQeS7w9Znun3uRdKX@172.28.14.115:3306/wpt',
        'orig_wpt_bi': 'mysql+pymysql://wptbi_rw:Qg0OmpCH1jUSOId7ytZF@172.28.14.115:3306/wptbi',
        'orig_wpt_mall': 'mysql+pymysql://wptmall_rw:Fk71Asb1YFZyD9axgZyC@172.28.14.115:3306/wptmall',
        'orig_wpt_payment': 'mysql+pymysql://wptpayment_rw:CBgqQM1m318TGxwNie2@172.28.14.115:3306/wpt_payment',
        'orig_wpt_ods': 'mysql+pymysql://localhost/orig_wpt_ods',
        'wpt_log': 'mysql+pymysql://wptlog_rw:Qg0OmpCH1jUSOId7ytZF@172.28.14.115/wptlog?charset=utf8',
        'orig_wpt_master_points': 'mysql+pymysql://MasterPoints_rw:vvp0YvCFlMd5nF6jCudr@172.28.14.115//MasterPoints',

    }

    CELERY_IGNORE_RESULT = True
    CELERY_BROKER_URL = 'amqp://localhost:5672'
    CELERY_TIMEZONE = 'America/New_York'
    CELERY_DEFAULT_QUEUE = 'default'
    CELERY_CREATE_MISSING_QUEUES = 'default'

    BROKER_HEARTBEAT = 24 * 60 * 60 * 2

    CELERY_ENABLE_UTC = False
    CELERY_DISABLE_RATE_LIMITS = True
    CELERYD_MAX_TASKS_PER_CHILD = 100
    BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 1800}
    CELERYD_FORCE_EXECV = True
    BROKER_POOL_LIMIT = 200

    CELERY_QUEUES = (

        Queue('default', Exchange('default'), routing_key='default'),
        Queue('sync_user_and_bill', Exchange('sync_user_and_bill'), routing_key='sync_user_and_bill'),
        Queue('for_wpt_bi', Exchange('for_wpt_bi'), routing_key='for_wpt_bi'),
        Queue('sync_gold_and_silver', Exchange('sync_gold_and_silver'), routing_key='sync_gold_and_silver'),
        Queue('email', Exchange('email'), routing_key='email'),
        Queue('facebook', Exchange('facebook'), routing_key='facebook'),
        Queue('mobile', Exchange('mobile'), routing_key='mobile'),

        Queue('statistic', Exchange('statistic'), routing_key='statistic'),
        Queue('slow', Exchange('slow'), routing_key='slow'),
    )

    CELERY_IMPORTS = (
        'app.tasks.bi_user',
        'app.tasks.bi_user_bill',
        'app.tasks.bi_user_bill_detail',
        'app.tasks.bi_user_currency',
        'app.tasks.bi_statistic',
        'app.tasks.bi_user_statistic',
        'app.tasks.bi_clubwpt_user',
        'app.tasks.scheduled',
        'app.tasks.sql_lab',
        'app.tasks.sync_wpt_bi',
        'app.tasks.cron_daily_report',
        'app.models.promotion',
        'app.tasks.sendgrid'
    )
    CELERYBEAT_SCHEDULE = {

        ##############sync_user_and_bill####################

        'process_scheduled_bi': {
            'task': 'app.tasks.scheduled.process_bi',
            'schedule': timedelta(seconds=180),
            'options': {'queue': 'sync_user_and_bill'},
        },

        'process_scheduled_bi_user_coin': {
            'task': 'app.tasks.scheduled.process_bi_user_coin',
            'schedule': timedelta(seconds=60 * 60),
            'options': {'queue': 'slow'}
        },
        ##################################

        'process_scheduled_sync_wpt_bi': {
            'task': 'app.tasks.scheduled.process_wpt_bi',
            'schedule': timedelta(seconds=120),
            'options': {'queue': 'for_wpt_bi'}
        },

        ##############sync_gold_and_silver####################

        'process_scheduled_bi_currency': {
            'task': 'app.tasks.scheduled.process_bi_currency',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'sync_gold_and_silver'}
        },

        #################statistic #################3

        'process_scheduled_bi_statistic_for_yesterday': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_yesterday',
            'schedule': crontab(hour=0, minute=20),
            'options': {'queue': 'statistic'}
        },
        'process_scheduled_bi_statistic_for_today': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_today',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'statistic'}
        },
        'process_scheduled_bi_user_statistic_for_yesterday': {
            'task': 'app.tasks.scheduled.process_bi_user_statistic_for_yesterday',
            'schedule': crontab(hour=3, minute=30),
            'options': {'queue': 'default'}
        },

        ##############daily report####################

        'process_scheduled_daily_report': {
            'task': 'app.tasks.scheduled.daily_report',
            'schedule': crontab(hour=0, minute=35),
            'options': {'queue': 'email'}
        },

        ################# auto-promotion #################

        'process_scheduled_auto_promotion': {

            'task': 'app.models.promotion.auto_promotion',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'default'}
        },

        ################# facebook-promotion #################

        'process_scheduled_facebook_promotion_push_1': {
            'task': 'app.models.promotion.facebook_promotion_push',
            'schedule': timedelta(seconds=20 * 60),
            'options': {'queue': 'facebook'},
            'args': (1,),

        },

        'process_scheduled_facebook_promotion_push_2': {
            'task': 'app.models.promotion.facebook_promotion_push',
            'schedule': timedelta(seconds=20 * 60),
            'options': {'queue': 'facebook'},
            'args': (2,)
        },

        #################email_promotion_allowed#################3

        'process_scheduled_update_email_promotion_allowed': {
            'task': 'app.tasks.scheduled.update_email_promotion_allowed',
            'schedule': crontab(hour=0, minute=15),
            'options': {'queue': 'default'}
        },

        #################sendgrid#################3

        'process_scheduled_sync_sendgrid_campaigns': {
            'task': 'app.tasks.scheduled.sync_sendgrid_campaign',
            'schedule': crontab(hour=8, minute=0),
            'options': {'queue': 'default'}
        },

    }

    MAIL_SUBJECT_PREFIX = '[BI System] '
    MAIL_USE_TLS = True

    MAIL_SERVER = 'smtp.sendgrid.net'
    MAIL_PORT = 587
    MAIL_USERNAME = 'AKIAI7RN2NTQR2I2LDPQ'
    MAIL_PASSWORD = 'Aq9DT+dPc7ny8n+/r9jgx/HIiHk2oECMqroNJ25bQslm'
    MAIL_DEFAULT_SENDER = 'WPT Report <bi@play-wpt.com>'

    CACHE_TYPE = 'simple'


class TestConfig(Config):
    ENV = 'test'
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    USE_X_SENDFILE = False
    WTF_CSRF_ENABLED = False

    SENDGRID_API_KEY = ''

    AWS_SNS_KEY = ''
    AWS_SNS_SECRET = ''

    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + db_file.name
    # SQLALCHEMY_BINDS = {
    #     'orig_wpt':            'mysql+pymysql://localhost/orig_wpt',
    #     'orig_wpt_mall':       'mysql+pymysql://localhost/orig_wpt_mall',
    #     'orig_wpt_payment':    'mysql+pymysql://localhost/orig_wpt_payment',

    #     'orig_slots':          'mysql+pymysql://localhost/orig_slots',
    #     'orig_poker':          'mysql+pymysql://localhost/orig_pokers',

    #     'orig_wpt_ods':        'mysql+pymysql://localhost/orig_wpt_ods',
    # }
    SQLALCHEMY_ECHO = True

    CELERY_BROKER_URL = 'amqp://localhost:5672'
    CELERY_TIMEZONE = 'America/New_York'
    CELERY_IMPORTS = (
        'app.tasks.bi_user',
        'app.tasks.bi_user_bill',
        'app.tasks.bi_user_bill_detail',
        'app.tasks.bi_user_currency',
        'app.tasks.bi_statistic',
        'app.tasks.bi_user_statistic',
        'app.tasks.save_records_to_redis',
        'app.tasks.bi_clubwpt_user',
        'app.tasks.scheduled',
        'app.tasks.sql_lab',
        'app.tasks.promotion',
        'app.tasks.sync_wpt_bi',
        'app.tasks.cron_daily_report',
        'app.tasks.sendgrid'
    )
    CELERYBEAT_SCHEDULE = {
        'process_scheduled_bi': {
            'task': 'app.tasks.scheduled.process_bi',
            'schedule': timedelta(seconds=8 * 60),
            'options': {'queue': 'for_sync_data'}
        },
        'process_scheduled_sync_wpt_bi': {
            'task': 'app.tasks.scheduled.process_wpt_bi',
            'schedule': timedelta(seconds=5 * 60),
            'options': {'queue': 'for_wpt_bi'}
        },
        'process_scheduled_bi_currency': {
            'task': 'app.tasks.scheduled.process_bi_currency',
            'schedule': crontab(minute=20),
            'options': {'queue': 'for_sync_data'}
        },
        'process_scheduled_daily_report': {
            'task': 'app.tasks.scheduled.daily_report',
            'schedule': crontab(hour=5, minute=30),
            'options': {'queue': 'for_sync_data'}
        },
        'process_scheduled_promotion_push': {
            'task': 'app.tasks.scheduled.process_promotion_push',
            'schedule': timedelta(seconds=6 * 60),
            'options': {'queue': 'for_promotion'}
        },
        'process_scheduled_bi_statistic_for_yesterday': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_yesterday',
            'schedule': crontab(hour=3, minute=0),
            'options': {'queue': 'default'}
        },
        'process_scheduled_bi_statistic_for_today': {
            'task': 'app.tasks.scheduled.process_bi_statistic_for_today',
            'schedule': timedelta(seconds=20 * 60),
            'options': {'queue': 'default'}
        },

        'process_scheduled_sync_sendgrid_campaigns': {
            'task': 'app.tasks.scheduled.sync_sendgrid_campaign',
            'schedule': timedelta(seconds=60 * 60),
            'options': {'queue': 'default'}
        }
    }

    CACHE_TYPE = 'simple'
    #
    # MAIL_SERVER = 'email-smtp.us-east-1.amazonaws.com'
    # MAIL_PORT = 25
    # MAIL_USERNAME = 'AKIAI7RN2NTQR2I2LDPQ'
    # MAIL_PASSWORD = 'Aq9DT+dPc7ny8n+/r9jgx/HIiHk2oECMqroNJ25bQslm'
    # MAIL_DEFAULT_SENDER = 'WPT Report <bi@play-wpt.com>'

    # CACHE_TYPE = 'simple'

    MAIL_SERVER = 'smtp.sendgrid.net'
    MAIL_PORT = 587

    MAIL_USERNAME = 'accounts@playwpt.com'
    MAIL_PASSWORD = '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'
    MAIL_DEFAULT_SENDER = 'WPT Report <bi@play-wpt.com>'
#