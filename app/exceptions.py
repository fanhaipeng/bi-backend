class TimeoutException(Exception):
    pass


class SQLException(Exception):
    pass


class ecurityException(Exception):
    pass


class MetricPermException(Exception):
    pass


class NoDataException(Exception):
    pass


class TemplateException(Exception):
    pass
