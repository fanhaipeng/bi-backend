import atexit
import fcntl

from flask import Flask, render_template
from flask_admin import Admin
from flask_apscheduler import APScheduler
from flask_login import LoginManager

from app.assets import assets
from app.extensions import db, debug_toolbar, login, gravatar, mail, migrate, scheduler
from app.libs.json_encoder import FlaskJSONEncoder
from app.utils import  md5
from app.models.bi import (BIUser,
                           BIUserCurrency,
                           BIUserBill,
                           BIUserBillDetail,
                           BIClubWPTUser,
                           WPTUserLoginLog, BIStatistic)
from app.models.main import AdminUser, AdminUserActivity
from app.models.orig_wpt import WPTUserLoginLog
from app.models.orig_wpt_ods import WPTODSTICKETDETAIL
from app.views import (AdminBaseIndexView,
                       AdminBaseModelView,
                       AdminUserModelView,
                       AdminBIUserModelView,
                       AdminBIUserCurrencyModelView,
                       AdminBIUserBillModelView,
                       AdminBIUserBillDetailModelView,
                       AdminBIClubWPTUserModelView,
                       AdminWPTUserLoginLogModelView, AdminBIUserTicketDetailModelView)

login_manager = LoginManager()
# 实施会话保护,login_user(curr_user, remember=True)的情况下,更换浏览器,自动退出
login_manager.session_protection = "strong"




def create_app(object_name, register_blueprint=True):
    """
    An flask application factory, as explained here:
    http://flask.pocoo.org/docs/patterns/appfactories/

    Arguments:
        object_name: the python path of the config object,
                     e.g. app.settings.ProdConfig
    """

    #  to solve oracle chinese garbled issue
    import os
    os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.AL32UTF8'

    app = Flask(__name__, static_folder='public', static_url_path='')
    app.config.from_object(object_name)

    class ReverseProxied(object):
        def __init__(self, app):
            self.app = app

        def __call__(self, environ, start_response):
            return self.app(environ, start_response)

    if app.debug == False:
        app.wsgi_app = ReverseProxied(app.wsgi_app)

    app.json_encoder = FlaskJSONEncoder

    register_errorhandlers(app)
    register_extensions(app)

    if register_blueprint:
        register_blueprints(app)

    return app


def register_extensions(app):
    """Register Flask extensions."""

    debug_toolbar.init_app(app)

    db.init_app(app)

    login_manager.init_app(app)

    migrate.init_app(app, db)

    mail.init_app(app)

    gravatar.init_app(app)

    assets.init_app(app)

    # 解决scheduler多线程多次触发的问题

    f = open("scheduler.lock", "wb")

    try:
        fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        scheduler = APScheduler()
        scheduler.init_app(app)
        scheduler.start()
    except:
        pass

    def unlock():
        fcntl.flock(f, fcntl.LOCK_UN)
        f.close()

    atexit.register(unlock)

    import warnings

    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', 'Fields missing from ruleset', UserWarning)
        admin = Admin(app, index_view=AdminBaseIndexView(), url='/data')
        admin.add_view(AdminUserModelView(AdminUser, db.session, menu_icon_type='fa', menu_icon_value='fa-circle-o',
                                          endpoint='admin_user'))
        admin.add_view(
            AdminBIUserModelView(BIUser, db.session, name='BI User', menu_icon_type='fa', menu_icon_value='fa-circle-o',
                                 endpoint='bi_user'))
        admin.add_view(
            AdminBIUserCurrencyModelView(BIUserCurrency, db.session, name='BI User Currency', menu_icon_type='fa',
                                         menu_icon_value='fa-circle-o', endpoint='bi_user_currency'))
        admin.add_view(AdminBIUserBillModelView(BIUserBill, db.session, name='BI User Bill', menu_icon_type='fa',
                                                menu_icon_value='fa-circle-o', endpoint='bi_user_bill'))
        admin.add_view(AdminBIUserBillDetailModelView(BIUserBillDetail, db.session, name='BI User Bill Detail',
                                                      menu_icon_type='fa', menu_icon_value='fa-circle-o',
                                                      endpoint='bi_user_bill_detail'))
        admin.add_view(
            AdminBIClubWPTUserModelView(BIClubWPTUser, db.session, name='BI ClubWPT User', menu_icon_type='fa',
                                        menu_icon_value='fa-circle-o', endpoint='bi_clubwpt_user'))
        admin.add_view(
            AdminWPTUserLoginLogModelView(WPTUserLoginLog, db.session, name='User Login Log', menu_icon_type='fa',
                                          menu_icon_value='fa-circle-o', endpoint='user_login_log'))
        admin.add_view(
            AdminBIUserTicketDetailModelView(WPTODSTICKETDETAIL, db.session, name='User Tickets', menu_icon_type='fa',
                                             menu_icon_value='fa-circle-o', endpoint='tickets_detail'))

    login.init_app(app)
    login.login_view = "account.sign_in"
    login.login_message_category = "warning"

    @login.user_loader
    def load_user(user_id):
        return AdminUser.query.get(user_id)


def register_blueprints(app):
    """Register Flask blueprints."""

    from app.controllers.account import account
    from app.controllers.dashboard import dashboard
    from app.controllers.wallboard import wallboard
    from app.controllers.report import report
    from app.controllers.auto_report import auto_report

    from app.controllers.sql_lab import sql_lab
    from app.controllers.page import page
    from app.controllers.promotion import promotion
    from app.controllers.sendgrid import sendgrid
    from app.controllers.api import api

    app.register_blueprint(account)
    app.register_blueprint(dashboard)
    app.register_blueprint(wallboard)
    app.register_blueprint(report)
    app.register_blueprint(auto_report)
    app.register_blueprint(sql_lab)
    app.register_blueprint(page)
    app.register_blueprint(promotion)
    app.register_blueprint(sendgrid)
    app.register_blueprint(api)

    from app.libs.context_processor import global_processor
    app.context_processor(global_processor)


    env=app.jinja_env
    env.filters['md5'] = md5


    return None


def register_errorhandlers(app):
    """Register error handlers."""

    def render_error(error):
        """Render error template."""

        error_code = getattr(error, 'code', 500)
        return render_template('error/{0}.html'.format(error_code)), error_code

    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)

    return None
