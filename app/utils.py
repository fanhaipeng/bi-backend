import calendar
import datetime
import hashlib
import importlib
import logging
import signal
from urllib.parse import urlencode

import arrow
import pytz
from flask import current_app as app

from app.exceptions import TimeoutException


def md5(str):
    hash = hashlib.md5()
    hash.update(str.encode('utf-8'))
    md5_encode = hash.hexdigest()

    return md5_encode


def generate_email_validate_url(user_id):
    MD5Key = '6606259CA94711E5B03100155D0E0D0C'
    hash = hashlib.md5()
    hash.update((str(user_id) + MD5Key).encode('utf-8'))
    md5_encode = hash.hexdigest()
    payload = {'ct': md5_encode, 'uid': str(user_id)}
    email_validate_url = urlencode(payload)

    return email_validate_url


def convert_emoji_to_unicode(message):
    """
    将:hearts:样式表达的emoji转换为Unicode形式
    """
    import emoji

    extra_emoji_mapping_1 = {':timer_clock:': '⏲', ':aquarius:': '♒', ':sparkle:': '❇', ':coffin:': '⚰',
                             ':funeral_urn:': '⚱', ':recycle:': '♻', ':snowflake:': '❄', ':baseball:': '⚾',
                             ':heart:': '❤', ':arrow_heading_up:': '⤴', ':partly_sunny:': '⛅', ':fist:': '✊',
                             ':white_check_mark:': '✅', ':grey_question:': '❔', ':leo:': '♌', ':bangbang:': '‼',
                             ':peace_symbol:': '☮', ':heavy_division_sign:': '➗', ':double_vertical_bar:': '⏸',
                             ':heavy_heart_exclamation_mark_ornament:': '❣', ':gemini:': '♊', ':arrow_lower_left:': '↙',
                             ':leftwards_arrow_with_hook:': '↩', ':golf:': '⛳', ':arrow_backward:': '◀',
                             ':white_circle:': '⚪', ':wavy_dash:': '〰', ':arrow_double_down:': '⏬',
                             ':white_large_square:': '⬜', ':star_and_crescent:': '☪', ':biohazard_sign:': '☣',
                             ':pisces:': '♓', ':arrow_lower_right:': '↘', ':scissors:': '✂', ':star_of_david:': '✡',
                             ':skier:': '⛷', ':question:': '❓', ':alembic:': '⚗', ':arrow_up_down:': '↕',
                             ':alarm_clock:': '⏰', ':orthodox_cross:': '☦', ':negative_squared_cross_mark:': '❎',
                             ':information_source:': 'ℹ', ':ophiuchus:': '⛎', ':four:': '4⃣', ':sparkles:': '✨',
                             ':grey_exclamation:': '❕', ':part_alternation_mark:': '〽', ':gear:': '⚙',
                             ':writing_hand:': '✍', ':white_small_square:': '▫', ':zero:': '0⃣', ':fountain:': '⛲',
                             ':arrow_left:': '⬅', ':phone:': '☎', ':one:': '1⃣', ':taurus:': '♉', ':black_circle:': '⚫',
                             ':eject:': '⏏', ':star:': '⭐', ':atom_symbol:': '⚛', ':arrow_down:': '⬇',
                             ':hourglass:': '⌛',
                             ':congratulations:': '㊗', ':black_right_pointing_double_triangle_with_vertical_bar:': '⏭',
                             ':point_up:': '☝', ':radioactive_sign:': '☢', ':pencil2:': '✏',
                             ':skull_and_crossbones:': '☠', ':mountain:': '⛰', ':airplane:': '✈', ':diamonds:': '♦',
                             ':interrobang:': '⁉', ':shinto_shrine:': '⛩', ':fast_forward:': '⏩',
                             ':umbrella_with_rain_drops:': '☔', ':arrow_upper_left:': '↖', ':five:': '5⃣',
                             ':white_medium_square:': '◻', ':umbrella:': '☂', ':fuelpump:': '⛽', ':yin_yang:': '☯',
                             ':no_entry:': '⛔', ':hash:': '#⃣', ':arrow_upper_right:': '↗', ':copyright:': '©',
                             ':helmet_with_white_cross:': '⛑', ':keyboard:': '⌨', ':seven:': '7⃣', ':two:': '2⃣',
                             ':clubs:': '♣', ':arrow_up:': '⬆', ':tm:': '™', ':virgo:': '♍', ':hand:': '✋',
                             ':relaxed:': '☺', ':latin_cross:': '✝', ':thunder_cloud_and_rain:': '⛈', ':x:': '❌',
                             ':aries:': '♈', ':left_right_arrow:': '↔', ':ferry:': '⛴', ':snowman_without_snow:': '⛄',
                             ':white_medium_small_square:': '◽', ':boat:': '⛵', ':snowman:': '☃', ':coffee:': '☕',
                             ':ice_skate:': '⛸', ':six:': '6⃣', ':libra:': '♎', ':ballot_box_with_check:': '☑',
                             ':black_left_pointing_double_triangle_with_vertical_bar:': '⏮', ':warning:': '⚠',
                             ':black_square_for_stop:': '⏹', ':capricorn:': '♑', ':tent:': '⛺', ':hotsprings:': '♨',
                             ':black_small_square:': '▪', ':arrow_right_hook:': '↪', ':keycap_star:': '*⃣',
                             ':hourglass_flowing_sand:': '⏳', ':church:': '⛪', ':arrow_heading_down:': '⤵',
                             ':curly_loop:': '➰', ':arrow_double_up:': '⏫', ':rewind:': '⏪', ':wheel_of_dharma:': '☸',
                             ':black_medium_small_square:': '◾', ':shamrock:': '☘', ':cloud:': '☁', ':stopwatch:': '⏱',
                             ':registered:': '®', ':white_frowning_face:': '☹', ':o:': '⭕', ':zap:': '⚡',
                             ':heavy_minus_sign:': '➖', ':crossed_swords:': '⚔', ':spades:': '♠', ':m:': 'Ⓜ',
                             ':soccer:': '⚽', ':eight_pointed_black_star:': '✴', ':arrow_forward:': '▶',
                             ':heavy_check_mark:': '✔', ':sunny:': '☀', ':umbrella_on_ground:': '⛱',
                             ':fleur_de_lis:': '⚜', ':black_large_square:': '⬛', ':exclamation:': '❗', ':watch:': '⌚',
                             ':hearts:': '♥', ':heavy_multiplication_x:': '✖', ':black_medium_square:': '◼',
                             ':anchor:': '⚓', ':arrow_right:': '➡', ':eight:': '8⃣', ':comet:': '☄',
                             ':black_circle_for_record:': '⏺', ':loop:': '➿', ':scorpius:': '♏',
                             ':black_right_pointing_triangle_with_double_vertical_bar:': '⏯', ':heavy_plus_sign:': '➕',
                             ':email:': '✉', ':eight_spoked_asterisk:': '✳', ':wheelchair:': '♿', ':three:': '3⃣',
                             ':hammer_and_pick:': '⚒', ':nine:': '9⃣', ':secret:': '㊙', ':chains:': '⛓',
                             ':scales:': '⚖',
                             ':cancer:': '♋', ':black_nib:': '✒', ':pick:': '⛏', ':person_with_ball:': '⛹',
                             ':sagittarius:': '♐', ':v:': '✌'}
    extra_emoji_mapping_2 = {':label:': '\U0001f3f7',
                             ':couple:': '\U0001f46b',
                             ':slot_machine:': '\U0001f3b0',
                             ':bikini:': '\U0001f459',
                             ':film_projector:': '\U0001f4fd',
                             ':oncoming_taxi:': '\U0001f696',
                             ':yen:': '\U0001f4b4',
                             ':computer:': '\U0001f4bb',
                             ':clock8:': '\U0001f557', ':ram:': '\U0001f40f',
                             ':shower:': '\U0001f6bf',
                             ':high_heel:': '\U0001f460', ':round_pushpin:': '\U0001f4cd',
                             ':musical_score:': '\U0001f3bc',
                             ':key:': '\U0001f511', ':bento:': '\U0001f371', ':cocktail:': '\U0001f378',
                             ':gift_heart:': '\U0001f49d',
                             ':wind_chime:': '\U0001f390',
                             ':turkey:': '\U0001f983',
                             ':waving_black_flag:': '\U0001f3f4',
                             ':bell:': '\U0001f514', ':smiling_imp:': '\U0001f608', ':orange_book:': '\U0001f4d9',
                             ':car:': '\U0001f697',
                             ':closed_book:': '\U0001f4d5',
                             ':field_hockey_stick_and_ball:': '\U0001f3d1', ':dog2:': '\U0001f415',
                             ':rugby_football:': '\U0001f3c9',
                             ':closed_lock_with_key:': '\U0001f510',
                             ':milky_way:': '\U0001f30c',
                             ':left_speech_bubble:': '\U0001f5e8', ':ticket:': '\U0001f3ab',
                             ':white_flower:': '\U0001f4ae',
                             ':no_good:': '\U0001f645',
                             ':kissing_closed_eyes:': '\U0001f61a', ':horse:': '\U0001f434',
                             ':tulip:': '\U0001f337',
                             ':clock3:': '\U0001f552', ':customs:': '\U0001f6c3', ':ideograph_advantage:': '\U0001f250',
                             ':sunglasses:': '\U0001f60e',
                             ':lemon:': '\U0001f34b', ':cherries:': '\U0001f352', ':door:': '\U0001f6aa',
                             ':cookie:': '\U0001f36a',
                             ':money_with_wings:': '\U0001f4b8',
                             ':chipmunk:': '\U0001f43f', ':blossom:': '\U0001f33c',
                             ':running_shirt_with_sash:': '\U0001f3bd',
                             ':e-mail:': '\U0001f4e7',
                             ':flag-ch:': '\U0001f1e8-\U0001f1ed',
                             ':heavy_dollar_sign:': '\U0001f4b2',
                             ':hatching_chick:': '\U0001f423', ':classical_building:': '\U0001f3db',
                             ':skin-tone-4:': '\U0001f3fd',
                             ':sheep:': '\U0001f411',
                             ':department_store:': '\U0001f3ec', ':u6709:': '\U0001f236',
                             ':tram:': '\U0001f68a',
                             ':hole:': '\U0001f573',
                             ':clock6:': '\U0001f555',
                             ':clapper:': '\U0001f3ac', ':clock1230:': '\U0001f567', ':anger:': '\U0001f4a2',
                             ':chart_with_downwards_trend:': '\U0001f4c9',
                             ':nerd_face:': '\U0001f913', ':no_pedestrians:': '\U0001f6b7', ':lips:': '\U0001f444',
                             ':mag_right:': '\U0001f50e',
                             ':footprints:': '\U0001f463', ':cool:': '\U0001f192',
                             ':page_facing_up:': '\U0001f4c4',
                             ':dollar:': '\U0001f4b5', ':deciduous_tree:': '\U0001f333',
                             ':kissing_smiling_eyes:': '\U0001f619',
                             ':chicken:': '\U0001f414',
                             ':sports_medal:': '\U0001f3c5', ':ant:': '\U0001f41c', ':seedling:': '\U0001f331',
                             ':fries:': '\U0001f35f',
                             ':man:': '\U0001f468',
                             ':turtle:': '\U0001f422', ':eggplant:': '\U0001f346',
                             ':man-man-girl-boy:': '\U0001f468-200d-\U0001f468-200d-\U0001f467-200d-\U0001f466',
                             ':neutral_face:': '\U0001f610',
                             ':trolleybus:': '\U0001f68e',
                             ':heart_eyes:': '\U0001f60d', ':goat:': '\U0001f410', ':cl:': '\U0001f191',
                             ':grin:': '\U0001f601',
                             ':arrow_down_small:': '\U0001f53d', ':notebook_with_decorative_cover:': '\U0001f4d4',
                             ':slightly_smiling_face:': '\U0001f642',
                             ':couch_and_lamp:': '\U0001f6cb', ':koko:': '\U0001f201', ':fire_engine:': '\U0001f692',
                             ':handbag:': '\U0001f45c',
                             ':fallen_leaf:': '\U0001f342',
                             ':stuck_out_tongue:': '\U0001f61b',
                             ':microphone:': '\U0001f3a4', ':hocho:': '\U0001f52a',
                             ':see_no_evil:': '\U0001f648',
                             ':black_joker:': '\U0001f0cf', ':put_litter_in_its_place:': '\U0001f6ae',
                             ':zzz:': '\U0001f4a4',
                             ':sweat_smile:': '\U0001f605',
                             ':dvd:': '\U0001f4c0', ':two_hearts:': '\U0001f495',
                             ':ng:': '\U0001f196',
                             ':free:': '\U0001f193',
                             ':jeans:': '\U0001f456', ':kiss:': '\U0001f48b',
                             ':thermometer:': '\U0001f321',
                             ':books:': '\U0001f4da',
                             ':large_blue_diamond:': '\U0001f537', ':rabbit2:': '\U0001f407',
                             ':baby_symbol:': '\U0001f6bc',
                             ':place_of_worship:': '\U0001f6d0', ':sake:': '\U0001f376',
                             ':kissing_heart:': '\U0001f618',
                             ':middle_finger:': '\U0001f595',
                             ':dancers:': '\U0001f46f', ':minibus:': '\U0001f690', ':hugging_face:': '\U0001f917',
                             ':black_square_button:': '\U0001f532', ':hospital:': '\U0001f3e5',
                             ':golfer:': '\U0001f3cc',
                             ':volcano:': '\U0001f30b', ':pager:': '\U0001f4df',
                             ':house_with_garden:': '\U0001f3e1', ':blue_book:': '\U0001f4d8',
                             ':disappointed:': '\U0001f61e',
                             ':rice_scene:': '\U0001f391',
                             ':national_park:': '\U0001f3de', ':paperclip:': '\U0001f4ce', ':battery:': '\U0001f50b',
                             ':christmas_tree:': '\U0001f384',
                             ':sob:': '\U0001f62d',
                             ':hotel:': '\U0001f3e8',
                             ':spider_web:': '\U0001f578', ':closed_umbrella:': '\U0001f302',
                             ':doughnut:': '\U0001f369',
                             ':busts_in_silhouette:': '\U0001f465',
                             ':sparkling_heart:': '\U0001f496',
                             ':level_slider:': '\U0001f39a', ':icecream:': '\U0001f366', ':mahjong:': '\U0001f004',
                             ':flag-gi:': '\U0001f1ec-\U0001f1ee',
                             ':snow_cloud:': '\U0001f328', ':gem:': '\U0001f48e',
                             ':woman-woman-girl-boy:': '\U0001f469-200d-\U0001f469-200d-\U0001f467-200d-\U0001f466',
                             ':video_camera:': '\U0001f4f9', ':love_letter:': '\U0001f48c', ':weary:': '\U0001f629',
                             ':left_luggage:': '\U0001f6c5',
                             ':money_mouth_face:': '\U0001f911', ':hibiscus:': '\U0001f33a', ':apple:': '\U0001f34e',
                             ':face_with_rolling_eyes:': '\U0001f644',
                             ':on:': '\U0001f51b', ':racing_car:': '\U0001f3ce',
                             ':lock:': '\U0001f512',
                             ':koala:': '\U0001f428',
                             ':stuck_out_tongue_closed_eyes:': '\U0001f61d', ':gun:': '\U0001f52b',
                             ':sun_with_face:': '\U0001f31e',
                             ':man-woman-girl:': '\U0001f468-200d-\U0001f469-200d-\U0001f467', ':mega:': '\U0001f4e3',
                             ':no_entry_sign:': '\U0001f6ab',
                             ':u6e80:': '\U0001f235', ':waning_crescent_moon:': '\U0001f318',
                             ':vertical_traffic_light:': '\U0001f6a6', ':date:': '\U0001f4c5', ':womens:': '\U0001f6ba',
                             ':bug:': '\U0001f41b',
                             ':woman-woman-girl-girl:': '\U0001f469-200d-\U0001f469-200d-\U0001f467-200d-\U0001f467',
                             ':cherry_blossom:': '\U0001f338', ':fax:': '\U0001f4e0', ':heart_eyes_cat:': '\U0001f63b',
                             ':lower_left_fountain_pen:': '\U0001f58b',
                             ':ok_hand:': '\U0001f44c', ':taco:': '\U0001f32e', ':boar:': '\U0001f417',
                             ':carousel_horse:': '\U0001f3a0',
                             ':hammer:': '\U0001f528',
                             ':boy:': '\U0001f466', ':clock130:': '\U0001f55c',
                             ':speaker:': '\U0001f508',
                             ':point_left:': '\U0001f448',
                             ':abcd:': '\U0001f521', ':flag-gh:': '\U0001f1ec-\U0001f1ed', ':briefcase:': '\U0001f4bc',
                             ':u6708:': '\U0001f237',
                             ':clock930:': '\U0001f564',
                             ':clock230:': '\U0001f55d', ':bed:': '\U0001f6cf', ':barely_sunny:': '\U0001f325',
                             ':foggy:': '\U0001f301',
                             ':full_moon:': '\U0001f315',
                             ':soon:': '\U0001f51c',
                             ':mailbox_with_mail:': '\U0001f4ec',
                             ':woman-woman-girl:': '\U0001f469-200d-\U0001f469-200d-\U0001f467',
                             ':green_apple:': '\U0001f34f',
                             ':fried_shrimp:': '\U0001f364', ':b:': '\U0001f171',
                             ':man-man-boy-boy:': '\U0001f468-200d-\U0001f468-200d-\U0001f466-200d-\U0001f466',
                             ':credit_card:': '\U0001f4b3',
                             ':woman-woman-boy:': '\U0001f469-200d-\U0001f469-200d-\U0001f466',
                             ':flag-ie:': '\U0001f1ee-\U0001f1ea',
                             ':dress:': '\U0001f457', ':two_women_holding_hands:': '\U0001f46d',
                             ':u5272:': '\U0001f239',
                             ':calendar:': '\U0001f4c6', ':monkey:': '\U0001f412',
                             ':saxophone:': '\U0001f3b7',
                             ':mantelpiece_clock:': '\U0001f570', ':hamster:': '\U0001f439',
                             ':om_symbol:': '\U0001f549',
                             ':a:': '\U0001f170', ':clock7:': '\U0001f556',
                             ':name_badge:': '\U0001f4db',
                             ':haircut:': '\U0001f487',
                             ':moon:': '\U0001f314', ':poultry_leg:': '\U0001f357',
                             ':disappointed_relieved:': '\U0001f625',
                             ':non-potable_water:': '\U0001f6b1',
                             ':synagogue:': '\U0001f54d',
                             ':notebook:': '\U0001f4d3',
                             ':no_smoking:': '\U0001f6ad',
                             ':ox:': '\U0001f402', ':older_man:': '\U0001f474', ':whale2:': '\U0001f40b',
                             ':small_orange_diamond:': '\U0001f538',
                             ':beach_with_umbrella:': '\U0001f3d6', ':mask:': '\U0001f637',

                             ':skin-tone-5:': '\U0001f3fe', ':+1:': '\U0001f44d', ':purse:': '\U0001f45b',
                             ':joy:': '\U0001f602',
                             ':airplane_arriving:': '\U0001f6ec', ':rice_cracker:': '\U0001f358',
                             ':birthday:': '\U0001f382',
                             ':clock330:': '\U0001f55e',
                             ':mailbox_with_no_mail:': '\U0001f4ed', ':cake:': '\U0001f370',

                             ':airplane_departure:': '\U0001f6eb', ':flag-cd:': '\U0001f1e8-\U0001f1e9',
                             ':bomb:': '\U0001f4a3',
                             ':high_brightness:': '\U0001f506',

                             ':passenger_ship:': '\U0001f6f3',
                             ':linked_paperclips:': '\U0001f587',
                             ':prayer_beads:': '\U0001f4ff',
                             ':droplet:': '\U0001f4a7',
                             ':point_right:': '\U0001f449',
                             ':stadium:': '\U0001f3df',
                             ':performing_arts:': '\U0001f3ad',
                             ':eyes:': '\U0001f440', ':man-heart-man:': '\U0001f468-200d-2764-fe0f-200d-\U0001f468',
                             ':baby_chick:': '\U0001f424',
                             ':ship:': '\U0001f6a2', ':flag-cf:': '\U0001f1e8-\U0001f1eb',
                             ':8ball:': '\U0001f3b1',
                             ':bathtub:': '\U0001f6c1', ':flag-ag:': '\U0001f1e6-\U0001f1ec',
                             ':motor_boat:': '\U0001f6e5',
                             ':scream:': '\U0001f631',
                             ':ribbon:': '\U0001f380',
                             ':bouquet:': '\U0001f490',

                             ':convenience_store:': '\U0001f3ea', ':pray:': '\U0001f64f',
                             ':potable_water:': '\U0001f6b0',
                             ':jack_o_lantern:': '\U0001f383', ':penguin:': '\U0001f427',
                             ':crystal_ball:': '\U0001f52e',
                             ':tongue:': '\U0001f445', ':checkered_flag:': '\U0001f3c1',
                             ':thinking_face:': '\U0001f914',
                             ':person_frowning:': '\U0001f64d',
                             ':necktie:': '\U0001f454',
                             ':ledger:': '\U0001f4d2',
                             ':melon:': '\U0001f348',
                             ':bear:': '\U0001f43b',
                             ':train2:': '\U0001f686',
                             ':flag-ic:': '\U0001f1ee-\U0001f1e8', ':snail:': '\U0001f40c', ':beer:': '\U0001f37a',
                             ':strawberry:': '\U0001f353',
                             ':yum:': '\U0001f60b',
                             ':satellite:': '\U0001f6f0',
                             ':princess:': '\U0001f478',
                             ':compression:': '\U0001f5dc', ':arrows_clockwise:': '\U0001f503',
                             ':package:': '\U0001f4e6',
                             ':rain_cloud:': '\U0001f327',
                             ':scream_cat:': '\U0001f640',
                             ':hamburger:': '\U0001f354',
                             ':flag-gb:': '\U0001f1ec-\U0001f1e7',
                             ':smile:': '\U0001f604',
                             ':fire:': '\U0001f525', ':clock2:': '\U0001f551',
                             ':beers:': '\U0001f37b', ':rainbow:': '\U0001f308',
                             ':building_construction:': '\U0001f3d7',
                             ':gift:': '\U0001f381', ':u7121:': '\U0001f21a', ':postbox:': '\U0001f4ee',
                             ':crossed_flags:': '\U0001f38c', ':flag-ci:': '\U0001f1e8-\U0001f1ee',
                             ':pig:': '\U0001f437',
                             ':articulated_lorry:': '\U0001f69b',
                             ':sleeping_accommodation:': '\U0001f6cc', ':radio:': '\U0001f4fb',
                             ':full_moon_with_face:': '\U0001f31d',
                             ':facepunch:': '\U0001f44a',
                             ':flag-gg:': '\U0001f1ec-\U0001f1ec',
                             ':bullettrain_side:': '\U0001f684',
                             ':lower_left_paintbrush:': '\U0001f58c',
                             ':wink:': '\U0001f609',
                             ':heartpulse:': '\U0001f497', ':police_car:': '\U0001f693',
                             ':railway_track:': '\U0001f6e4',
                             ':ice_cream:': '\U0001f368', ':shopping_bags:': '\U0001f6cd', ':helicopter:': '\U0001f681',
                             ':clock4:': '\U0001f553',
                             ':u7a7a:': '\U0001f233', ':rosette:': '\U0001f3f5', ':taxi:': '\U0001f695',
                             ':flag-bj:': '\U0001f1e7-\U0001f1ef',
                             ':person_with_pouting_face:': '\U0001f64e', ':dart:': '\U0001f3af', ':feet:': '\U0001f43e',
                             ':flag-bi:': '\U0001f1e7-\U0001f1ee',
                             ':clock12:': '\U0001f55b', ':mailbox_closed:': '\U0001f4ea', ':cry:': '\U0001f622',
                             ':raised_hands:': '\U0001f64c',
                             ':bowling:': '\U0001f3b3', ':honey_pot:': '\U0001f36f', ':u55b6:': '\U0001f23a',
                             ':flag-eg:': '\U0001f1ea-\U0001f1ec',
                             ':man-woman-girl-boy:': '\U0001f468-200d-\U0001f469-200d-\U0001f467-200d-\U0001f466',
                             ':robot_face:': '\U0001f916', ':flag-ac:': '\U0001f1e6-\U0001f1e8',
                             ':tokyo_tower:': '\U0001f5fc',
                             ':wave:': '\U0001f44b',

                             ':relieved:': '\U0001f60c',
                             ':crab:': '\U0001f980', ':rat:': '\U0001f400',
                             ':point_down:': '\U0001f447',
                             ':wc:': '\U0001f6be',
                             ':green_heart:': '\U0001f49a',
                             ':grinning:': '\U0001f600',
                             ':bullettrain_front:': '\U0001f685',

                             ':earth_asia:': '\U0001f30f',
                             ':spaghetti:': '\U0001f35d',
                             ':link:': '\U0001f517',
                             ':kaaba:': '\U0001f54b',
                             ':mouse2:': '\U0001f401', ':football:': '\U0001f3c8', ':pineapple:': '\U0001f34d',
                             ':sunrise:': '\U0001f305',
                             ':chart:': '\U0001f4b9',
                             ':man_with_turban:': '\U0001f473', ':badminton_racquet_and_shuttlecock:': '\U0001f3f8',

                             ':burrito:': '\U0001f32f',
                             ':pushpin:': '\U0001f4cc',
                             ':flag-ad:': '\U0001f1e6-\U0001f1e9', ':revolving_hearts:': '\U0001f49e',
                             ':earth_americas:': '\U0001f30e',
                             ':crying_cat_face:': '\U0001f63f', ':loud_sound:': '\U0001f50a',
                             ':dog:': '\U0001f436',
                             ':racing_motorcycle:': '\U0001f3cd',
                             ':guitar:': '\U0001f3b8', ':abc:': '\U0001f524', ':shield:': '\U0001f6e1',
                             ':telephone_receiver:': '\U0001f4de', ':office:': '\U0001f3e2',
                             ':nut_and_bolt:': '\U0001f529',
                             ':children_crossing:': '\U0001f6b8', ':the_horns:': '\U0001f918',
                             ':izakaya_lantern:': '\U0001f3ee',
                             ':whale:': '\U0001f433',
                             ':spiral_note_pad:': '\U0001f5d2', ':purple_heart:': '\U0001f49c',
                             ':light_rail:': '\U0001f688',
                             ':beetle:': '\U0001f41e',
                             ':trumpet:': '\U0001f3ba',
                             ':medal:': '\U0001f396', ':suspension_railway:': '\U0001f69f', ':bar_chart:': '\U0001f4ca',

                             ':restroom:': '\U0001f6bb', ':game_die:': '\U0001f3b2',
                             ':right_anger_bubble:': '\U0001f5ef',
                             ':arrow_up_small:': '\U0001f53c',
                             ':couple_with_heart:': '\U0001f491', ':curry:': '\U0001f35b',
                             ':movie_camera:': '\U0001f3a5',
                             ':back:': '\U0001f519', ':leaves:': '\U0001f343',
                             ':tornado:': '\U0001f32a',
                             ':panda_face:': '\U0001f43c',

                             ':mans_shoe:': '\U0001f45e',
                             ':iphone:': '\U0001f4f1', ':flag-je:': '\U0001f1ef-\U0001f1ea',
                             ':dove_of_peace:': '\U0001f54a',
                             ':balloon:': '\U0001f388',
                             ':muscle:': '\U0001f4aa', ':popcorn:': '\U0001f37f',
                             ':two_men_holding_hands:': '\U0001f46c',
                             ':smiley:': '\U0001f603',
                             ':world_map:': '\U0001f5fa', ':surfer:': '\U0001f3c4', ':aerial_tramway:': '\U0001f6a1',
                             ':frog:': '\U0001f438',
                             ':point_up_2:': '\U0001f446', ':cricket_bat_and_ball:': '\U0001f3cf',
                             ':clock530:': '\U0001f560',

                             ':cityscape:': '\U0001f3d9', ':globe_with_meridians:': '\U0001f310',
                             ':banana:': '\U0001f34c',
                             ':red_circle:': '\U0001f534',
                             ':racehorse:': '\U0001f40e',
                             ':cow2:': '\U0001f404',
                             ':dark_sunglasses:': '\U0001f576', ':camping:': '\U0001f3d5', ':metro:': '\U0001f687',
                             ':admission_tickets:': '\U0001f39f',
                             ':clock10:': '\U0001f559', ':man_with_gua_pi_mao:': '\U0001f472', ':oden:': '\U0001f362',
                             ':1234:': '\U0001f522',
                             ':moneybag:': '\U0001f4b0', ':speech_balloon:': '\U0001f4ac',
                             ':bride_with_veil:': '\U0001f470',
                             ':construction:': '\U0001f6a7',
                             ':rowboat:': '\U0001f6a3', ':pound:': '\U0001f4b7',
                             ':flag-bb:': '\U0001f1e7-\U0001f1e7',
                             ':notes:': '\U0001f3b6', ':post_office:': '\U0001f3e3',
                             ':flag-bh:': '\U0001f1e7-\U0001f1ed',
                             ':sleepy:': '\U0001f62a', ':vibration_mode:': '\U0001f4f3', ':capital_abcd:': '\U0001f520',
                             ':stars:': '\U0001f320',
                             ':meat_on_bone:': '\U0001f356', ':sunflower:': '\U0001f33b', ':bow:': '\U0001f647',
                             ':dragon_face:': '\U0001f432',
                             ':octopus:': '\U0001f419',
                             ':cinema:': '\U0001f3a6',
                             ':crescent_moon:': '\U0001f319', ':dancer:': '\U0001f483', ':womans_hat:': '\U0001f452',
                             ':yellow_heart:': '\U0001f49b',
                             ':angel:': '\U0001f47c', ':clock1130:': '\U0001f566', ':flashlight:': '\U0001f526',
                             ':desert_island:': '\U0001f3dd', ':grapes:': '\U0001f347', ':electric_plug:': '\U0001f50c',
                             ':stuck_out_tongue_winking_eye:': '\U0001f61c', ':hotdog:': '\U0001f32d',
                             ':tiger2:': '\U0001f405',
                             ':hatched_chick:': '\U0001f425',
                             ':woman:': '\U0001f469', ':sandal:': '\U0001f461', ':flag-af:': '\U0001f1e6-\U0001f1eb',
                             ':mountain_railway:': '\U0001f69e',
                             ':flag-ee:': '\U0001f1ea-\U0001f1ea',
                             ':bird:': '\U0001f426',
                             ':u7981:': '\U0001f232', ':desert:': '\U0001f3dc',
                             ':-1:': '\U0001f44e', ':tophat:': '\U0001f3a9',
                             ':rabbit:': '\U0001f430',
                             ':scroll:': '\U0001f4dc',
                             ':flag-de:': '\U0001f1e9-\U0001f1ea', ':palm_tree:': '\U0001f334',
                             ':pensive:': '\U0001f614',
                             ':dromedary_camel:': '\U0001f42a',
                             ':bee:': '\U0001f41d', ':maple_leaf:': '\U0001f341',
                             ':rooster:': '\U0001f413',
                             ':card_index:': '\U0001f4c7', ':card_file_box:': '\U0001f5c3',
                             ':twisted_rightwards_arrows:': '\U0001f500',
                             ':oncoming_automobile:': '\U0001f698', ':trackball:': '\U0001f5b2',
                             ':man-woman-boy-boy:': '\U0001f468-200d-\U0001f469-200d-\U0001f466-200d-\U0001f466',
                             ':joy_cat:': '\U0001f639',
                             ':floppy_disk:': '\U0001f4be',
                             ':athletic_shoe:': '\U0001f45f', ':rotating_light:': '\U0001f6a8',
                             ':information_desk_person:': '\U0001f481',
                             ':tv:': '\U0001f4fa',
                             ':smile_cat:': '\U0001f638',
                             ':tomato:': '\U0001f345',
                             ':small_blue_diamond:': '\U0001f539', ':open_hands:': '\U0001f450',
                             ':rage:': '\U0001f621',
                             ':monkey_face:': '\U0001f435', ':school:': '\U0001f3eb', ':hear_no_evil:': '\U0001f649',
                             ':mountain_cableway:': '\U0001f6a0',
                             ':film_frames:': '\U0001f39e', ':tada:': '\U0001f389', ':sound:': '\U0001f509',
                             ':oncoming_bus:': '\U0001f68d',
                             ':speedboat:': '\U0001f6a4',
                             ':family:': '\U0001f46a', ':radio_button:': '\U0001f518', ':flags:': '\U0001f38f',
                             ':flag-eh:': '\U0001f1ea-\U0001f1ed',
                             ':flag-bd:': '\U0001f1e7-\U0001f1e9', ':man_in_business_suit_levitating:': '\U0001f574',
                             ':flag-gf:': '\U0001f1ec-\U0001f1eb',
                             ':woman-heart-woman:': '\U0001f469-200d-2764-fe0f-200d-\U0001f469',
                             ':last_quarter_moon_with_face:': '\U0001f31c', ':unicorn_face:': '\U0001f984',
                             ':flower_playing_cards:': '\U0001f3b4',
                             ':cheese_wedge:': '\U0001f9c0', ':repeat:': '\U0001f501', ':open_mouth:': '\U0001f62e',
                             ':nose:': '\U0001f443',
                             ':lipstick:': '\U0001f484',
                             ':ring:': '\U0001f48d', ':laughing:': '\U0001f606',
                             ':baggage_claim:': '\U0001f6c4',
                             ':tractor:': '\U0001f69c',
                             ':pouting_cat:': '\U0001f63e', ':shell:': '\U0001f41a', ':watermelon:': '\U0001f349',
                             ':new_moon:': '\U0001f311',
                             ':fireworks:': '\U0001f386',
                             ':man-man-girl:': '\U0001f468-200d-\U0001f468-200d-\U0001f467',
                             ':train:': '\U0001f68b',
                             ':camel:': '\U0001f42b', ':page_with_curl:': '\U0001f4c3',
                             ':flag-ga:': '\U0001f1ec-\U0001f1e6',
                             ':frame_with_picture:': '\U0001f5bc', ':camera_with_flash:': '\U0001f4f8',
                             ':blue_heart:': '\U0001f499',
                             ':school_satchel:': '\U0001f392', ':water_buffalo:': '\U0001f403', ':house:': '\U0001f3e0',
                             ':japanese_ogre:': '\U0001f479',
                             ':girl:': '\U0001f467', ':oncoming_police_car:': '\U0001f694',
                             ':shirt:': '\U0001f455',
                             ':signal_strength:': '\U0001f4f6', ':hammer_and_wrench:': '\U0001f6e0',
                             ':alien:': '\U0001f47d',
                             ':syringe:': '\U0001f489',
                             ':o2:': '\U0001f17e', ':pizza:': '\U0001f355',
                             ':bellhop_bell:': '\U0001f6ce',
                             ':broken_heart:': '\U0001f494', ':couplekiss:': '\U0001f48f',
                             ':flag-fj:': '\U0001f1eb-\U0001f1ef',
                             ':video_game:': '\U0001f3ae',
                             ':japanese_goblin:': '\U0001f47a', ':blowfish:': '\U0001f421',
                             ':smiley_cat:': '\U0001f63a',
                             ':camera:': '\U0001f4f7',
                             ':skin-tone-2:': '\U0001f3fb', ':eye:': '\U0001f441', ':roller_coaster:': '\U0001f3a2',
                             ':flag-dg:': '\U0001f1e9-\U0001f1ec', ':persevere:': '\U0001f623',
                             ':fearful:': '\U0001f628',
                             ':tired_face:': '\U0001f62b',
                             ':dolls:': '\U0001f38e',
                             ':volleyball:': '\U0001f3d0',
                             ':accept:': '\U0001f251', ':kissing_cat:': '\U0001f63d', ':u6307:': '\U0001f22f',
                             ':postal_horn:': '\U0001f4ef', ':bridge_at_night:': '\U0001f309',
                             ':fish_cake:': '\U0001f365',
                             ':flag-ai:': '\U0001f1e6-\U0001f1ee',
                             ':bicyclist:': '\U0001f6b4', ':ok:': '\U0001f197', ':city_sunset:': '\U0001f306',
                             ':statue_of_liberty:': '\U0001f5fd',
                             ':japanese_castle:': '\U0001f3ef', ':file_folder:': '\U0001f4c1',
                             ':european_post_office:': '\U0001f3e4', ':sweat:': '\U0001f613', ':cyclone:': '\U0001f300',
                             ':hushed:': '\U0001f62f',
                             ':angry:': '\U0001f620', ':joystick:': '\U0001f579',
                             ':last_quarter_moon:': '\U0001f317', ':passport_control:': '\U0001f6c2',
                             ':barber:': '\U0001f488',
                             ':file_cabinet:': '\U0001f5c4',
                             ':diamond_shape_with_a_dot_inside:': '\U0001f4a0',
                             ':bus:': '\U0001f68c', ':clipboard:': '\U0001f4cb',
                             ':ballot_box_with_ballot:': '\U0001f5f3',
                             ':face_with_thermometer:': '\U0001f912', ':wine_glass:': '\U0001f377',
                             ':id:': '\U0001f194',
                             ':night_with_stars:': '\U0001f303',
                             ':speak_no_evil:': '\U0001f64a', ':ski:': '\U0001f3bf',
                             ':microscope:': '\U0001f52c',
                             ':first_quarter_moon_with_face:': '\U0001f31b', ':flag-ae:': '\U0001f1e6-\U0001f1ea',
                             ':clock11:': '\U0001f55a',
                             ':dolphin:': '\U0001f42c',
                             ':wrench:': '\U0001f527', ':wastebasket:': '\U0001f5d1',
                             ':evergreen_tree:': '\U0001f332',
                             ':beginner:': '\U0001f530', ':cupid:': '\U0001f498',
                             ':raised_hand_with_fingers_splayed:': '\U0001f590',
                             ':house_buildings:': '\U0001f3d8', ':minidisc:': '\U0001f4bd', ':vhs:': '\U0001f4fc',
                             ':flag-ca:': '\U0001f1e8-\U0001f1e6',
                             ':memo:': '\U0001f4dd', ':unamused:': '\U0001f612',
                             ':arrows_counterclockwise:': '\U0001f504',
                             ':bust_in_silhouette:': '\U0001f464',
                             ':rose:': '\U0001f339', ':bread:': '\U0001f35e',
                             ':end:': '\U0001f51a',
                             ':tiger:': '\U0001f42f',
                             ':envelope_with_arrow:': '\U0001f4e9', ':flushed:': '\U0001f633',
                             ':no_mobile_phones:': '\U0001f4f5',
                             ':card_index_dividers:': '\U0001f5c2', ':shaved_ice:': '\U0001f367',
                             ':lollipop:': '\U0001f36d',
                             ':hot_pepper:': '\U0001f336',
                             ':spider:': '\U0001f577', ':satellite_antenna:': '\U0001f4e1', ':crocodile:': '\U0001f40a',
                             ':mountain_bicyclist:': '\U0001f6b5',
                             ':large_orange_diamond:': '\U0001f536', ':repeat_one:': '\U0001f502',
                             ':weight_lifter:': '\U0001f3cb',
                             ':motorway:': '\U0001f6e3', ':do_not_litter:': '\U0001f6af',
                             ':rice_ball:': '\U0001f359', ':book:': '\U0001f4d6', ':european_castle:': '\U0001f3f0',
                             ':woman-woman-boy-boy:': '\U0001f469-200d-\U0001f469-200d-\U0001f466-200d-\U0001f466',
                             ':toilet:': '\U0001f6bd',
                             ':bulb:': '\U0001f4a1', ':wedding:': '\U0001f492',
                             ':swimmer:': '\U0001f3ca',
                             ':custard:': '\U0001f36e',
                             ':construction_worker:': '\U0001f477', ':loudspeaker:': '\U0001f4e2',
                             ':flag-gd:': '\U0001f1ec-\U0001f1e9',
                             ':sweet_potato:': '\U0001f360',
                             ':thought_balloon:': '\U0001f4ad', ':runner:': '\U0001f3c3', ':guardsman:': '\U0001f482',
                             ':tea:': '\U0001f375',
                             ':womans_clothes:': '\U0001f45a', ':mobile_phone_off:': '\U0001f4f4',
                             ':sos:': '\U0001f198',
                             ':tennis:': '\U0001f3be',
                             ':baby:': '\U0001f476', ':bike:': '\U0001f6b2', ':leopard:': '\U0001f406',
                             ':ghost:': '\U0001f47b',
                             ':studio_microphone:': '\U0001f399',
                             ':seat:': '\U0001f4ba',
                             ':cop:': '\U0001f46e', ':vs:': '\U0001f19a',
                             ':calling:': '\U0001f4f2',
                             ':small_red_triangle:': '\U0001f53a',
                             ':desktop_computer:': '\U0001f5a5',
                             ':expressionless:': '\U0001f611', ':dango:': '\U0001f361', ':ear_of_rice:': '\U0001f33e',
                             ':pill:': '\U0001f48a',
                             ':santa:': '\U0001f385',
                             ':busstop:': '\U0001f68f', ':smirk:': '\U0001f60f', ':corn:': '\U0001f33d',
                             ':candy:': '\U0001f36c',
                             ':anguished:': '\U0001f627', ':sparkler:': '\U0001f387',
                             ':lock_with_ink_pen:': '\U0001f50f',
                             ':dash:': '\U0001f4a8',
                             ':heartbeat:': '\U0001f493', ':bow_and_arrow:': '\U0001f3f9',
                             ':cold_sweat:': '\U0001f630',
                             ':smirk_cat:': '\U0001f63c', ':no_bicycles:': '\U0001f6b3',
                             ':heart_decoration:': '\U0001f49f',
                             ':ambulance:': '\U0001f691',
                             ':three_button_mouse:': '\U0001f5b1', ':printer:': '\U0001f5a8',
                             ':skin-tone-6:': '\U0001f3ff',
                             ':crown:': '\U0001f451',
                             ':unlock:': '\U0001f513', ':pouch:': '\U0001f45d',
                             ':sunrise_over_mountains:': '\U0001f304',
                             ':musical_note:': '\U0001f3b5',
                             ':flag-cc:': '\U0001f1e8-\U0001f1e8', ':mouse:': '\U0001f42d',
                             ':clock9:': '\U0001f558',
                             ':knife_fork_plate:': '\U0001f37d', ':triangular_ruler:': '\U0001f4d0',
                             ':no_mouth:': '\U0001f636',
                             ':snowboarder:': '\U0001f3c2',
                             ':table_tennis_paddle_and_ball:': '\U0001f3d3',
                             ':flag-id:': '\U0001f1ee-\U0001f1e9', ':horse_racing:': '\U0001f3c7',
                             ':chestnut:': '\U0001f330',
                             ':waving_white_flag:': '\U0001f3f3',
                             ':chart_with_upwards_trend:': '\U0001f4c8',
                             ':hankey:': '\U0001f4a9',
                             ':woman-kiss-woman:': '\U0001f469-200d-2764-fe0f-200d-\U0001f48b-200d-\U0001f469',
                             ':snake:': '\U0001f40d',
                             ':control_knobs:': '\U0001f39b',
                             ':confounded:': '\U0001f616',
                             ':monorail:': '\U0001f69d',
                             ':boot:': '\U0001f462',
                             ':sweat_drops:': '\U0001f4a6', ':mushroom:': '\U0001f344', ':ferris_wheel:': '\U0001f3a1',
                             ':green_book:': '\U0001f4d7',
                             ':lower_left_ballpoint_pen:': '\U0001f58a', ':astonished:': '\U0001f632',
                             ':trophy:': '\U0001f3c6',
                             ':flag-cg:': '\U0001f1e8-\U0001f1ec',
                             ':rolled_up_newspaper:': '\U0001f5de', ':pear:': '\U0001f350',
                             ':flag-bg:': '\U0001f1e7-\U0001f1ec',
                             ':skull:': '\U0001f480',
                             ':white_square_button:': '\U0001f533', ':japan:': '\U0001f5fe',
                             ':chocolate_bar:': '\U0001f36b',
                             ':oil_drum:': '\U0001f6e2', ':herb:': '\U0001f33f',
                             ':open_file_folder:': '\U0001f4c2', ':kissing:': '\U0001f617', ':sleeping:': '\U0001f634',
                             ':basketball:': '\U0001f3c0',
                             ':euro:': '\U0001f4b6', ':cat:': '\U0001f431', ':mag:': '\U0001f50d',
                             ':menorah_with_nine_branches:': '\U0001f54e',
                             ':parking:': '\U0001f17f', ':lightning:': '\U0001f329', ':stew:': '\U0001f372',
                             ':headphones:': '\U0001f3a7',
                             ':flag-bf:': '\U0001f1e7-\U0001f1eb', ':factory:': '\U0001f3ed',
                             ':outbox_tray:': '\U0001f4e4',
                             ':mute:': '\U0001f507', ':ok_woman:': '\U0001f646',
                             ':mortar_board:': '\U0001f393',
                             ':100:': '\U0001f4af',
                             ':clock630:': '\U0001f561', ':massage:': '\U0001f486', ':cat2:': '\U0001f408',
                             ':newspaper:': '\U0001f4f0',
                             ':cactus:': '\U0001f335',
                             ':station:': '\U0001f689', ':circus_tent:': '\U0001f3aa', ':small_airplane:': '\U0001f6e9',
                             ':waxing_crescent_moon:': '\U0001f312',
                             ':man-woman-girl-girl:': '\U0001f468-200d-\U0001f469-200d-\U0001f467-200d-\U0001f467',
                             ':face_with_head_bandage:': '\U0001f915',
                             ':clock5:': '\U0001f554',
                             ':man-man-girl-girl:': '\U0001f468-200d-\U0001f468-200d-\U0001f467-200d-\U0001f467',
                             ':blue_car:': '\U0001f699', ':small_red_triangle_down:': '\U0001f53b',
                             ':mailbox:': '\U0001f4eb',
                             ':flag-be:': '\U0001f1e7-\U0001f1ea',
                             ':u7533:': '\U0001f238',
                             ':confetti_ball:': '\U0001f38a',
                             ':u5408:': '\U0001f234', ':fork_and_knife:': '\U0001f374', ':clock730:': '\U0001f562',
                             ':dragon:': '\U0001f409',
                             ':zipper_mouth_face:': '\U0001f910', ':spiral_calendar_pad:': '\U0001f5d3',
                             ':sleuth_or_spy:': '\U0001f575',
                             ':confused:': '\U0001f615', ':mount_fuji:': '\U0001f5fb',
                             ':speaking_head_in_silhouette:': '\U0001f5e3',
                             ':clock1:': '\U0001f550',
                             ':cow:': '\U0001f42e', ':clock1030:': '\U0001f565',
                             ':ramen:': '\U0001f35c',
                             ':elephant:': '\U0001f418',
                             ':upside_down_face:': '\U0001f643',
                             ':man-kiss-man:': '\U0001f468-200d-2764-fe0f-200d-\U0001f48b-200d-\U0001f468',
                             ':smoking:': '\U0001f6ac', ':rice:': '\U0001f35a',
                             ':mostly_sunny:': '\U0001f324',
                             ':wind_blowing_face:': '\U0001f32c', ':champagne:': '\U0001f37e',
                             ':six_pointed_star:': '\U0001f52f',
                             ':atm:': '\U0001f3e7',
                             ':dagger_knife:': '\U0001f5e1', ':candle:': '\U0001f56f', ':city_sunrise:': '\U0001f307',
                             ':bath:': '\U0001f6c0',
                             ':derelict_house_building:': '\U0001f3da', ':art:': '\U0001f3a8',
                             ':innocent:': '\U0001f607',
                             ':traffic_light:': '\U0001f6a5', ':first_quarter_moon:': '\U0001f313',
                             ':bookmark_tabs:': '\U0001f4d1',
                             ':dizzy:': '\U0001f4ab',
                             ':wolf:': '\U0001f43a',
                             ':reminder_ribbon:': '\U0001f397', ':dizzy_face:': '\U0001f635',
                             ':nail_care:': '\U0001f485',
                             ':lion_face:': '\U0001f981', ':fishing_pole_and_fish:': '\U0001f3a3',
                             ':kimono:': '\U0001f458',
                             ':raising_hand:': '\U0001f64b',
                             ':new:': '\U0001f195',
                             ':blush:': '\U0001f60a',
                             ':slightly_frowning_face:': '\U0001f641', ':frowning:': '\U0001f626',
                             ':flag-ba:': '\U0001f1e7-\U0001f1e6',
                             ':clock830:': '\U0001f563',
                             ':ocean:': '\U0001f30a', ':walking:': '\U0001f6b6', ':moyai:': '\U0001f5ff',
                             ':four_leaf_clover:': '\U0001f340',
                             ':straight_ruler:': '\U0001f4cf', ':space_invader:': '\U0001f47e',
                             ':symbols:': '\U0001f523',
                             ':ear:': '\U0001f442', ':low_brightness:': '\U0001f505', ':poodle:': '\U0001f429',
                             ':partly_sunny_rain:': '\U0001f326',
                             ':old_key:': '\U0001f5dd', ':incoming_envelope:': '\U0001f4e8',
                             ':mens:': '\U0001f6b9',
                             ':love_hotel:': '\U0001f3e9', ':peach:': '\U0001f351',
                             ':currency_exchange:': '\U0001f4b1',
                             ':sa:': '\U0001f202', ':flag-ec:': '\U0001f1ea-\U0001f1e8', ':ab:': '\U0001f18e',
                             ':flag-fi:': '\U0001f1eb-\U0001f1ee',
                             ':cd:': '\U0001f4bf',
                             ':tropical_drink:': '\U0001f379',
                             ':triangular_flag_on_post:': '\U0001f6a9',
                             ':trident:': '\U0001f531', ':flag-ea:': '\U0001f1ea-\U0001f1e6',
                             ':older_woman:': '\U0001f475',
                             ':earth_africa:': '\U0001f30d',
                             ':rocket:': '\U0001f680',
                             ':railway_car:': '\U0001f683',
                             ':baby_bottle:': '\U0001f37c',
                             ':sushi:': '\U0001f363',
                             ':boom:': '\U0001f4a5', ':truck:': '\U0001f69a',
                             ':amphora:': '\U0001f3fa', ':pig_nose:': '\U0001f43d', ':top:': '\U0001f51d',
                             ':underage:': '\U0001f51e',
                             ':triumph:': '\U0001f624', ':snow_capped_mountain:': '\U0001f3d4',
                             ':clock430:': '\U0001f55f',
                             ':skin-tone-3:': '\U0001f3fc', ':clap:': '\U0001f44f', ':eyeglasses:': '\U0001f453',
                             ':large_blue_circle:': '\U0001f535', ':ice_hockey_stick_and_puck:': '\U0001f3d2',
                             ':no_bell:': '\U0001f515', ':fog:': '\U0001f32b',
                             ':waning_gibbous_moon:': '\U0001f316',
                             ':person_with_blond_hair:': '\U0001f471', ':new_moon_with_face:': '\U0001f31a',
                             ':scorpion:': '\U0001f982',
                             ':mosque:': '\U0001f54c',
                             ':inbox_tray:': '\U0001f4e5', ':flag-ge:': '\U0001f1ec-\U0001f1ea',
                             ':pig2:': '\U0001f416',
                             ':spock-hand:': '\U0001f596',
                             ':man-man-boy:': '\U0001f468-200d-\U0001f468-200d-\U0001f466',
                             ':tangerine:': '\U0001f34a',
                             ':egg:': '\U0001f373',
                             ':tropical_fish:': '\U0001f420',
                             ':bookmark:': '\U0001f516',
                             ':telescope:': '\U0001f52d',
                             ':musical_keyboard:': '\U0001f3b9', ':up:': '\U0001f199',
                             ':steam_locomotive:': '\U0001f682',
                             ':violin:': '\U0001f3bb',
                             ':bamboo:': '\U0001f38d',
                             ':lower_left_crayon:': '\U0001f58d',
                             ':tanabata_tree:': '\U0001f38b', ':imp:': '\U0001f47f', ':grimacing:': '\U0001f62c'}
    extra_emoji_mapping = emoji.EMOJI_ALIAS_UNICODE
    extra_emoji_mapping.update(extra_emoji_mapping_1)
    extra_emoji_mapping.update(extra_emoji_mapping_2)
    emoji_keys = extra_emoji_mapping.keys()

    for key in emoji_keys:
        if key in message:
            message = message.replace(key, emoji.EMOJI_ALIAS_UNICODE[key])

    return message


def current_time(timezone=None):
    """ get current time """
    now = arrow.utcnow()
    if timezone is None:
        return now
    return now.to(timezone)


def get_timezone_offset_of_from_Beijing_to_EST():
    EST_now = datetime.datetime.now(pytz.timezone('America/New_York'))
    timezone_offset_of_from_Beijing_to_EST = abs(EST_now.utcoffset().total_seconds() / 60 / 60) + 8
    return int(timezone_offset_of_from_Beijing_to_EST)


def row_to_dict(row):
    result = {}
    for column in row.__table__.columns:
        result[column.name] = getattr(row, column.name)

    return result


def get_day_range_of_month(now):
    current_year = now.year
    current_month = now.month
    previous_month = current_month - 1
    previous_year = current_year
    _, last_day_of_current_month = calendar.monthrange(current_year, current_month)

    if current_month == 1:
        previous_year = current_year - 1
        previous_month = 12

    _, last_day_of_current_month = calendar.monthrange(current_year, current_month)
    _, last_day_of_previous_month = calendar.monthrange(previous_year, previous_month)

    first_day_of_current_month = str(current_year) + '-' + str(current_month) + '-' + '01'
    last_day_of_current_month = str(current_year) + '-' + str(current_month) + '-' + str(last_day_of_current_month)
    first_day_of_previous_month = str(previous_year) + '-' + str(previous_month) + '-' + '01'
    last_day_of_previous_month = str(previous_year) + '-' + str(previous_month) + '-' + str(last_day_of_previous_month)

    first_day_of_current_month = arrow.Arrow.strptime(first_day_of_current_month, '%Y-%m-%d').format('YYYY-MM-DD')
    last_day_of_current_month = arrow.Arrow.strptime(last_day_of_current_month, '%Y-%m-%d').format('YYYY-MM-DD')

    first_day_of_previous_month = arrow.Arrow.strptime(first_day_of_previous_month, '%Y-%m-%d').format('YYYY-MM-DD')
    last_day_of_previous_month = arrow.Arrow.strptime(last_day_of_previous_month, '%Y-%m-%d').format('YYYY-MM-DD')

    return first_day_of_current_month, last_day_of_current_month, first_day_of_previous_month, last_day_of_previous_month


def generate_sql_date(target):
    timezone_offset = app.config['APP_TIMEZONE']
    now = current_time(timezone_offset)
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')
    today = now.format('YYYY-MM-DD')

    day = {'today': today, 'yesterday': yesterday}
    someday = day.get(target, target)

    if target == 'lifetime':
        someday = None
        index_date = None
        sql_date = today, someday, index_date, timezone_offset
        return sql_date

    elif target in ['today', 'yesterday']:
        index_date = now.replace(days=-3).format('YYYY-MM-DD')
        sql_date = today, someday, index_date, timezone_offset
        return sql_date

    else:
        target_date = arrow.Arrow.strptime(target, '%Y-%m-%d', app.config['APP_TIMEZONE'])
        index_date = target_date.replace(days=-3).format('YYYY-MM-DD')
        sql_date = today, someday, index_date, timezone_offset
        return sql_date


def current_time_as_float(timezone=None):
    return current_time(timezone).float_timestamp * 1000


def get_last_day_of_prev_month(timezone=None):
    prev_month = current_time(timezone).replace(months=-1)
    return arrow.get(prev_month.year, prev_month.month, calendar.monthrange(prev_month.year, prev_month.month)[1])


class timeout(object):
    """
    To be used in a ``with`` block and timeout its content.
    """

    def __init__(self, callback_func, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
        self.callback_func = callback_func

    def handle_timeout(self, signum, frame):
        logging.error("Process timed out")

        self.callback_func()
        raise TimeoutException(self.error_message)

    def __enter__(self):
        try:
            signal.signal(signal.SIGALRM, self.handle_timeout)
            signal.alarm(self.seconds)
        except ValueError as e:
            logging.warning("timeout can't be used in the current context")
            logging.exception(e)

    def __exit__(self, type, value, traceback):
        try:
            signal.alarm(0)
        except ValueError as e:
            logging.warning("timeout can't be used in the current context")
            logging.exception(e)


def error_msg_from_exception(e):
    """Translate exception into error message

    Database have different ways to handle exception. This function attempts
    to make sense of the exception object and construct a human readable
    sentence.

    TODO(bkyryliuk): parse the Presto error message from the connection
                     created via create_engine.
    engine = create_engine('presto://localhost:3506/silver') -
      gives an e.message as the str(dict)
    presto.connect("localhost", port=3506, catalog='silver') - as a dict.
    The latter version is parsed correctly by this function.
    """
    msg = ''
    if hasattr(e, 'message'):
        if type(e.message) is dict:
            msg = e.message.get('message')
        elif e.message:
            msg = "{}".format(e.message)
    return msg or '{}'.format(e)


def dedup(l, suffix='__'):
    """De-duplicates a list of string by suffixing a counter

    Always returns the same number of entries as provided, and always returns
    unique values.

    # >>> dedup(['foo', 'bar', 'bar', 'bar'])
    ['foo', 'bar', 'bar__1', 'bar__2']
    """
    new_l = []
    seen = {}
    for s in l:
        if s in seen:
            seen[s] += 1
            s += suffix + str(seen[s])
        else:
            seen[s] = 0
        new_l.append(s)
    return new_l


def str_to_class(module_name, class_name):
    mdl = importlib.import_module(module_name)
    kls = getattr(mdl, class_name)
    return kls
