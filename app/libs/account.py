from itsdangerous import URLSafeTimedSerializer
from itsdangerous import Signer

from  app.tasks.mail import send_mail

serializer = URLSafeTimedSerializer('xwerwlrwrwerkwekdldl3kwekr')


def invite_token(user):
    return serializer.dumps(str(user.id))


def reset_link_for_user(user):
    token = invite_token(user)
    invite_url = "{}/reset/{}".format('/', token)

    return invite_url


def validate_token(token):
    max_token_age = 60 * 60 * 1
    return serializer.loads(token, max_age=max_token_age)


def send_password_reset_email(user):
    reset_link = reset_link_for_user(user)
    context = dict(user_name=user.name, reset_link=reset_link)
    subject = "Reset your password"
    send_mail.delay(user.email, subject, 'reset', attachment=None, attachment_content_type=None, **context)

    return reset_link
