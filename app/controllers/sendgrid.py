import datetime
import json

from flask import Blueprint
from flask import request, abort, jsonify

from app import db
from app.models.promotion import EmailEvent

sendgrid = Blueprint('sendgrid', __name__)


@sendgrid.route('/sendgrid/event', methods=["POST"])
def email_event():
    if request.method == 'POST':

        rows = json.loads(request.data.decode())
        column_names = set(EmailEvent.__table__.columns.keys())

        # 清洗数据

        for row in rows:
            row['timestamp'] = datetime.datetime.utcfromtimestamp(int(row['timestamp']))
            if 'smtp-id' in row:
                row['smtp_id'] = json.dumps(row['smtp-id'])
                del row['smtp-id']
            if 'type' in row:
                row['types'] = json.dumps(row['type'])
                del row['type']

            if 'send_time' in row:
                try:
                    row['send_time'] = datetime.datetime.strptime(row['send_time'], '%Y/%m/%d %H:%M:%S')

                except ValueError:

                    try:
                        row['send_time'] = datetime.datetime.strptime(row['send_time'], '%m/%d/%Y %I:%M:%S %p')

                    except ValueError:

                        row['send_time'] = datetime.datetime.strptime(row['send_time'], '%Y-%m-%d %H:%M:%S')

            if 'url_offset' in row:
                row['url_offset'] = json.dumps(row['url_offset'])

        # 如果有新列，保存下来，然后定期查看，更新对应的表

        event_keys = set()
        for row in rows:
            keys = list(row.keys())
            for key in keys:
                event_keys.add(key)
        new_column_name = event_keys - column_names
        if new_column_name:
            with open('./email_event_column.txt', 'a') as f:

                for i in new_column_name:
                    f.write(str(i) + '\n')

            return jsonify(message='error'), 500

        else:

            #  保存数据
            for row in rows:
                db.session.execute(EmailEvent.__table__.insert(), row)
            db.session.commit()
            return jsonify(message='ok'), 200

    else:
        abort(400)
