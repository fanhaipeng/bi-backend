import arrow
from arrow import Arrow
from flask import Blueprint, render_template, request, jsonify
from flask import current_app as app
from flask_login import login_required
from sqlalchemy import text

from app.extensions import db
from app.models.main import permission_required
from app.utils import current_time, generate_sql_date, get_timezone_offset_of_from_Beijing_to_EST, \
    error_msg_from_exception

dashboard = Blueprint('dashboard', __name__)


@dashboard.route("/", methods=["GET"])
@login_required
@permission_required('manager')
def index():
    return render_template('dashboard/index.html')


@dashboard.route("/dashboard/visualization/summary_data", methods=["GET"])
@login_required
@permission_required('manager')
def visualization_summary_data():
    now = current_time(app.config['APP_TIMEZONE'])
    timezone_offset = app.config['APP_TIMEZONE']

    if request.args.get('day') and request.args.get('day') == 'yday':
        day = now.replace(days=-1).format('YYYY-MM-DD')
    else:
        day = now.format('YYYY-MM-DD')

    new_reg = db.engine.execute(text("""
                                      SELECT new_reg
                                      FROM   bi_statistic
                                      WHERE  platform = 'All Platform'
                                             AND    game = 'All Game'
                                             AND    on_day = :day
                                      """), day=day).scalar()

    revenue = db.engine.execute(text("""
                                     SELECT revenue
                                     FROM   bi_statistic
                                     WHERE  platform = 'All Platform'
                                            AND    game = 'All Game'
                                            AND    on_day = :day
                                     """), day=day, timezone_offset=timezone_offset).scalar()

    game_dau = db.engine.execute(text("""
                                      SELECT dau
                                      FROM   bi_statistic
                                      WHERE  game ='All Game'
                                             AND    platform = 'All Platform'
                                             AND    on_day = :day
                                      """), day=day).scalar()

    new_reg_game_dau = db.engine.execute(text("""
                                               SELECT new_reg_game_dau
                                               FROM   bi_statistic
                                               WHERE  on_day = :day
                                                      AND platform = 'All Platform'
                                                      AND game = 'All Game'
                                               """), day=day).scalar()

    payload = {
        'new_reg': new_reg,
        'revenue': revenue or 0,
        'game_dau': game_dau,
        'new_reg_game_dau': new_reg_game_dau
    }

    return jsonify(payload)


@dashboard.route("/dashboard/visualization/executive_data", methods=["GET"])
@login_required
@permission_required('manager')
def visualization_executive_data():
    days_ago = request.args.get('days_ago')
    timezone_offset = app.config['APP_TIMEZONE']

    if days_ago is None:
        start_time, end_time = request.args.get('date_range').split('  -  ')
    else:
        now = current_time(app.config['APP_TIMEZONE'])
        end_time = now.replace(days=-1).format('YYYY-MM-DD')
        start_time = now.replace(days=-int(days_ago)).format('YYYY-MM-DD')

    game = request.args.get('game')
    platform = request.args.get('platform')
    report_type = request.args.get('report_type')
    proxy = []

    if report_type == 'New Registration':

        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              new_reg
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                       """), start_time=start_time, end_time=end_time, game=game,
                                  platform=platform)

    elif report_type == 'Game DAU':

        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              dau
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                       """), start_time=start_time, end_time=end_time, game=game,
                                  platform=platform)

    elif report_type == 'WAU':

        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              wau
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                       """), start_time=start_time, end_time=end_time, game=game,
                                  platform=platform)

    elif report_type == 'MAU':

        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              mau
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                        """), start_time=start_time, end_time=end_time, game=game,
                                  platform=platform)

    elif report_type == 'New Reg Game DAU':

        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              new_reg_game_dau
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                        """), start_time=start_time, end_time=end_time, game=game,
                                  platform=platform)

    elif report_type == 'Revenue':
        proxy = db.engine.execute(text("""
                                       SELECT DATE(on_day),
                                              revenue
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND platform = :platform
                                              AND game = 'All Game'
                                       """), start_time=start_time, end_time=end_time, timezone_offset=timezone_offset,
                                  platform=platform)

    labels = []
    data = []
    for row in proxy:
        labels.append(arrow.get(row[0]).format('YYYY-MM-DD'))
        data.append(row[1])
    return jsonify(labels=labels, data=data, game=game, platform=platform)


@dashboard.route("/dashboard/get_today_ccu")
@login_required
@permission_required('manager')
def get_today_ccu():
    today = current_time(app.config['APP_TIMEZONE']).format('YYYY-MM-DD')
    ccu_today_sql = """
            SELECT
                 all_ccu
            FROM   ogdzweb.tj_flow_ccu
            WHERE TO_CHAR(CAST(CAST(TIME AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :today
             ORDER BY TIME  DESC
         """

    query_result = db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(ccu_today_sql), today=today)
    ccu_today = [row[0] for row in query_result]

    return jsonify(today_ccu=ccu_today)


@dashboard.route("/dashboard/get_now_ccu")
@login_required
@permission_required('manager')
def get_now_ccu():
    today = current_time(app.config['APP_TIMEZONE']).format('YYYY-MM-DD')

    ccu_now_sql = """
            SELECT all_ccu FROM (
            SELECT all_ccu
            FROM   ogdzweb.tj_flow_ccu
            WHERE TO_CHAR(CAST(CAST(TIME AS TIMESTAMP) AT TIME ZONE '-8:00' AS TIMESTAMP) AT TIME ZONE 'America/New_York', 'yyyy-mm-dd') = :today
            ORDER BY  time DESC
            ) a  WHERE ROWNUM =1
         """

    ccu_now = db.get_engine(db.get_app(), bind='orig_wpt_ods').execute(text(str(ccu_now_sql)), today=today).scalar()
    return jsonify(now_ccu=ccu_now)


@dashboard.route("/convert/<target>", methods=["GET"])
@login_required
@permission_required('manager')
def convert_time_zone(target):
    try:
        timezone_offset_of_from_Beijing_to_EST = get_timezone_offset_of_from_Beijing_to_EST()
        _, someday, _, timezone_offset = generate_sql_date(target)
        someday_arrow = Arrow.strptime(someday, '%Y-%m-%d')
        start_time = someday_arrow.replace(hours=timezone_offset_of_from_Beijing_to_EST).format('YYYY-MM-DD HH:mm:ss')
        end_time = someday_arrow.replace(days=1, hours=timezone_offset_of_from_Beijing_to_EST).format(
            'YYYY-MM-DD HH:mm:ss')
    except Exception as e:

        return jsonify({'error': error_msg_from_exception(e)})

    return jsonify({'start_time': start_time, 'end_time': end_time})
