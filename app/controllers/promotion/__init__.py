from flask import Blueprint

promotion = Blueprint('promotion', __name__)

from .email import email_operate_cohort, email_cohort_display_panel, email_cohort_edit_panel, test_email, \
    email_notification, \
    email_histories, email_sender, email_retry, email_campaign_html_content

from .facebook import facebook_notification, facebook_notification_histories, facebook_notification_retry, \
    facebook_notification_sender, facebook_cohort_display_panel, facebook_operate_cohort

from .mobile import mobile_notification, mobile_notification_histories, mobile_notification_retry, \
    mobile_notification_sender, mobile_operate_cohort
