import json
from collections import defaultdict
from itertools import groupby

import arrow
import hashlib
import re
import sqlparse
from dateutil import tz
from flask import Blueprint, render_template, request, jsonify, abort
from flask import current_app as app
from flask_login import login_required, current_user
from operator import itemgetter
from sqlalchemy import text
from validate_email import validate_email

from app.constants import PROMOTION_PUSH_STATUSES, PROMOTION_PUSH_TYPES
from app.extensions import db, sendgrid, redis_conn
from app.models.main import AdminUserQuery, permission_required
from app.models.promotion import PromotionPush, AutoPromotionPush, retry, push_async
from app.tasks.sendgrid import get_campaigns, get_senders
from app.utils import error_msg_from_exception

promotion = Blueprint('promotion', __name__)

from . import promotion


@promotion.route("/promotion/email", methods=["GET"])
@login_required
@permission_required('manager')
def email_notification():
    sendgrid_campaigns = redis_conn.get('campaigns')
    sendgrid_senders = redis_conn.get('senders')

    if sendgrid_campaigns is None or sendgrid_senders is None:
        campaigns_content = get_campaigns()
        senders_content = get_senders()

    else:

        campaigns_content = json.loads(sendgrid_campaigns)
        senders_content = json.loads(sendgrid_senders)

    campaigns = list(map(lambda x: {'id': x['id'], 'title': x['title'], 'status': x['status']}, campaigns_content))
    senders = reversed(list(map(lambda x: {'id': x['id'], 'nickname': x['nickname']}, senders_content)))
    statuses = list(set(map(lambda x: x['status'], campaigns)))

    based_query_id = request.args.get('based_query_id')
    template_file = 'promotion/email/email_notification.html'
    if based_query_id is None:
        return render_template(template_file, statuses=statuses, campaigns=campaigns, senders=senders)

    based_query = db.session.query(AdminUserQuery).filter_by(id=based_query_id).first()
    if based_query is None:
        return render_template(template_file, statuses=statuses, campaigns=campaigns, senders=senders)
    return render_template(template_file, based_query=based_query, statuses=statuses, campaigns=campaigns,
                           senders=senders)


@promotion.route("/promotion/email/histories", methods=["GET"])
@login_required
@permission_required('manager')
def email_histories():
    data = db.session.query(PromotionPush).filter_by(push_type=PROMOTION_PUSH_TYPES.EMAIL.value,
                                                     is_automated=False).order_by(
        PromotionPush.created_at.desc()).limit(50).all()

    return jsonify(data=[item.to_dict() for item in data])


@promotion.route("/promotion/email/retry", methods=["POST"])
@login_required
@permission_required('manager')
def email_retry():
    push_id = request.form.get('push_id')
    push = db.session.query(PromotionPush).filter_by(id=push_id).one()
    retry.apply_async((push.id,))
    return jsonify(result='ok')


@promotion.route("/promotion/email/campaign_html_content", methods=["GET"])
@login_required
@permission_required('manager')
def email_campaign_html_content():
    campaign_id = int(request.args.get('campaign_id'))
    html_content = None

    sendgrid_campaigns = redis_conn.get('campaigns')

    if sendgrid_campaigns is None:
        campaigns_content = get_campaigns()
    else:
        campaigns_content = json.loads(sendgrid_campaigns)

    for campaign in campaigns_content:
        if campaign_id == campaign['id']:
            html_content = campaign['html_content']
            break
    return jsonify(html_content=html_content)


@promotion.route("/promotion/email/sender_campaign", methods=["POST"])
@permission_required('manager')
def email_sender():
    based_query_id = request.form.get('based_query_id')
    based_rules = json.loads(request.form.get('query_rules', 'null'))
    based_sql = request.form.get("query_rules_sql")
    sender_id = request.form.get('sender_id', type=int)
    campaign_id = request.form.get('campaign_id')
    email_subject = request.form.get('email_subject')

    scheduled_at_est = request.form.get('scheduled_at')
    scheduled_at_utc = arrow.get(scheduled_at_est).replace(tzinfo=tz.gettz(app.config['APP_TIMEZONE'])).to('UTC')

    def get_campaigns_from_sendgrid():
        sendgrid_campaigns = redis_conn.get('campaigns')
        sendgrid_senders = redis_conn.get('senders')

        if sendgrid_campaigns is None or sendgrid_senders is None:

            campaigns_content = get_campaigns()
            senders_content = get_senders()

        else:

            campaigns_content = json.loads(sendgrid_campaigns)
            senders_content = json.loads(sendgrid_senders)

        email_content = \
            [campaign['html_content'] for campaign in campaigns_content if campaign['id'] == int(campaign_id)][0]
        sender = [sender for sender in senders_content if sender['id'] == sender_id][0]
        email_content = email_content. \
            replace("[Sender_Name]", sender['nickname']). \
            replace("[Sender_Address]", sender['address']). \
            replace("[Sender_State]", sender['state']). \
            replace("[Sender_City]", sender['city']). \
            replace("[Sender_Zip]", sender['zip'])

        email_campaign = {"content": [{"type": "text/html", "value": email_content}], "from": sender['from'],
                          "reply_to": sender['reply_to'], "subject": email_subject}

        email_campaign = json.dumps(email_campaign)

        return email_campaign

    email_campaign = get_campaigns_from_sendgrid()

    pending_digest = (str(current_user.id) + '_' + email_campaign + '_' + scheduled_at_utc.format('YYYYMMDD')).encode(
        'utf-8')
    message_key = hashlib.md5(pending_digest).hexdigest()

    push = db.session.query(PromotionPush).filter_by(message_key=message_key).first()

    if push:
        return jsonify(
            error="You have been sent this email before, please change message if you don't need to send the same email."), 500

    try:

        PromotionPush.extract_user_ids(based_query_id, based_sql, for_check=True)

    except Exception as e:

        return jsonify(error=error_msg_from_exception(e)), 500

    push = PromotionPush(
        admin_user_id=current_user.id,
        based_query_id=based_query_id,
        based_sql=based_sql,
        based_query_rules=based_rules,
        push_type=PROMOTION_PUSH_TYPES.EMAIL.value,
        message=email_campaign,
        message_key=message_key,
        status=PROMOTION_PUSH_STATUSES.WAITING.value,
        scheduled_at=scheduled_at_utc.format('YYYY-MM-DD HH:mm:ss'))

    db.session.add(push)
    db.session.commit()

    if app.config['ENV'] == 'prod':
        push_async.apply_async((push.id,), eta=arrow.get(scheduled_at_utc))

    else:
        push_async(push.id)

    return jsonify(result='ok')


@promotion.route("/promotion/email/send_test_email", methods=["POST"])
@permission_required('manager')
def test_email():
    test_email_address = request.form.get('test_email_address')
    email_subject = request.form.get('email_subject')
    campaign_id = request.form.get('campaign_id')
    sender_id = request.form.get('sender_id', type=int)

    test_email_address = test_email_address.split(',')

    if len(test_email_address) > 10:
        return jsonify(error="Can't be exceeding 10"), 500

    invalid_addresses = [email_address for email_address in test_email_address if not validate_email(email_address)]

    if invalid_addresses:
        invalid_addresses = ','.join(invalid_addresses)
        return jsonify(error=invalid_addresses + ' is Invalid'), 500

    sendgrid_campaigns = redis_conn.get('campaigns')
    sendgrid_senders = redis_conn.get('senders')

    if sendgrid_campaigns is None or sendgrid_senders is None:

        campaigns_content = get_campaigns()
        senders_content = get_senders()

    else:

        campaigns_content = json.loads(sendgrid_campaigns)
        senders_content = json.loads(sendgrid_senders)

    sender = [sender for sender in senders_content if sender['id'] == sender_id][0]
    email_content = [campaign['html_content'] for campaign in campaigns_content if campaign['id'] == int(campaign_id)][
        0]

    email_campaign = {"content": [{"type": "text/html", "value": email_content}], "from": sender['from'],
                      "reply_to": sender['reply_to'], "subject": email_subject}

    test_email_recipients = [{"email": email_address} for email_address in test_email_address]
    personalizations = [{"to": [{'email': recipient.get('email')}],
                         "substitutions": {"-country-": recipient.get("country"), "-email-": recipient.get("email"),
                                           "-Play_Username-": recipient.get("username")}} for recipient in
                        test_email_recipients]

    def build_email_campaign(email_campaign, personalizations, **kwargs):

        email_campaign['personalizations'] = personalizations
        #  ubsubscribe
        email_content = email_campaign["content"][0]["value"]
        # custom field
        pattern = re.compile(r'\[%.*?%\]')
        custom_fields = re.findall(pattern, email_content)
        custom_fields_format = ['[%' + (field.split('%')[1]).split(' ')[0] + '%]' for field in custom_fields]
        for i in range(0, len(custom_fields)):
            email_content = email_content.replace(custom_fields[i], custom_fields_format[i])

        email_content = email_content. \
            replace("[Unsubscribe]", '<%asm_group_unsubscribe_raw_url%>'). \
            replace("[Weblink]", "https://www.playwpt.com"). \
            replace("[Sender_Name]", kwargs['nickname']). \
            replace("[Sender_Address]", kwargs['address']). \
            replace("[Sender_State]", kwargs['state']). \
            replace("[Sender_City]", kwargs['city']). \
            replace("[Sender_Zip]", kwargs['zip']). \
            replace("[Unsubscribe_Preferences]", '<%asm_preferences_raw_url%>'). \
            replace("[%country%]", "-country-"). \
            replace("[%Play_Username%]", "-Play_Username-"). \
            replace("[%email%]", "-email-")

        email_campaign["content"][0]["value"] = email_content

        # ubsubscribe
        suppression = {"group_id": 2161, "groups_to_display": [2161]}
        email_campaign['asm'] = suppression

        return email_campaign

    data = build_email_campaign(email_campaign, personalizations, **sender)

    try:
        sendgrid.client.mail.send.post(request_body=data)

    except Exception as e:

        return jsonify(error=error_msg_from_exception(e)), 500

    else:
        return jsonify(result='ok'), 202


########## ########## ########## ########## ########## AUTO_PROMOTION ########## ########## ########## ########## ##########

@promotion.route("/promotion/email/cohorts/display", methods=["GET"])
@login_required
@permission_required('manager')
def email_cohort_display_panel():

    cohorts = db.session.query(AutoPromotionPush).all()

    cohort_and_category_mapping = [{'cohort': cohort, 'category': cohort.category} for cohort in cohorts if
                                   cohort.push.push_type == PROMOTION_PUSH_TYPES.EMAIL.value]

    cohort_and_category_mapping.sort(key=itemgetter('category'))
    cohort_and_category_mapping = groupby(cohort_and_category_mapping, itemgetter('category'))

    cohort_group_by_category = defaultdict(list)
    for category, cohorts in cohort_and_category_mapping:
        for cohort in cohorts:
            cohort_group_by_category[category].append(cohort['cohort'])

    items=[]
    for key, value in cohort_group_by_category.items():
        items.append({'category': key, 'cohorts': value})


    return render_template('promotion/cohorts_display.html', items=items, push_type='Email')


@promotion.route("/promotion/email/cohort/<cohort_name>", methods=["GET"])
@login_required
@permission_required('manager')
def email_cohort_edit_panel(cohort_name):
    cohort = AutoPromotionPush.get_email_cohort(cohort_name).first()
    if cohort is None:
        return abort(404)

    campaigns = AutoPromotionPush.get_sendgrid_campaigns()
    campaign_options = json.loads(cohort.campaign_options)
    sql_options = json.loads(cohort.push.based_sql)
    return render_template('promotion/email/email_cohort_editing.html',
                           cohort=cohort,
                           sender_id=campaign_options['sender_id'],
                           email_subject=campaign_options['email_subject'],
                           sql=sql_options['formatted_sql'],
                           database=sql_options['database'],
                           campaign_options=json.loads(cohort.campaign_options), **campaigns)


@promotion.route("/promotion/email/cohorts/operate", methods=['GET', 'POST', 'PUT', 'DELETE'])
@login_required
@permission_required('manager')
def email_operate_cohort():
    if request.method == 'GET':
        campaigns = AutoPromotionPush.get_sendgrid_campaigns()
        return render_template('promotion/email/email_cohort_create.html', **campaigns)
    elif request.method == 'POST':
        cohort_name = request.form.get('cohort_name')
        cohort_name = cohort_name.strip()
        category = request.form.get('category').strip()

        sender_id = request.form.get('sender_id', type=int)
        scheduled_at = request.form.get('scheduled_at')
        campaign_id = request.form.get('campaign_id')
        is_important = request.form.get('is_important')
        category = request.form.get('category')
        email_subject = request.form.get('email_subject').strip()
        database = request.form.get('database')
        sql = request.form.get('sql')
        description = request.form.get('description').strip()
        maximum_count = request.form.get('maximum_count')
        rearm = request.form.get('rearm')
        formatted_sql = sqlparse.format(sql.strip().strip(';'), reindent=True, keyword_case='upper')

        def evaluate_sql(sql, database):
            stmt = sqlparse.parse(sql)[0]
            tokens = [str(item) for item in stmt.tokens]
            slim_sql = ''.join(tokens)
            if database == 'bi': database = None
            result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(slim_sql))
            column_names = [col[0] for col in result_proxy.cursor.description]
            column_names = [column_name.lower() for column_name in column_names]

            sql_tokens = [str(tokens).upper() for tokens in [tokens for tokens in stmt.tokens if tokens.is_keyword]]
            sql_limit = any(
                [tokens in ['INSERT', 'UPDATE', 'DELETE', 'USE', 'DROP', 'TRUNCATE'] for tokens in sql_tokens])

            if not ('user_id' in column_names or 'og_account' in column_names or 'username' in column_names):

                return False, "In your SQL statement, you must provide username or user_id or org_account"


            elif sql_limit:
                return False, "Your SQL statements are not allowed against this database"
            else:

                return True, 'ok'

        def create_cohort_options(sender_id, scheduled_at, campaign_id, email_subject, based_sql):
            sendgrid_campaigns = redis_conn.get('campaigns')
            sendgrid_senders = redis_conn.get('senders')

            if sendgrid_campaigns is None or sendgrid_senders is None:
                campaigns_content = get_campaigns()
                senders_content = get_senders()
            else:
                campaigns_content = json.loads(sendgrid_campaigns)
                senders_content = json.loads(sendgrid_senders)

            email_content = \
                [campaign['html_content'] for campaign in campaigns_content if campaign['id'] == int(campaign_id)][0]
            sender = [sender for sender in senders_content if sender['id'] == sender_id][0]

            email_content = email_content. \
                replace("[Sender_Name]", sender['nickname']). \
                replace("[Sender_Address]", sender['address']). \
                replace("[Sender_State]", sender['state']). \
                replace("[Sender_City]", sender['city']). \
                replace("[Sender_Zip]", sender['zip'])

            scheduled_at = arrow.get(scheduled_at).replace(tzinfo=tz.gettz(app.config['APP_TIMEZONE'])).to('UTC')
            email_campaign = json.dumps(
                {"content": [{"type": "text/html", "value": email_content}], "from": sender['from'],
                 "reply_to": sender['reply_to'], "subject": email_subject})

            formatted_message = message.strip()
            pending_digest = (
                    str(current_user.id) + '_' + formatted_message + '_' + scheduled_at.format('YYYYMMDD')).encode(
                'utf-8')
            message_key = hashlib.md5(pending_digest).hexdigest()

            push = PromotionPush(
                admin_user_id=current_user.id,
                based_sql=json.dumps(dict(formatted_sql=formatted_sql, database=database)),
                push_type=PROMOTION_PUSH_TYPES.EMAIL.value,
                message=email_campaign,
                message_key=message_key,
                is_automated=True,
                cohort_name=cohort_name,
                status=PROMOTION_PUSH_STATUSES.WAITING.value,
                scheduled_at=scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

            db.session.add(push)
            db.session.commit()

            cohort = AutoPromotionPush.get_email_cohort(cohort_name).delete()

            db.session.flush()
            db.session.commit()

            cohort = AutoPromotionPush(push_id=push.id,
                                       cohort_name=cohort_name,
                                       category=category,
                                       description=description,
                                       is_important=is_important,
                                       push_type=PROMOTION_PUSH_TYPES.EMAIL.value,
                                       is_activated=False, is_paused=False,
                                       campaign_id=campaign_id,
                                       maximum_count=maximum_count,
                                       campaign_options=json.dumps(
                                           dict(sender_id=sender_id, email_subject=email_subject)),
                                       rearm=rearm)
            db.session.add(cohort)
            db.session.commit()

        try:
            is_valid_sql, message = evaluate_sql(formatted_sql, database)

            if is_valid_sql:

                create_cohort_options(sender_id, scheduled_at, campaign_id, email_subject, formatted_sql)

                return jsonify(message=message), 200

            else:

                return jsonify(error=message), 500

        except Exception as e:

            return jsonify(error=error_msg_from_exception(e)), 500
    elif request.method == 'DELETE':
        cohort_id = request.form.get('cohort_id', type=int)
        cohort = db.session.query(AutoPromotionPush).filter_by(id=cohort_id)
        cohort.delete()
        db.session.commit()
        return jsonify(message='ok'), 200
    elif request.method == 'PUT':
        cohort_id = request.form.get('cohort_id', type=int)
        operate = request.form.get('operate')
        try:
            cohort = db.session.query(AutoPromotionPush).filter_by(id=cohort_id).one()
        except:
            return jsonify(message='not found'), 200
        else:
            if operate == 'activate':
                cohort.is_activated = True
                cohort.is_paused = False
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
            if operate == 'pause':
                cohort.is_paused = True
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
    else:
        return abort(404)


# 获得此cohort对应的push的每次推广的记录
@promotion.route("/promotion/email/cohort/<cohort_name>/latest/histories", methods=["GET"])
@login_required
@permission_required('manager')
def the_latest_cohort_for_email(cohort_name):
    try:
        cohort = AutoPromotionPush.get_email_cohort(cohort_name).one()
    except:
        return jsonify(message='error'), 500
    else:

        data = [cohort.push.to_dict_for_cohort(count=count) for count in reversed((range(0, cohort.push.rearm_count)))]

    return jsonify(data=data)
