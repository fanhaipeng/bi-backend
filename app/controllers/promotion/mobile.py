import json
from itertools import groupby
from operator import itemgetter

from collections import defaultdict

import arrow
import base64
import hashlib
import sqlparse
from dateutil import tz
from flask import current_app as app
from flask import render_template, request, jsonify, abort
from flask_login import login_required, current_user
from sqlalchemy import text

from app.constants import PROMOTION_PUSH_STATUSES, PROMOTION_PUSH_TYPES
from app.extensions import db
from app.models.main import AdminUserQuery, permission_required
from app.models.promotion import PromotionPush, AutoPromotionPush, retry, push_async
from app.utils import error_msg_from_exception, convert_emoji_to_unicode
from . import promotion


@promotion.route("/promotion/mobile_notification", methods=["GET"])
@login_required
@permission_required('manager')
def mobile_notification():
    based_query_id = request.args.get('based_query_id')
    if based_query_id is None:
        return render_template('promotion/mobile/mobile_notification.html')

    based_query = db.session.query(AdminUserQuery).filter_by(id=based_query_id).first()

    if based_query is None:
        return render_template('promotion/mobile/mobile_notification.html')

    return render_template('promotion/mobile/mobile_notification.html', based_query=based_query)


@promotion.route("/promotion/mobile_notification/histories", methods=["GET"])
@login_required
@permission_required('manager')
def mobile_notification_histories():
    data = db.session.query(PromotionPush).filter_by(push_type=PROMOTION_PUSH_TYPES.MOBILE.value,
                                                     is_automated=False).order_by(
        PromotionPush.created_at.desc()).limit(50).all()

    return jsonify(data=[item.to_dict() for item in data])


@promotion.route("/promotion/mobile_notification/retry", methods=["POST"])
@login_required
@permission_required('manager')
def mobile_notification_retry():
    push_id = request.form.get('push_id')
    push = db.session.query(PromotionPush).filter_by(id=push_id).one()
    retry.apply_async((push.id,))
    return jsonify(result='ok')


@promotion.route("/promotion/mobile_notification/sender", methods=["POST"])
@login_required
@permission_required('manager')
def mobile_notification_sender():
    based_query_id = request.form.get('based_query_id')
    based_rules = json.loads(request.form.get('query_rules', 'null'))
    based_sql = request.form.get("query_rules_sql")

    title = convert_emoji_to_unicode(request.form.get('title'))
    body = convert_emoji_to_unicode(request.form.get('body'))

    event_type = request.form.get('event_type')

    scheduled_at_est = request.form.get('scheduled_at')
    scheduled_at_utc = arrow.get(scheduled_at_est).replace(tzinfo=tz.gettz(app.config['APP_TIMEZONE'])).to('UTC')

    pending_digest = (
        str(current_user.id) + '_' + title + event_type + body + '_' + scheduled_at_utc.format('YYYYMMDD')).encode(
        'utf-8')
    message_key = hashlib.md5(pending_digest).hexdigest()
    push = db.session.query(PromotionPush).filter_by(message_key=message_key).first()

    if push:
        return jsonify(
            error="You have been sent this message before, Please change message if you don't need to send the same message."), 500

    try:
        PromotionPush.extract_user_ids(based_query_id, based_sql, for_check=True)

    except Exception as e:

        return jsonify(error=error_msg_from_exception(e)), 500

    message = json.dumps(
        {'body': base64.b64encode(bytes(body, 'utf-8')).decode(),
         'title': base64.b64encode(bytes(title, 'utf-8')).decode(), 'event_type': event_type})

    push = PromotionPush(admin_user_id=current_user.id,
                         based_query_id=based_query_id,
                         based_sql=based_sql,
                         based_query_rules=based_rules,
                         status=PROMOTION_PUSH_STATUSES.WAITING.value,
                         push_type=PROMOTION_PUSH_TYPES.MOBILE.value,
                         message=message,
                         message_key=message_key,
                         scheduled_at=scheduled_at_utc.format('YYYY-MM-DD HH:mm:ss'))

    db.session.add(push)
    db.session.commit()

    if app.config['ENV'] == 'prod':
        push_async.apply_async((push.id,), eta=arrow.get(scheduled_at_utc))
    else:
        push_async(push.id)
    return jsonify(result='ok')


########## ########## ########## ########## ########## AUTO_PROMOTION ########## ########## ########## ########## ##########

@promotion.route("/promotion/mobile/cohorts/display", methods=["GET"])
@login_required
@permission_required('manager')
def mobile_cohort_display_panel():
    cohorts = db.session.query(AutoPromotionPush).all()

    cohort_and_category_mapping = [{'cohort': cohort, 'category': cohort.category} for cohort in cohorts if
                                   cohort.push.push_type == PROMOTION_PUSH_TYPES.MOBILE.value]

    cohort_and_category_mapping.sort(key=itemgetter('category'))
    cohort_and_category_mapping = groupby(cohort_and_category_mapping, itemgetter('category'))

    cohort_group_by_category = defaultdict(list)
    for category, cohorts in cohort_and_category_mapping:
        for cohort in cohorts:
            cohort_group_by_category[category].append(cohort['cohort'])

    items=[]
    for key, value in cohort_group_by_category.items():
        items.append({'category': key, 'cohorts': value})

    return render_template('promotion/cohorts_display.html', items=items, push_type='Mobile')



@promotion.route("/promotion/mobile/cohort/<cohort_name>", methods=["GET"])
@login_required
@permission_required('manager')
def mobile_cohort_edit_panel(cohort_name):
    cohort = AutoPromotionPush.get_mobile_cohort(cohort_name).first()
    if cohort is None:
        return abort(404)

    message = json.loads(cohort.push.message)
    sql_options = json.loads(cohort.push.based_sql)
    title = message.get('title')
    if title: title = base64.b64decode(title).decode()

    body = base64.b64decode(message.get('body')).decode().replace('\n', '\\n')
    event_type = message.get('event_type')

    return render_template('promotion/mobile/mobile_cohort_editing.html',
                           cohort=cohort,
                           sql=sql_options['formatted_sql'],
                           database=sql_options['database'],
                           body=body, title=title, event_type=event_type)


@promotion.route("/promotion/mobile/cohorts/operate", methods=['GET', 'POST', 'PUT', 'DELETE'])
@login_required
@permission_required('manager')
def mobile_operate_cohort():
    if request.method == 'GET':
        return render_template('promotion/mobile/mobile_cohort_create.html')
    elif request.method == 'POST':
        cohort_name = request.form.get('cohort_name')
        cohort_name = cohort_name.strip()
        category = request.form.get('category').strip()

        scheduled_at = request.form.get('scheduled_at')
        is_important = request.form.get('is_important')
        database = request.form.get('database')
        sql = request.form.get('sql')
        description = request.form.get('description').strip()
        maximum_count = request.form.get('maximum_count')
        title = convert_emoji_to_unicode(request.form.get('title'))
        body = convert_emoji_to_unicode(request.form.get('body'))

        event_type = request.form.get('event_type')
        rearm = request.form.get('rearm')

        formatted_sql = sqlparse.format(sql.strip().strip(';'), reindent=True, keyword_case='upper')

        message = json.dumps(
            {'body': base64.b64encode(bytes(body, 'utf-8')).decode(),
             'title': base64.b64encode(bytes(title, 'utf-8')).decode(), 'event_type': event_type})

        def evaluate_sql(sql, database):
            stmt = sqlparse.parse(sql)[0]
            tokens = [str(item) for item in stmt.tokens]
            slim_sql = ''.join(tokens)
            if database == 'bi': database = None
            result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(slim_sql))
            column_names = [col[0] for col in result_proxy.cursor.description]
            column_names = [ column_name.lower() for column_name in column_names]

            sql_tokens = [str(tokens).upper() for tokens in [tokens for tokens in stmt.tokens if tokens.is_keyword]]
            sql_limit = any(
                [tokens in ['INSERT', 'UPDATE', 'DELETE', 'USE', 'DROP', 'TRUNCATE'] for tokens in sql_tokens])

            if not ('user_id'  in column_names or 'og_account' in column_names or'username' in column_names):
                return False, "In your SQL statement, you must provide username or user_id or org_account"


            elif sql_limit:
                return False, "Your SQL statements are not allowed against this database"
            else:
                return True, 'ok'

        try:
            is_valid_sql, alert = evaluate_sql(formatted_sql, database)

            if is_valid_sql:

                pending_digest = (
                    str(current_user.id) + '_' + event_type + title + body + '_' + scheduled_at.format(
                        'YYYYMMDD')).encode(
                    'utf-8')
                message_key = hashlib.md5(pending_digest).hexdigest()

                scheduled_at = arrow.get(scheduled_at).replace(tzinfo=tz.gettz(app.config['APP_TIMEZONE'])).to('UTC')
                push = PromotionPush(
                    admin_user_id=current_user.id,
                    based_sql=json.dumps(dict(formatted_sql=formatted_sql, database=database)),
                    push_type=PROMOTION_PUSH_TYPES.MOBILE.value,
                    message=message,
                    message_key=message_key,
                    is_automated=True,
                    cohort_name=cohort_name,
                    status=PROMOTION_PUSH_STATUSES.WAITING.value,
                    scheduled_at=scheduled_at.format('YYYY-MM-DD HH:mm:ss'))

                db.session.add(push)
                db.session.commit()

                cohort = AutoPromotionPush.get_mobile_cohort(cohort_name).delete()
                db.session.flush()
                db.session.commit()

                cohort = AutoPromotionPush(push_id=push.id,
                                           cohort_name=cohort_name,
                                           category=category,
                                           description=description,
                                           push_type=PROMOTION_PUSH_TYPES.MOBILE.value,
                                           is_important=is_important,
                                           is_activated=False,
                                           is_paused=False,
                                           maximum_count=maximum_count,
                                           rearm=rearm)
                db.session.add(cohort)
                db.session.commit()

                return jsonify(message=alert), 200

            else:

                return jsonify(error=alert), 500

        except Exception as e:

            return jsonify(error=error_msg_from_exception(e)), 500

    elif request.method == 'DELETE':
        cohort_id = request.form.get('cohort_id', type=int)
        cohort = db.session.query(AutoPromotionPush).filter_by(id=cohort_id)
        cohort.delete()
        db.session.commit()
        return jsonify(message='ok'), 200
    elif request.method == 'PUT':
        cohort_id = request.form.get('cohort_id', type=int)
        operate = request.form.get('operate')
        try:
            cohort = db.session.query(AutoPromotionPush).filter_by(id=cohort_id).one()
        except:
            return jsonify(message='not found'), 200
        else:
            if operate == 'activate':
                cohort.is_activated = True
                cohort.is_paused = False
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
            if operate == 'pause':
                cohort.is_paused = True
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
    else:
        return abort(404)


# 获得此cohort对应的push的每次推广的记录
@promotion.route("/promotion/mobile/cohort/<cohort_name>/latest/histories", methods=["GET"])
@login_required
@permission_required('manager')
def the_latest_cohort_for_mobile(cohort_name):
    try:
        cohort = AutoPromotionPush.get_mobile_cohort(cohort_name).one()

    except:
        return jsonify(message='error'), 500
    else:

        data = [cohort.push.to_dict_for_cohort(count=count) for count in reversed((range(0,cohort.push.rearm_count)))]

    return jsonify(data=data)
