import arrow
import sqlparse
from dateutil import tz
from flask import Blueprint
from flask import abort
from flask import current_app as app
from flask import render_template, request, jsonify
from flask_login import login_required
from sqlalchemy import text
from validate_email import validate_email

from app.extensions import db
from app.models.main import permission_required
from app.models.scheduled_report import ScheduledReport
from app.utils import error_msg_from_exception

auto_report = Blueprint('auto_report', __name__)


@auto_report.route("/auto_reports/display", methods=["GET"])
@login_required
@permission_required('manager')
def report_display_panel():
    reports = db.session.query(ScheduledReport).all()
    return render_template('auto_report/reports_display.html', reports=reports)


@auto_report.route("/auto_report/<report_name>", methods=["GET"])
@login_required
@permission_required('manager')
def report_edit_panel(report_name):
    report = ScheduledReport.get_report(report_name).one()
    if report is None:
        return abort(404)

    return render_template('auto_report/report_editing.html', report=report)


@auto_report.route("/auto_reports/operate", methods=['GET', 'POST', 'PUT', 'DELETE'])
@login_required
@permission_required('manager')
def report_operate_panel():
    if request.method == 'GET':
        return render_template('auto_report/report_create.html')

    elif request.method == 'POST':

        sql = request.form.get('sql')
        report_name = request.form.get('report_name')
        description = request.form.get('description').strip()
        scheduled_at = request.form.get('scheduled_at')
        maximum_count = request.form.get('maximum_count')
        rearm = request.form.get('rearm')
        formatted_sql = sqlparse.format(sql.strip().strip(';'), reindent=True, keyword_case='upper')
        database = request.form.get('database')
        recipients = request.form.get('recipients')

        test_email_address = recipients.split(',')

        if len(test_email_address) > 10:
            return jsonify(error="Can't be exceeding 10"), 500

        invalid_addresses = [email_address for email_address in test_email_address if not validate_email(email_address)]

        if invalid_addresses:
            invalid_addresses = ','.join(invalid_addresses)
            return jsonify(error=invalid_addresses + ' is Invalid'), 500

        def evaluate_sql(sql, database):
            stmt = sqlparse.parse(sql)[0]
            tokens = [str(item) for item in stmt.tokens]
            slim_sql = ''.join(tokens)
            if database == 'bi':
                database = None

            result_proxy = db.get_engine(db.get_app(), bind=database).execute(text(slim_sql))

            sql_tokens = [str(tokens).upper() for tokens in [tokens for tokens in stmt.tokens if tokens.is_keyword]]
            sql_limit = any(
                [tokens in ['INSERT', 'UPDATE', 'DELETE', 'USE', 'DROP', 'TRUNCATE'] for tokens in sql_tokens])

            if sql_limit:
                return False, "Your SQL statements are not allowed against this database"
            else:

                return True, 'ok'

        try:
            is_valid_sql, alert = evaluate_sql(formatted_sql, database)

            if is_valid_sql:

                report = ScheduledReport.get_report(report_name).delete()

                scheduled_at_utc = arrow.get(scheduled_at).replace(tzinfo=tz.gettz(app.config['APP_TIMEZONE'])).to(
                    'UTC')

                report = ScheduledReport(sql=sql,
                                         database=database,
                                         recipients=recipients,
                                         report_name=report_name,
                                         description=description,
                                         is_activated=False,
                                         is_paused=False,
                                         scheduled_at=scheduled_at_utc.format('YYYY-MM-DD HH:mm:ss'),
                                         maximum_count=maximum_count,
                                         rearm=rearm)
                db.session.add(report)
                db.session.commit()

                return jsonify(message=alert), 200

            else:

                return jsonify(error=alert), 500

        except Exception as e:

            return jsonify(error=error_msg_from_exception(e)), 500

    elif request.method == 'DELETE':
        report_id = request.form.get('report_id', type=int)

        report = db.session.query(ScheduledReport).filter_by(id=report_id)
        report.delete()
        db.session.commit()
        return jsonify(message='ok'), 200
    elif request.method == 'PUT':
        report_id = request.form.get('report_id', type=int)
        operate = request.form.get('operate')

        try:
            report = db.session.query(ScheduledReport).filter_by(id=report_id).one()
        except:
            return jsonify(message='not found'), 200

        else:

            if operate == 'activate':
                report.is_activated = True
                report.is_paused = False
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
            if operate == 'pause':
                report.is_paused = True
                db.session.flush()
                db.session.commit()
                return jsonify(message='ok'), 200
    else:
        return abort(404)
