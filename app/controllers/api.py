import datetime

import pytz
from flask import Blueprint, jsonify
from flask import request, abort

from app import BIUser
from app.extensions import db
from app.models.promotion import send_message_to_user
from app.utils import error_msg_from_exception

api = Blueprint('api', __name__)

now = datetime.datetime.now(pytz.timezone('Asia/Shanghai')).replace(tzinfo=None)


@api.route('/api/gold_notification', methods=["POST"])
def gold_notification():
    try:
        if request.method == 'POST':
            ip = request.remote_addr

            user_id = request.form.get('user_id')
            increment = request.form.get('increment')
            time = datetime.datetime.strptime(request.form.get('time'), '%Y-%m-%d %H:%M:%S')
            interval = (now - time).total_seconds()
            gold_balance = request.form.get('gold_balance')

            user_id = db.session.query(BIUser.user_id).filter_by(user_id=user_id).scalar()

            if ip not in ['127.0.0.1', '10.10.0.247', '10.10.2.153', '54.89.38.78', '10.10.0.79', '10.10.0.76', '10.10.0.26', ]:
                return jsonify(message='非法请求, ip: {}'.format(ip)), 401

            elif user_id is None:
                return jsonify(message='没有这个用户'), 400

            elif interval > 5 * 60:
                return jsonify(message='消息已过期'), 400

            else:
                send_message_to_user.apply_async(args=(user_id, increment, gold_balance))
                return jsonify(message='已接收'), 202

        else:
            return abort(404)

    except Exception as e:

        return jsonify({'error': error_msg_from_exception(e)})

