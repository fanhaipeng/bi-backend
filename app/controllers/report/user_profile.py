import json
import time

from arrow import Arrow
from flask import current_app as app, jsonify
from flask import render_template
from flask import request
from flask_login import login_required
from sqlalchemy import func, and_, desc

from app import db, BIUser, BIUserBill
from app.extensions import redis_conn
from app.models.bi import BIUserStatistic
from app.models.main import permission_required
from app.models.orig_wpt import WPTUserAvatarChange, WPTUserAvatar, WPTAvatarStorage, WPTUserInfo
from app.utils import row_to_dict, current_time
from . import report


@report.route("/report/user_profile", methods=["GET"])
@login_required
def get_user_profile():
    user_index = request.args.get('user_index')
    user_index = user_index.strip()

    # It is possible to enter username or displayname

    user_id = db.session.query(BIUser.user_id).filter(BIUser.username == user_index).scalar()

    if user_id is None:
        user_id = db.session.query(BIUser.user_id).filter(BIUser.display_name == user_index).scalar()
        user_index = db.session.query(BIUser.username).filter(BIUser.display_name == user_index).scalar()

    if user_id is None:
        return render_template('report/user_profile.html', user_id=user_id, user_index=user_index)

    key_prefix = 'BI/user_records/'
    user_records = redis_conn.hgetall(key_prefix + str(user_id))

    if user_records is not None:
        user_records = {date: json.loads(user_records[date]) for date in user_records}
        activity_records = {str(time.mktime(time.strptime(date, '%y%m%d'))): user_records[date]['game_dau'] for date
                            in user_records if user_records[date].get('game_dau', None) is not None}
        purchase_records = {str(time.mktime(time.strptime(date, '%y%m%d'))): user_records[date]['purchase'] for
                            date in user_records if user_records[date].get('purchase', None) is not None}

        activity_day = len([record for record in activity_records.values() if record == 1])


    else:
        purchase_records = {}
        activity_records = {}
        activity_day = 0

    id, gold_balance, reg_time, reg_country, reg_state = db.session.query(BIUser.id, BIUser.gold_balance,
                                                                          BIUser.reg_time, BIUser.reg_country,
                                                                          BIUser.reg_state).filter(
        BIUser.user_id == user_id).first()

    lifetime_days = (current_time(app.config['APP_TIMEZONE']) - reg_time).days

    activity_level = (activity_day, lifetime_days)

    purchase = db.session.query(func.sum(BIUserBill.currency_amount)).filter(
        and_(BIUserBill.user_id == user_id, BIUserBill.currency_type == 'Dollar')).scalar()

    avatar_id = db.session.query(WPTUserAvatarChange.avatar_id).filter(WPTUserAvatarChange.user_id == user_id).order_by(
        desc(WPTUserAvatarChange.created_on_utc)).first()

    if gold_balance is not None:
        gold_balance = format(gold_balance, ',d')

    if avatar_id is None:
        avatar_id = db.session.query(WPTUserAvatar.avatar_id).filter(WPTUserAvatar.user_id == user_id).order_by(
            desc(WPTUserAvatar.created_on_utc)).first()

    avatar_id = avatar_id[0]
    avatar_url = db.session.query(WPTAvatarStorage.big_avatar_url).filter(
        WPTAvatarStorage.avatar_id == avatar_id).scalar()

    play_count = db.session.query(WPTUserInfo.playcgznum).filter(WPTUserInfo.username == user_index).scalar()
    wins_count = db.session.query(WPTUserInfo.wincgznum).filter(WPTUserInfo.username == user_index).scalar()

    return render_template('report/user_profile.html',
                           reg_time=reg_time.strftime('%Y-%m-%d %H:%M:%S'),
                           gold_balance=gold_balance,
                           activity_level=activity_level,
                           purchase=format(purchase or 0, '0.2f'),
                           purchase_records=purchase_records,
                           activity_records=activity_records,
                           user_records=json.dumps(user_records),
                           user_index=user_index,
                           avatar_id=avatar_id,
                           avatar_url=avatar_url,
                           user_id=user_id,
                           play_count=play_count,
                           wins_count=wins_count,
                           reg_state=reg_state,
                           reg_country=reg_country,
                           id=id)


@report.route("/report/user_records", methods=["GET"])
def get_user_records():
    date = request.args.get('date')
    user_id = request.args.get('user_id')
    date_fmt = Arrow.strptime(date, '%Y/%m/%d').strftime('%Y-%m-%d')

    query_result = [row_to_dict(row) for row in
                    db.session.query(BIUserStatistic).filter(BIUserStatistic.user_id == int(user_id),
                                                             BIUserStatistic.stats_date == date_fmt).all()][0]

    dau_related = {metric: query_result[metric] for metric in query_result if
                   metric in ['ring_dau', 'sng_dau', 'mtt_dau', 'store_dau', 'slots_dau', 'promo_dau']}
    dollar_related = {metric: query_result[metric] for metric in query_result if
                      metric in ['lucky_spin_spend', 'dollar_gold_pkg_spend', 'dollar_silver_pkg_spend',
                                 'dollar_purchase_count', 'dollar_spend', ]}
    gold_related = {metric: query_result[metric] for metric in query_result if
                    metric in ['avatar_spend', 'charms_spend', 'emoji_spend', 'table_gift_spend', 'free_gold',
                               'gold_to_silver', 'lucky_spins']}
    game_related = {metric: query_result[metric] for metric in query_result if
                    metric in ['sng_rake', 'sng_gold_buyins', 'sng_gold_winnings', 'sng_gold_entries', 'ring_rake',
                               'ring_hands', 'mtt_rake', 'mtt_gold_buyins', 'mtt_gold_winnings', 'mtt_gold_entries',
                               'slots_winnings', 'slots_wagered', 'slots_spins']}

    return jsonify(dau_related=dau_related, dollar_related=dollar_related, gold_related=gold_related,
                   game_related=game_related)
