from flask import current_app as app
from flask import render_template
from flask_login import login_required
from sqlalchemy import text

from app import db
from app.constants import  POKER_TRANSACTION_TYPES
from app.models.main import permission_required
from app.utils import current_time
from . import report


@report.route("/report/sankey", methods=["GET"])
@login_required
@permission_required('manager')
def sankey():
    now = current_time(app.config['APP_TIMEZONE'])
    yesterday = now.replace(days=-1).format('YYYY-MM-DD')

    # web email reg related

    web_email_reg = db.engine.execute(text("""
                                        SELECT
                                        COUNT(*)
                                        FROM
                                        bi_user
                                        WHERE
                                        DATE(convert_tz(reg_time, '+00:00', '-04:00')) =:yesterday
                                        AND reg_source = 'Web'
                                        AND reg_facebook_connect = 0
                                        AND email IS NOT NULL
                                      """), yesterday=yesterday).scalar()

    web_email_input_name = db.engine.execute(text("""
                                        SELECT COUNT(*)
                                        FROM bi_user
                                        WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                        AND reg_source ='Web' AND username IS NOT NULL
                                        AND reg_facebook_connect = 0
                                        AND email IS NOT NULL
                                      """), yesterday=yesterday).scalar()

    web_email_input_name_users = [row[0] for row in
                                  list(db.engine.execute(text("""
                                        SELECT user_id
                                        FROM bi_user
                                        WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                        AND reg_source ='Web' AND username IS NOT NULL
                                        AND reg_facebook_connect = 0
                                        AND email IS NOT NULL
                                        """), yesterday=yesterday))]

    web_email_select_avatar = db.get_engine(db.get_app(), bind='orig_wpt').execute(text("""
                                        SELECT COUNT(DISTINCT user_id) 
                                        FROM avatar_user_assign 
                                        WHERE DATE (convert_tz(created_on_utc, '+00:00', '-04:00'))= :yesterday 
                                        AND user_id IN :user_id """), yesterday=yesterday, user_id=tuple(
        web_email_input_name_users)).scalar()

    web_email_validated = db.engine.execute(text("""
                                            SELECT COUNT(*)
                                            FROM bi_user
                                            WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                            AND reg_source ='Web' AND account_status='email validated'
                                            AND reg_facebook_connect = 0
                                            AND email IS NOT NULL
                                               """), yesterday=yesterday).scalar()

    web_email_new_dau = db.engine.execute(text("""
                                            SELECT COUNT( DISTINCT bi_user.user_id)
                                            FROM bi_user
                                            INNER JOIN bi_user_currency ON bi_user.user_id = bi_user_currency.user_id
                                            WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                            AND reg_source = 'Web' AND account_status = 'email validated'
                                            AND reg_facebook_connect = 0
                                            AND email IS NOT NULL
                                            AND DATE (CONVERT_TZ(created_at, '+00:00', '-04:00')) = :yesterday
                                            AND transaction_type   IN :poker_transaction_types
                                            """), yesterday=yesterday,
                                          poker_transaction_types=POKER_TRANSACTION_TYPES).scalar()

    # web facebook reg related

    web_facebook_login_reg = db.engine.execute(text("""
                                                SELECT COUNT(*)
                                                FROM bi_user
                                                WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                                AND reg_source ='Web Facebook' AND username IS NOT NULL
                                                AND reg_facebook_connect = 1
                                                """), yesterday=yesterday).scalar()

    web_facebook_input_name = db.engine.execute(text("""
                                                SELECT COUNT(*)
                                                FROM bi_user
                                                WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                                AND reg_source ='Web Facebook' AND username IS NOT NULL
                                                AND reg_facebook_connect = 1
                                               """), yesterday=yesterday).scalar()

    web_facebook_input_name_users = [row[0] for row in
                                     list(db.engine.execute(text("""
                                                SELECT user_id
                                                FROM bi_user
                                                WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                                AND reg_facebook_connect = 1
                                                AND reg_source ='Web Facebook' AND username IS  NOT NULL
                                                                                """), yesterday=yesterday))]

    web_facebook_select_avatar = db.get_engine(db.get_app(), bind='orig_wpt').execute(text("""
                                                SELECT COUNT(DISTINCT user_id) 
                                                FROM avatar_user_assign 
                                                WHERE DATE (convert_tz(created_on_utc, '+00:00', '-04:00'))=:yesterday
                                                AND user_id IN :user_id """), yesterday=yesterday, user_id=tuple(
        web_facebook_input_name_users)).scalar()

    web_facebook_new_dau = db.engine.execute(text("""
                                            SELECT COUNT( DISTINCT bi_user.user_id)
                                            FROM bi_user
                                            INNER JOIN bi_user_currency ON bi_user.user_id = bi_user_currency.user_id
                                            WHERE DATE (convert_tz(reg_time, '+00:00', '-04:00')) = :yesterday
                                            AND reg_source = 'Web Facebook' 
                                            AND reg_facebook_connect = 1
                                            AND DATE (CONVERT_TZ(created_at, '+00:00', '-04:00')) = :yesterday
                                            AND transaction_type   IN :poker_transaction_types
                                      """), yesterday=yesterday,
                                             poker_transaction_types=POKER_TRANSACTION_TYPES).scalar()

    return render_template('report/sankey.html', web_email_reg=web_email_reg,
                           web_email_input_name=web_email_input_name,
                           web_email_select_avatar=web_email_select_avatar,
                           web_email_validated=web_email_validated,
                           web_email_new_dau=web_email_new_dau,
                           web_facebook_login_reg=web_facebook_login_reg,
                           web_facebook_input_name=web_facebook_input_name,
                           web_facebook_select_avatar=web_facebook_select_avatar,
                           web_facebook_new_dau=web_facebook_new_dau)
