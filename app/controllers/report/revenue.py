import datetime

import arrow
from flask import current_app as app
from flask import render_template, jsonify, request
from flask_login import login_required
from sqlalchemy import text

from app.extensions import db
from app.models.main import permission_required
from app.utils import current_time
from . import report


@report.route("/report/revenue", methods=["GET"])
@login_required
@permission_required('manager')
def revenue():
    return render_template('report/revenue.html')


@report.route("/report/revenue_for_platform")
@login_required
@permission_required('manager')
def revenue_for_platform():
    group_type = request.args.get("group_type")
    now = current_time(app.config['APP_TIMEZONE'])
    start_time = now.replace(days=-int(10)).format('YYYY-MM-DD')
    end_time = now.format('YYYY-MM-DD')
    timezone_offset = app.config['APP_TIMEZONE']

    every_platform_query_result_proxy = []
    if group_type == 'Weekly':
        start_time = arrow.Arrow.strptime(start_time, '%Y-%m-%d') - datetime.timedelta(70)
        start_time = start_time.format('YYYY-MM-DD')

        for platform in ['All Platform', 'iOS', 'Android', 'Web', 'Facebook Game']:
            query_result = db.engine.execute(text("""
                                                SELECT DATE_FORMAT(on_day, '%Y-%u') AS week,
                                                       ROUND(AVG(revenue), 2)
                                                FROM   bi_statistic
                                                WHERE  game = 'All Game'
                                                       AND on_day BETWEEN :start_time AND :end_time
                                                       AND platform= :platform
                                                GROUP  BY week"""), platform=platform, start_time=start_time,
                                             end_time=end_time)

            every_platform_query_result_proxy.append(query_result)


    elif group_type == 'Monthly':
        start_time = arrow.Arrow.strptime(start_time, '%Y-%m-%d') - datetime.timedelta(360)
        start_time = start_time.format('YYYY-MM-DD')

        for platform in ['All Platform', 'iOS', 'Android', 'Web', 'Facebook Game']:
            query_result = db.engine.execute(text("""
                                                SELECT DATE_FORMAT(on_day, '%Y-%m') AS month,
                                                       ROUND(AVG(revenue), 2)
                                                FROM   bi_statistic
                                                WHERE  game = 'All Game'
                                                       AND on_day BETWEEN :start_time AND :end_time
                                                       AND platform= :platform
                                                GROUP  BY month"""), platform=platform, start_time=start_time,
                                             end_time=end_time)

            every_platform_query_result_proxy.append(query_result)

    else:

        for platform in ['All Platform', 'iOS', 'Android', 'Web', 'Facebook Game']:
            query_result = db.engine.execute(text("""
                                                SELECT DATE_FORMAT(on_day, '%Y-%m-%d'),
                                                       revenue
                                                FROM   bi_statistic
                                                WHERE  game = 'All Game'
                                                       AND on_day BETWEEN :start_time AND :end_time 
                                                       AND platform= :platform
                                                 """), start_time=start_time, end_time=end_time, platform=platform)

            every_platform_query_result_proxy.append(query_result)

    query_result = list(
        map(lambda query_result: list(map(list, zip(*query_result))), every_platform_query_result_proxy))

    return jsonify(metric_for_each_platform=query_result)
