from flask import Blueprint, render_template, jsonify
from flask import current_app as app
from flask_login import login_required
from sqlalchemy import text

from app.extensions import db
from app.models.main import permission_required
from app.utils import current_time

wallboard = Blueprint('wallboard', __name__)


@wallboard.route("/wallboard", methods=["GET"])
@login_required
@permission_required('manager')
def index():
    return render_template('wallboard/index.html')


@wallboard.route("/wallboard/visualization/summary_data", methods=["GET"])
@login_required
@permission_required('manager')
def visualization_summary_data():
    now = current_time(app.config['APP_TIMEZONE'])
    today = now.format('YYYY-MM-DD')

    sql = """
         SELECT a.on_day, 
               a.new_reg,
               a.new_reg_game_dau,
               a.new_reg_game_dau / a.new_reg              AS reg_to_dau,
               a.dau,
               a.dau - a.new_reg_game_dau                  AS return_dau,
               ( a.dau - a.new_reg_game_dau ) / b.dau_prev AS return_dau_rate,
               a.paid_user_count,
               a.paid_user_count / a.dau                   AS paid_user_rate
        FROM   (SELECT on_day,
                       new_reg,
                       new_reg_game_dau,
                       dau,
                       paid_user_count
                FROM   bi_statistic
                WHERE  on_day >= DATE_ADD(:today, INTERVAL -2 DAY)
                       AND on_day <= :today
                       AND game = 'all game'
                       AND platform = 'all platform') a
               INNER JOIN (SELECT on_day,
                                  dau dau_prev
                           FROM   bi_statistic
                           WHERE  on_day >= DATE_ADD(:today, INTERVAL -2 DAY)
                                  AND on_day <= :today
                                  AND game = 'all game'
                                  AND platform = 'all platform') b
                       ON a.on_day = DATE_ADD(b.on_day, INTERVAL 1 DAY)
        ORDER  BY a.on_day DESC  
         """

    query_result = [
        [row['on_day'],
         format(row['new_reg'] or 0, ',d'),
         format(row['new_reg_game_dau'] or 0, ',d'),
         format(row['reg_to_dau'] or 0, '0.2%'),
         format(row['dau'] or 0, ',d'),
         format(row['return_dau'] or 0, ',d'),
         format(row['return_dau_rate'] or 0, '0.2%'),
         format(row['paid_user_count'], ',d'),
         format(row['paid_user_rate'] or 0, '0.2%'),
         ] for row in db.engine.execute(text(sql), today=today)]

    today_data, yesterday_data = query_result

    return jsonify(today_data=today_data, yesterday_data=yesterday_data)


@wallboard.route("/wallboard/visualization/detail", methods=["GET"])
@login_required
@permission_required('manager')
def metric_detail():
    timezone_offset = app.config['APP_TIMEZONE']
    now = current_time(timezone_offset)
    start_time = now.replace(days=-6).format('YYYY-MM-DD')
    end_time = now.format('YYYY-MM-DD')

    last_7_days_metric = db.engine.execute(text("""
                                       SELECT new_reg, dau, paid_user_count
                                       FROM   bi_statistic
                                       WHERE  on_day BETWEEN :start_time AND :end_time
                                              AND game = :game
                                              AND platform = :platform
                                       """), start_time=start_time, end_time=end_time, game='All Game',
                                           platform='All Platform').fetchall()
    last_7_days_new_reg = [row['new_reg'] for row in last_7_days_metric]
    last_7_days_dau = [row['dau'] for row in last_7_days_metric]
    last_7_days_paid_users = [row['paid_user_count'] for row in last_7_days_metric]

    metric_platform_distribution = db.engine.execute(text("""
                                       SELECT  new_reg, paid_user_count, new_reg_game_dau, platform
                                       FROM   bi_statistic
                                       WHERE  on_day = :end_time
                                              AND game = :game
                                              AND platform != 'All Platform' 
                                              AND platform != 'Web Mobile' 
                                              ORDER BY FIELD(platform, 'Web','iOS','Android' ,'Facebook Game')
                                       """), start_time=start_time, end_time=end_time, game='All Game').fetchall()

    reg_platform_distribution = [row['new_reg'] for row in metric_platform_distribution]
    new_dau_platform_distribution = [row['new_reg_game_dau'] for row in metric_platform_distribution]
    paid_users_platform_distribution = [row['paid_user_count'] for row in metric_platform_distribution]

    reg_conversion_for_platform = {}

    for platform in ['All Platform', 'iOS', 'Android', 'Web', 'Facebook Game']:
        reg_to_dau = db.engine.execute(text("""
                                                SELECT ROUND(new_reg_game_dau / new_reg, 2) AS reg_to_dau
                                                FROM   bi_statistic
                                                WHERE  game = 'All Game'
                                                       AND platform = :platform
                                                       AND  on_day BETWEEN :start_time AND :end_time
         """), start_time=start_time, end_time=end_time, platform=platform)

        reg_conversion_for_platform[platform] = list([row['reg_to_dau'] for row in reg_to_dau])

    return jsonify(last_7_days_new_reg=last_7_days_new_reg,
                   last_7_days_dau=last_7_days_dau,
                   last_7_days_paid_users=last_7_days_paid_users,
                   reg_platform_distribution=reg_platform_distribution,
                   paid_users_platform_distribution=paid_users_platform_distribution,
                   new_dau_platform_distribution=new_dau_platform_distribution,
                   reg_conversion_for_platform=reg_conversion_for_platform)
