import json

import hashlib
import sqlparse
from flask import Blueprint, render_template, request, jsonify, send_from_directory
from flask import current_app as app
from flask_login import login_required, current_user

from app.constants import ADMIN_USER_QUERY_STATUSES, SQL_RESULT_STRATEGIES, ADMIN_USER_ROLES
from app.extensions import db
from app.models.main import AdminUserQuery, permission_required
from app.tasks.sql_lab import get_sql_results
from app.utils import error_msg_from_exception, timeout

sql_lab = Blueprint('sql_lab', __name__)


@sql_lab.route("/sql_lab", methods=["GET"])
@login_required
@permission_required('manager')
def index():
    current_user_tags = db.session.query(AdminUserQuery.id, AdminUserQuery.label_name).filter_by(
        is_custom_label=True).filter_by(admin_user_id=current_user.id).order_by(AdminUserQuery.updated_at).all()

    return render_template('sql_lab/index.html', current_user_tags=current_user_tags)


@sql_lab.route("/custom_board/save_label_name", methods=["POST"])
@login_required
@permission_required('manager')
def save_as_custom_label():
    query_id = request.form.get('query_id')
    label_name = request.form.get('label_name')
    query = db.session.query(AdminUserQuery).filter_by(id=query_id).first()

    query.is_custom_label = True
    query.label_name = label_name
    db.session.flush()
    db.session.commit()

    return jsonify(message='ok'), 200


@sql_lab.route("/custom_board/delete_label_name", methods=["GET"])
@login_required
@permission_required('manager')
def delete_custom_label():
    query_id = request.args.get('query_id')
    query = db.session.query(AdminUserQuery).filter_by(id=query_id).first()
    query.is_custom_label = False
    db.session.flush()
    db.session.commit()

    return jsonify(message='ok'), 200


@sql_lab.route("/query_custom_label", methods=["GET"])
@login_required
@permission_required('manager')
def query_custom_label():
    query_id = request.args.get('query_id')
    target_db = db.session.query(AdminUserQuery.target_db).filter_by(id=query_id).scalar()
    sql = db.session.query(AdminUserQuery.sql).filter_by(id=query_id).scalar()
    return jsonify(sql=sql, target_db=target_db)


@sql_lab.route("/sql_lab/query_histories", methods=["GET"])
@login_required
@permission_required('manager')
def query_histories():
    data = db.session.query(AdminUserQuery).filter_by(admin_user_id=current_user.id).order_by(
        AdminUserQuery.updated_at.desc()).limit(100).all()
    return jsonify(data=[item.to_dict() for item in data])


@sql_lab.route("/sql_lab/format_sql", methods=["POST"])
@login_required
@permission_required('manager')
def format_sql():
    """ format given SQL query """
    sql = request.form.get('sql')
    data = sqlparse.format(sql.strip(), reindent=True, keyword_case='upper')
    return jsonify(data=data)


@sql_lab.route("/sql_lab/execute_sql", methods=["POST"])
@login_required
@permission_required('manager')
def execute_sql():
    """Executes the sql query returns the results."""

    sql = request.form.get('sql')
    include_email = json.loads(request.form.get('include_email', False))
    formatted_sql = sqlparse.format(sql.strip().strip(';'), reindent=True, keyword_case='upper')
    strategy = request.form.get('strategy')

    database = request.form.get('database')
    if database not in app.config.get('SQLALCHEMY_BINDS').keys():
        database = None

    if database is None:
        pending_digest = (str(current_user.id) + '_' + formatted_sql).encode('utf-8')
    else:
        pending_digest = (str(current_user.id) + '_' + database + '_' + formatted_sql).encode('utf-8')

    sql_key = hashlib.md5(pending_digest).hexdigest()

    query = db.session.query(AdminUserQuery).filter_by(sql_key=sql_key).first()
    if query is None:
        query = AdminUserQuery(
            target_db=database,
            sql=formatted_sql,
            sql_key=sql_key,
            status=ADMIN_USER_QUERY_STATUSES.PENDING.value,
            admin_user_id=current_user.id
        )

        db.session.add(query)
        db.session.commit()
    else:
        query.status = ADMIN_USER_QUERY_STATUSES.PENDING.value
        query.rows = None
        query.error_message = None
        query.run_time = None

        db.session.flush()
        db.session.commit()

    query_id = query.id

    permission = current_user.has_role(ADMIN_USER_ROLES.ROOT.value) or current_user.has_role(
        ADMIN_USER_ROLES.ADMIN.value)

    callback_func = lambda: get_sql_results.apply_async(
        args=(database, query_id, SQL_RESULT_STRATEGIES.SEND_TO_MAIL.value, include_email), queue='email')
    try:
        if strategy == SQL_RESULT_STRATEGIES.RENDER_JSON.value:
            with timeout(callback_func=callback_func, seconds=60,
                         error_message="The query exceeded the 60 seconds timeout."):
                result = get_sql_results(database, query_id, strategy=strategy, include_email=include_email)
            return jsonify(result)
        elif strategy == SQL_RESULT_STRATEGIES.SEND_TO_MAIL.value:
            if permission:
                get_sql_results.apply_async(args=(database, query_id, strategy, include_email), priority=9)

                return jsonify(query_id=query_id), 202
            else:
                return jsonify(error="You don't have permission to access this funtion"), 403
        elif strategy == SQL_RESULT_STRATEGIES.GENERATE_DOWNLOAD_LINK.value:
            if permission:
                result = get_sql_results(database, query_id, strategy=strategy, include_email=include_email)
                return jsonify(result)
            else:
                return jsonify(error="You don't have permission to access this funtion"), 403
    except Exception as e:
        return jsonify(error=error_msg_from_exception(e)), 500


@sql_lab.route("/sql_lab/download", methods=["GET"])
@login_required
@permission_required('manager')
def download_result():
    """ download given SQL query result """
    permission = current_user.has_role(ADMIN_USER_ROLES.ROOT.value) or current_user.has_role(
        ADMIN_USER_ROLES.ADMIN.value)

    if permission:
        sql_key = request.args.get('key')
        ts = request.args.get('ts')
        query = db.session.query(AdminUserQuery).filter_by(sql_key=sql_key).first()
        if query is not None:
            filename = "%s_%s.%s" % (sql_key, ts, app.config['REPORT_FILE_EXTENSION'])
            return send_from_directory(app.config['REPORT_FILE_FOLDER'], filename, as_attachment=True)
