import json

from flask import Blueprint, render_template, request, url_for, redirect, flash
from flask_login import login_required, current_user, login_user, logout_user
from itsdangerous import SignatureExpired, BadSignature
from sqlalchemy.orm.exc import NoResultFound

from app import db
from app.constants import ADMIN_USER_ROLES
from app.forms import LoginForm, ResetPasswordForm, ForgetForm
from app.libs.account import send_password_reset_email, validate_token
from app.models.main import AdminUser

account = Blueprint('account', __name__)


@account.route("/sign_in", methods=["GET", "POST"])
def sign_in():
    if current_user.is_authenticated and current_user.has_role(ADMIN_USER_ROLES.GUEST.value):
        return redirect('data/bi_user/', code=302)

    if current_user.is_authenticated:
        return redirect(url_for("dashboard.index"))

    form = LoginForm()
    if request.method == 'POST':
        user = AdminUser.query.filter_by(email=form.email.data).first()
        remember = json.loads( request.form.get('remember', 'false'))
        if form.validate_on_submit():
            login_user(user, remember=remember)
            user.track_sign_in_success(request.headers.get('True-Client-IP'))
            flash("Logged in successfully.", "success")
            next = request.args.get('next')

            # TODO: check next is valid
            # if next_is_valid(next):
            #     return abort(400)

            if current_user.has_role(ADMIN_USER_ROLES.GUEST.value):
                return redirect('data/bi_user/', code=302)
            return redirect(next or url_for("dashboard.index"))
        else:
            if user:
                user.track_sign_in_failure()

    return render_template("account/sign_in.html", form=form)


@account.route("/sign_out", methods=["GET", "DELETE"])
@login_required
def sign_out():
    logout_user()
    return redirect(url_for('account.sign_in'))


@account.route('/forgot', methods=['GET', 'POST'])
def forgot_password():
    form = ForgetForm()

    if request.method == 'POST':

        if form.validate_on_submit():
            email = request.form['email']
            user = db.session.query(AdminUser).filter_by(email=email.strip()).one()
            send_password_reset_email(user)

            return render_template("account/forgot.html", submitted=False, form=form)

    return render_template("account/forgot.html", submitted=True, form=form)


@account.route('/reset/<token>', methods=['GET', 'POST'])
def reset_password(token):
    try:
        user_id = validate_token(token)
        user = db.session.query(AdminUser).filter_by(id=user_id).one()

    except NoResultFound:
        return render_template("account/error.html",
                               error_message="Invalid invite link. Please ask for a new one."), 400
    except (SignatureExpired, BadSignature):
        return render_template("account/error.html",
                               error_message="Your invite link has expired. Please ask for a new one."), 400

    status_code = 200

    form = ResetPasswordForm()

    if request.method == 'POST':

        if form.validate_on_submit():

            new_password = request.form['password1']
            user.set_password(new_password)
            db.session.flush()
            login_user(user)
            db.session.commit()

            flash('Your password has been updated.')

            if current_user.is_authenticated and current_user.has_role(ADMIN_USER_ROLES.GUEST.value):
                return redirect('data/bi_user/', code=302)
            if current_user.is_authenticated:
                return redirect(url_for("dashboard.index"))
        else:
            return render_template('account/reset.html', user=user, form=form, token=token), status_code

    return render_template('account/reset.html', user=user, form=form, token=token), status_code
