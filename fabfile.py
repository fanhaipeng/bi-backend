from fabric.api import env, run
from fabric.colors import *
from fabric.context_managers import cd
from fabric.contrib.console import confirm
from fabric.decorators import roles
from fabric.operations import local
from fabric.tasks import execute

# env.roledefs = {'web': {'hosts': ['deployer@54.165.243.239']u 'db': {'hosts': ['db']} 'celery': {'hosts': ['celery']}}
from app.utils import current_time

env.roledefs = {'web': {'hosts': ['deployer@54.165.243.239']}}
env.use_ssh_config = True
code_dir = '/home/deployer/bi/wpt-dwh/'


def prepare_deploy():
    local("git add .")
    local("git commit")
    local("git push origin prod")


@roles('web')
def update_code():
    with cd(code_dir):
        run('pip install -r requirements.txt')
        run('git checkout release')
        run('git pull origin release')
        run('git check out release')
        run('source  /home/deployer/bi/bin/activate')


@roles('celery')
def restart_celery():
    run('sudo stop celery-beat')
    run('restartcelery')


@roles('web')
def restart_web():
    run('sudo restart wpt-dwh')


def update():
    execute(update_code)


def restart():
    if confirm(red("Are you want to restart web service?")):
        execute(restart_web)
    if confirm(red("Are you want to restart celery ?")):
        # TODO celery 检测没有任务
        execute(restart_celery)


@roles('web')
def release():
    with cd(code_dir):
        run('git checkout prod')
        run('git merge release')


@roles('web')
def rollback():
    with cd(code_dir):
        run('git checkout prod')
        execute(restart)


def deploy():
    execute(prepare_deploy)
    if confirm(red("Are you sure to start deploy ?")):
        execute(update)
        execute(restart)
        if confirm(red("Do you want to release the version ?")):
            # TODO #检查web ，打印celery log,
            execute(release)
            print(green('Successful'))
        else:
            execute(rollback)
            print(green('Prod code have been rollbacked'))


deploy()