
 一个完善的BI自助分析平台, 具有前端多维展示, sql_lab自助分析, 促销推广以及警报邮件等功能.
============================



# 技术栈

* 语言: Python

* 后端框架: Flask

* 前端框架: AdminLte 脚手架

* 数据库: MYSQL, Redis

* 数据可视化: Echarts, Highcharts , Plotly.js

* 任务队列: Celery, RabbitMQ

* 自动化部署: Fabric

* 进程管理:  Supervisor

* Wsgi  : Gunicorn



# 功能介绍

## 数据可视化

### 仪表盘
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/eb4891b72424480909d5cea6380a82d558d0d412/app/public/static/images/15.png?token=bf8143d6748f2f045ead667ad45c8e080e4f61e0)

### TV展示
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/dd6e3589c911fa3cb650740fa61f7df840258857/app/public/static/images/wallboard_on_Tv.jpg?token=89c8992a934ea2e2649a41bb1f92c694a9749274)

### 日常指标监控
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/dd6e3589c911fa3cb650740fa61f7df840258857/app/public/static/images/5.png?token=020c249f7b1157644a6fe7afa59a87c30c6a42ae)

### 其他


### 用户行为日志

![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/3bd38b22acf79817603fb72756f120bd7b988cd3/app/public/static/images/14.png?token=db22bf338daa49efa9f9390a073d72a2589b9a52)

### 地图

![image]( https://bytebucket.org/fanhaipeng/bi-backend/raw/dd6e3589c911fa3cb650740fa61f7df840258857/app/public/static/images/6.png?token=4a3d0e2b83faf42b414c4eb0c8c093153b316a28)

### 桑基图
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/906acd0c99fd1f78f9f06e14faa591823f5f7ea1/app/public/static/images/8.png?token=29bb973029b11488571a2c144b97d4db8ab2714b)




## 自助分析

### SQL查询
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/906acd0c99fd1f78f9f06e14faa591823f5f7ea1/app/public/static/images/11.png?token=75ae84940306d1bc22f2d09d86559a53ca03f10a)

### 警报配置
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/dd6e3589c911fa3cb650740fa61f7df840258857/app/public/static/images/3.png?token=0c1571874adb891acf8f96ae2fb489df442e6fc3)


## 自动化营销推广


### 脸书
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/906acd0c99fd1f78f9f06e14faa591823f5f7ea1/app/public/static/images/10.png?token=8a2ad27a2419f8b3d341b0a9128ffb61d9453a4d)

### 邮件
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/dd6e3589c911fa3cb650740fa61f7df840258857/app/public/static/images/2.png?token=39a2687eccbacdcbf9c91bdb42a99b0c31b3dfc4)

### 手机
![image](https://bytebucket.org/fanhaipeng/bi-backend/raw/80bf98f0b2b32c4d91aedb7ed834701357f0ddec/app/public/static/images/7.png?token=3f73bb7440299f12d7301926e2f369b159bf273d)



