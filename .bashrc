# for virtualenv
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
source /usr/local/bin/virtualenvwrapper.sh
workon bi

#  for flask
export  WPT_DWH_ENV='prod'

# for bi log
alias facebook='tail -150 /data/log/celery-facebook.log'
alias email='tail -150 /data/log/celery-email.log'
alias mobile='tail -150 /data/log/celery-mobile.log'
alias for_wpt_bi='tail -150 /data/log/celery-for-wpt-bi.log'
alias gold='tail -150 /data/log/celery-gold-silver.log'
alias slow='tail -150 /data/log/celery-slow.log'
alias default='tail -150 /data/log/celery-default.log'
alias bill='tail -150 /data/log/celery-user-bill.log'
alias statistic='tail -150 /data/log/celery-statistic.log'
alias beat-log='tail -150 /data/log/celery-beat.log'
alias server-log='tail -150 /data/log/gunicorn.log'
alias process='supervisorctl status'
alias inspect='celery inspect active -b  amqp://bi:Y4rPXacMcEsmAK4kCes4gUVC@10.10.0.96:5672/bi'
alias status='celery status -b  amqp://bi:Y4rPXacMcEsmAK4kCes4gUVC@10.10.0.96:5672/bi'
alias events='celery -A app.tasks.celery events'
alias coin='celery -A app.tasks.celery call app.tasks.scheduled.process_bi_user_coin'
alias purge='celery -A app.tasks.celery purge -f -Q slow'


# for oracle sqlplus
export LD_LIBRARY_PATH=/opt/oracle/instantclient_12_1:$LD_LIBRARY_PATH

# solve Substantial drift from celery@mobile_promotion_worker_2.0 may mean clocks are out of sync. Current drift is 110 seconds
export TZ="America/New_York"

