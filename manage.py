import os

import pandas as pd
from flask_migrate import MigrateCommand
from flask_script import Manager, Server, prompt_bool
from flask_script.commands import ShowUrls, Clean

from app import create_app
from app.extensions import db
from app.models.bi import BIImportConfig, BIStatistic, BIUser, BIUserCurrency, BIUserBill, BIUserBillDetail, \
    BIClubWPTUser, BIUserStatistic
from app.models.main import AdminUser, AdminUserActivity, AdminUserQuery
from app.models.orig_wpt_bi import WPTBIUserStatistic
from app.models.promotion import PromotionPush, PromotionPushHistory
from app.tasks.bi_statistic import process_bi_statistic
from app.tasks.bi_user_currency import process_bi_user_currency
from app.tasks.bi_user_statistic import process_bi_user_statistic
from app.tasks.cron_daily_report import daily_report_dau, daily_report_game_table_statistic, daily_report_ccu
from app.tasks.scheduled import process_bi, process_wpt_bi, sync_sendgrid_campaign

env = os.environ.get('WPT_DWH_ENV', 'dev')
app = create_app('app.settings.%sConfig' % env.capitalize())
manager = Manager(app)
database_manager = Manager(usage="Perform database operations")
table_manager = Manager(usage="Perform table operations")
promotion_manager = Manager(usage="Perform promotion operations")


def init_import_config(*args, all_table=False):
    """ Init BIImportConfig Table """

    BIImportConfig.__table__.create(db.engine, checkfirst=True)

    import_config = dict(bi_user=['last_imported_user_id',
                                  'last_imported_user_update_time',
                                  'last_imported_user_billing_info_id',
                                  'last_imported_user_info_add_time',
                                  'last_imported_user_info_update_time',
                                  'last_imported_user_login_time',
                                  'last_imported_user_ourgame_add_time',
                                  'last_imported_user_ourgame_update_time',
                                  'last_imported_user_payment_spin_purchase_add_time',
                                  'last_imported_user_payment_spin_purchase_update_time',
                                  'last_imported_user_mall_order_add_time',
                                  'last_imported_user_mall_order_update_time',
                                  'last_imported_og_powergamecoin_add_time',
                                  'last_imported_og_gamecoin_add_time',
                                  'last_sync_bi_user_currency_for_lifetime',
                                  'last_imported_promotion_history_id',
                                  'last_imported_user_geo_data_user_id'],
                         wpt_bi=['last_synced_wpt_bi_user_statistic_user_id',
                                 'last_synced_wpt_bi_user_statistic_update_time'], bi_user_bill=[
            'last_imported_user_mall_bill_order_id',
            'last_imported_user_mall_bill_order_update_time',
            'last_imported_user_bill_dollar_paid_total_add_time',
            'last_imported_user_mall_bill_detail_order_id',
            'last_imported_user_mall_bill_detail_order_update_time'],
                         bi_user_currency=['last_imported_user_gold_currency_add_time',
                                           'last_imported_user_silver_currency_add_time'],
                         bi_clubwpt_user=['last_imported_clubwpt_user_add_time',
                                          'last_imported_clubwpt_user_update_time'])
    for table in args:
        config = import_config.get(table)
        if config is not None:
            for v in config:
                db.session.query(BIImportConfig).filter_by(var=v).delete()
                db.session.add(BIImportConfig(var=v))

        db.session.commit()

    if all_table == True:
        for config in import_config.values():
            for v in config:
                db.session.query(BIImportConfig).filter_by(var=v).delete()
                db.session.add(BIImportConfig(var=v))

        db.session.commit()

# command for database

@database_manager.command
def drop():
    "Drops all database tables"
    if prompt_bool("Are you sure you want to lose all your data (yes/no) "):
        db.drop_all(bind=None)
        db.drop_all(bind='bi_promotion')
        db.drop_all(bind='bi_lifetime')
        db.drop_all(bind='bi_user_statistic')


@database_manager.command
def create():
    """Creates database tables from sqlalchemy models and init config value, then
       you will get a test account (account = admin@admin.com , password=123)
    """

    init_import_config(all_table=True)

    db.create_all(bind=None)
    db.create_all(bind='bi_promotion')
    db.create_all(bind='bi_lifetime')
    db.create_all(bind='bi_user_statistic')

    user = AdminUser(name='admin', email='admin@admin.com', password='admin', timezone='EST')
    db.session.add(user)
    db.session.commit()


@database_manager.command
def recreate():
    "Recreates database tables (same as issuing 'drop' and then 'create')"

    if prompt_bool("Are you sure you want to recreate all your data (yes/no) "):
        drop()
        create()
        reset(table_name='all')
        sync()


# command for table

@table_manager.option('-t', '--table', dest='table_name')
def reset(table_name):
    """ Reset table and init table import value.\n
        Optional table: [admin, wpt_bi, bi_user, bi_clubwpt_user, bi_user_bill, bi_user_currency, bi_user_statistic bi_statistic, promotion,]
    """
    if prompt_bool("Are you sure to RESET {} ? (yes/no) ".format(table_name)):

        def reset_table(table):

            init_import_config(table)

            if table_name == 'admin':
                AdminUserActivity.__table__.drop(db.engine, checkfirst=True)
                AdminUserQuery.__table__.drop(db.engine, checkfirst=True)
                AdminUserActivity.__table__.create(db.engine, checkfirst=True)
                AdminUserQuery.__table__.create(db.engine, checkfirst=True)


            elif table_name == 'wpt_bi':
                db.session.query(WPTBIUserStatistic).delete()

            elif table_name == 'promotion':
                PromotionPush.__table__.drop(db.get_engine(db.get_app(), bind='bi_promotion'), checkfirst=True)
                PromotionPushHistory.__table__.drop(db.get_engine(db.get_app(), bind='bi_promotion'), checkfirst=True)

                PromotionPush.__table__.create(db.get_engine(db.get_app(), bind='bi_promotion'), checkfirst=True)
                PromotionPushHistory.__table__.create(db.get_engine(db.get_app(), bind='bi_promotion'), checkfirst=True)


            elif table_name == 'bi_user':
                BIUser.__table__.drop(db.engine, checkfirst=True)
                BIUser.__table__.create(db.engine, checkfirst=True)

            elif table_name == 'bi_user_bill':
                BIUserBill.__table__.drop(db.engine, checkfirst=True)
                BIUserBill.__table__.create(db.engine, checkfirst=True)
                BIUserBillDetail.__table__.drop(db.engine, checkfirst=True)
                BIUserBillDetail.__table__.create(db.engine, checkfirst=True)

            elif table_name == 'bi_user_currency':
                BIUserCurrency.__table__.drop(db.engine, checkfirst=True)
                BIUserCurrency.__table__.create(db.engine, checkfirst=True)

            elif table_name == 'bi_clubwpt_user':
                BIClubWPTUser.__table__.drop(db.engine, checkfirst=True)
                BIClubWPTUser.__table__.create(db.engine, checkfirst=True)

            elif table_name == 'bi_statistic':
                BIStatistic.__table__.drop(db.engine, checkfirst=True)
                BIStatistic.__table__.create(db.engine, checkfirst=True)

                from datetime import date
                for day in pd.date_range(date(2016, 6, 31), date(2019, 12, 31)):
                    for game in ['All Game', 'TexasPoker', 'TimeSlots']:
                        for platform in ['All Platform', 'iOS', 'Android', 'Web', 'Web Mobile', 'Facebook Game']:
                            db.session.add(BIStatistic(on_day=day.strftime("%Y-%m-%d"), game=game, platform=platform))

            elif table_name == 'bi_user_statistic':
                BIUserStatistic.__table__.drop(db.engine, checkfirst=True)
                BIUserStatistic.__table__.create(db.engine, checkfirst=True)

            else:
                print('{} table does not exist !'.format(table_name))

            db.session.commit()

        if table_name == 'all':
            for table_name in ['admin', 'wpt_bi', 'promotion', 'bi_user', 'bi_user_bill', 'bi_user_currency',
                               'bi_clubwpt_user', 'bi_statistic', 'bi_user_statistic']:
                reset_table(table=table_name)

        else:
            reset_table(table=table_name)


@table_manager.command
def sync():
    """
    Sync all data
   """

    def all_data():

        if app.config['ENV'] == 'prod':
            process_bi.delay()
            process_wpt_bi.delay()
            process_bi_user_currency.delay()
        else:
            process_bi()
            process_wpt_bi()
            process_bi_user_currency()

    all_data()


@table_manager.option('-t', '--table', dest='table_name')
@table_manager.option('-d', '--target', dest='target')
def statistic(table_name, target):
    """
    Gather statistic result. Optional table: [ bi_user_statistic, bi_statistic]
    """

    if table_name == 'bi_statistic':
        dau = int(input('dau = '))
        wau = int(input('wau = '))
        mau = int(input('mau = '))
        new_reg = int(input('new_reg = '))
        new_reg_dau = int(input('new_reg_dau = '))
        free_gold_silver = int(input('free_gold_silver = '))
        payment_records = int(input('payment_records = '))
        retention = int(input('retention = '))
        revenue = int(input('revenue = '))
        game_records = int(input('game_records = '))
        send_free_gold_failure = int(input('send_free_gold_failure = '))

        if app.config['ENV'] == 'prod':
            process_bi_statistic.delay(target, dau=dau, wau=wau, mau=mau, new_reg=new_reg,
                                       new_reg_dau=new_reg_dau,
                                       free_gold_silver=free_gold_silver, payment_records=payment_records,
                                       retention=retention, revenue=revenue, game_records=game_records,
                                       send_free_gold_failure=send_free_gold_failure)

        else:
            process_bi_statistic(target, dau=dau, wau=wau, mau=mau, new_reg=new_reg,
                                 new_reg_dau=new_reg_dau,
                                 free_gold_silver=free_gold_silver, payment_records=payment_records,
                                 retention=retention, revenue=revenue, game_records=game_records)

    if table_name == 'bi_user_statistic':
        dau_related = int(input('dau_related = '))
        dollar_related = int(input('dollar_related = '))
        game_behaviour_related = int(input('game_behaviour_related = '))
        gold_related = int(input('gold_related = '))
        reg_related = int(input('reg_related = '))

        if app.config['ENV'] == 'prod':
            process_bi_user_statistic.delay(target, dau_related=dau_related, dollar_related=dollar_related,
                                            game_behaviour_related=game_behaviour_related,
                                            gold_related=gold_related, reg_related=reg_related)
        else:
            process_bi_user_statistic(target, dau_related=dau_related, dollar_related=dollar_related,
                                      game_behaviour_related=game_behaviour_related,
                                      gold_related=gold_related, reg_related=reg_related)


# Others
@manager.command
def send_daily_report():
    if app.config['ENV'] == 'prod':
        daily_report_dau.delay()
        daily_report_ccu.delay()
        daily_report_game_table_statistic.delay()
    else:
        daily_report_dau()
        daily_report_ccu()
        daily_report_game_table_statistic()


@manager.command
def get_sendgrid_campaign():
    sync_sendgrid_campaign()


@manager.command
def tmp():
    sync_sendgrid_campaign()


@manager.shell
def make_shell_context():
    """ Creates a python REPL with several default imports
        in the context of the app
    """
    return dict(app=app, db=db, AdminUser=AdminUser, BIUser=BIUser, BIStatistic=BIStatistic,
                BIUserBill=BIUserBill, BIUserStatistic=BIUserStatistic, BIUserCurrency=BIUserCurrency)


manager.add_command("server", Server(host='0.0.0.0'))
manager.add_command("show-urls", ShowUrls())
manager.add_command("clean", Clean())
manager.add_command('db', MigrateCommand)
manager.add_command("database", database_manager)
manager.add_command("table", table_manager)
manager.add_command("promotion", promotion_manager)

if __name__ == "__main__":
    manager.run()
